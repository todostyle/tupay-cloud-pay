#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
echo "begin copy sql "
cp ../sql/ry_20210908.sql ./mysql/db
cp ../sql/ry_config_20211118.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../tupay-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy tupay-gateway "
cp ../tupay-gateway/target/tupay-gateway.jar ./ruoyi/gateway/jar

echo "begin copy tupay-auth "
cp ../tupay-auth/target/tupay-auth.jar ./ruoyi/auth/jar

echo "begin copy tupay-visual "
cp ../tupay-visual/tupay-monitor/target/tupay-visual-monitor.jar  ./ruoyi/visual/monitor/jar

echo "begin copy tupay-modules-system "
cp ../tupay-modules/tupay-system/target/tupay-modules-system.jar ./ruoyi/modules/system/jar

echo "begin copy tupay-modules-file "
cp ../tupay-modules/tupay-file/target/tupay-modules-file.jar ./ruoyi/modules/file/jar

echo "begin copy tupay-modules-job "
cp ../tupay-modules/tupay-job/target/tupay-modules-job.jar ./ruoyi/modules/job/jar

echo "begin copy tupay-modules-gen "
cp ../tupay-modules/tupay-gen/target/tupay-modules-gen.jar ./ruoyi/modules/gen/jar

