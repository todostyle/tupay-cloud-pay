package com.tostyle.tupay.payment.api;

import com.tostyle.tupay.common.core.constant.ServiceNameConstants;
import com.tostyle.tupay.payment.api.factory.RemotePayOrderFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 *  交易订单服务
 * @author tostyle
 * 2022/4/22 14:28
 */
@FeignClient(contextId = "remotePayOrderService", value = ServiceNameConstants.PAYMENT_SERVICE, fallbackFactory = RemotePayOrderFallbackFactory.class)
public interface RemotePayOrderService {
}
