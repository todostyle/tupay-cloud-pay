package com.tostyle.tupay.payment.api;

import com.tostyle.tupay.common.core.constant.ServiceNameConstants;
import com.tostyle.tupay.payment.api.factory.RemoteRefundOrderFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 交易退款服务
 * @author tostyle
 * 2022/4/22 14:37
 */
@FeignClient(contextId = "remoteRefundOrderService", value = ServiceNameConstants.PAYMENT_SERVICE, fallbackFactory = RemoteRefundOrderFallbackFactory.class)
public interface RemoteRefundOrderService {
}
