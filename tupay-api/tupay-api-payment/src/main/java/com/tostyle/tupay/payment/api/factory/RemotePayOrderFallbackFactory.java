package com.tostyle.tupay.payment.api.factory;

import com.tostyle.tupay.payment.api.RemotePayOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 交易服务降级处理
 * @author tostyle
 * 2022/4/22 14:30
 */
@Component
public class RemotePayOrderFallbackFactory implements FallbackFactory<RemotePayOrderService> {

    private static final Logger log = LoggerFactory.getLogger(RemotePayOrderFallbackFactory.class);

    @Override
    public RemotePayOrderService create(Throwable throwable) {
        log.error("交易服务调用失败:{}", throwable.getMessage());
        return null;
    }
}
