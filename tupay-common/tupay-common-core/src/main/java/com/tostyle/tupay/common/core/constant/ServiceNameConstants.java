package com.tostyle.tupay.common.core.constant;

/**
 * 服务名称
 * 
 * @author ruoyi
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "tupay-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "tupay-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "tupay-file";

    /**
     * 支付服务的serviceid
     */
    public static final String PAYMENT_SERVICE = "tupay-payment";
}
