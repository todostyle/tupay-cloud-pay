package com.tostyle.tupay.common.core.exception;


import com.tostyle.tupay.common.core.enums.ApiCodeEnum;
import com.tostyle.tupay.common.core.model.ApiResp;
import lombok.Getter;

/**
 * 自定义业务异常
 * @author tostyle
 * 2022/2/22 16:00
 */
@Getter
public class BizException extends RuntimeException {

    private ApiResp apiResp;

    /** 业务自定义异常 **/
    public BizException(String msg) {
        super(msg);
        this.apiResp = ApiResp.customFail(msg);
    }

    public BizException(ApiCodeEnum apiCodeEnum, String... params) {
        super();
        apiResp = ApiResp.fail(apiCodeEnum, params);
    }

    public BizException(ApiResp apiResp) {
        super(apiResp.getMsg());
        this.apiResp = apiResp;
    }
}
