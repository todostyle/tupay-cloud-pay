package com.tostyle.tupay.common.core.model;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.enums.ApiCodeEnum;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

/**
 * @author tostyle
 * 2022/2/10 16:42
 */
@Data
@AllArgsConstructor
public class ApiResp {

    /** 业务响应码 **/
    private Integer code;

    /** 业务响应信息 **/
    private String msg;

    /** 数据对象 **/
    private Object data;

    /** 签名值 **/
    private String sign;

    /** 输出json格式字符串 **/
    public String toJSONString(){
        return JSON.toJSONString(this);
    }

    /** 业务处理成功 **/
    public static ApiResp ok(){
        return ok(null);
    }

    /** 业务处理成功 **/
    public static ApiResp ok(Object data){
        return new ApiResp(ApiCodeEnum.SUCCESS.getCode(), ApiCodeEnum.SUCCESS.getMsg(), data, null);
    }

    /** 业务处理成功, 自动签名 **/
    public static ApiResp okWithSign(Object data, String mchKey){

        if(data == null){
            return new ApiResp(ApiCodeEnum.SUCCESS.getCode(), ApiCodeEnum.SUCCESS.getMsg(), null, null);
        }
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(data);
        String sign = Md5Utils.getSign(jsonObject, mchKey);
        return new ApiResp(ApiCodeEnum.SUCCESS.getCode(), ApiCodeEnum.SUCCESS.getMsg(), data, sign);
    }

    /** 业务处理失败 **/
    public static ApiResp fail(ApiCodeEnum apiCodeEnum, String... params){

        if(params == null || params.length <= 0){
            return new ApiResp(apiCodeEnum.getCode(), apiCodeEnum.getMsg(), null, null);
        }
        return new ApiResp(apiCodeEnum.getCode(), String.format(apiCodeEnum.getMsg(), params), null, null);
    }

    /** 自定义错误信息, 原封不用的返回输入的错误信息 **/
    public static ApiResp customFail(String customMsg){
        return new ApiResp(ApiCodeEnum.CUSTOM_FAIL.getCode(), customMsg, null, null);
    }

}
