package com.tostyle.tupay.common.core.model;

/**
 * @author tostyle
 * 2022/2/22 16:19
 */
public class PayConstant {

    /** yes or no **/
    public static final long NO = 0;
    public static final long YES = 1;

    /** 通用 可用 / 禁用 **/
    public static final long PUB_USABLE = 1;
    public static final int PUB_DISABLE = 0;

    /**
     * 账号类型:1-服务商 2-商户 3-商户应用
     */
    public static final long INFO_TYPE_ISV = 1;
    public static final long INFO_TYPE_MCH = 2;
    public static final long INFO_TYPE_MCH_APP = 3;


    /**
     * 商户类型:1-普通商户 2-特约商户
     */
    public static final long MCH_TYPE_NORMAL = 1;
    public static final long MCH_TYPE_ISVSUB = 2;



    //渠道类型
    public interface CHANNEL_CODE{
        String ALIPAY = "alipay";   // 支付宝官方支付
        String WXPAY = "wxpay";     // 微信官方支付
        String YSFPAY = "ysfpay";   // 云闪付开放平台
    }

    //支付方式代码
    public interface PAY_WAY_CODE{

        // 特殊支付方式
        String QR_CASHIER = "QR_CASHIER"; //  ( 通过二维码跳转到收银台完成支付， 已集成获取用户ID的实现。  )
        String AUTO_BAR = "AUTO_BAR"; // 条码聚合支付（自动分类条码类型）

        String ALI_BAR = "ALI_BAR";  //支付宝条码支付
        String ALI_JSAPI = "ALI_JSAPI";  //支付宝服务窗支付
        String ALI_APP = "ALI_APP";  //支付宝 app支付
        String ALI_PC = "ALI_PC";  //支付宝 电脑网站支付
        String ALI_WAP = "ALI_WAP";  //支付宝 wap支付
        String ALI_QR = "ALI_QR";  //支付宝 二维码付款

        String YSF_BAR = "YSF_BAR";  //云闪付条码支付
        String YSF_JSAPI = "YSF_JSAPI";  //云闪付服务窗支付

        String WX_JSAPI = "WX_JSAPI";  //微信jsapi支付
        String WX_LITE = "WX_LITE";  //微信小程序支付
        String WX_BAR = "WX_BAR";  //微信条码支付
        String WX_H5 = "WX_H5";  //微信H5支付
        String WX_NATIVE = "WX_NATIVE";  //微信扫码支付
    }

    //支付数据包 类型
    public interface PAY_DATA_TYPE {
        String PAY_URL = "payUrl";  //跳转链接的方式  redirectUrl
        String FORM = "form";  //表单提交
        String WX_APP = "wxapp";  //微信app参数
        String ALI_APP = "aliapp";  //支付宝app参数
        String YSF_APP = "ysfapp";  //云闪付app参数
        String CODE_URL = "codeUrl";  //二维码URL
        String CODE_IMG_URL = "codeImgUrl";  //二维码图片显示URL
        String NONE = "none";  //无参数
    }

    /** 入账方式 **/
    public static final String ENTRY_WX_CASH = "WX_CASH";
    public static final String ENTRY_ALIPAY_CASH = "ALIPAY_CASH";
    public static final String ENTRY_BANK_CARD = "BANK_CARD";

    /** 支付状态 **/
    public static final long STATE_INIT = 0; //订单生成
    public static final long STATE_ING = 1; //支付中
    public static final long STATE_SUCCESS = 2; //支付成功
    public static final long STATE_FAIL = 3; //支付失败
    public static final long STATE_CANCEL = 4; //已撤销
    public static final long STATE_REFUND = 5; //已退款
    public static final long STATE_CLOSED = 6; //订单关闭

    /** 退款状态 **/
    public static final long REFUND_STATE_INIT = 0; //订单生成
    public static final long REFUND_STATE_ING = 1; //退款中
    public static final long REFUND_STATE_SUCCESS = 2; //退款成功
    public static final long REFUND_STATE_FAIL = 3; //退款失败
    public static final long REFUND_STATE_CLOSED = 4; //退款任务关闭

    /** 退款类型状态 **/
    public static final long REFUND_STATE_NONE = 0; //未发生实际退款
    public static final long REFUND_STATE_SUB = 1; //部分退款
    public static final long REFUND_STATE_ALL = 2; //全额退款

    /** 转账状态 **/
    public static final long TRANSFER_STATE_INIT = 0; //订单生成
    public static final long TRANSFER_STATE_ING = 1; //转账中
    public static final long TRANSFER_STATE_SUCCESS = 2; //转账成功
    public static final long TRANSFER_STATE_FAIL = 3; //转账失败
    public static final long TRANSFER_STATE_CLOSED = 4; //转账关闭

    /** 分账状态 **/
    public static final long DIVISION_MODE_FORBID = 0; //该笔订单不允许分账
    public static final long DIVISION_MODE_AUTO = 1; //支付成功按配置自动完成分账
    public static final long DIVISION_MODE_MANUAL = 2; //商户手动分账(解冻商户金额)


    //订单类型:1-支付,2-退款, 3-转账
    public static final long TYPE_PAY_ORDER = 1;
    public static final long TYPE_REFUND_ORDER = 2;
    public static final long TYPE_TRANSFER_ORDER = 3;


    //通知状态
    public static final long NOTIFY_STATE_ING = 1;
    public static final long NOTIFY_STATE_SUCCESS = 2;
    public static final long NOTIFY_STATE_FAIL = 3;


    //接口版本
    public interface PAY_IF_VERSION{
        String WX_V2 = "V2";  //微信接口版本V2
        String WX_V3 = "V3";  //微信接口版本V3
    }

    public static final long TYPE_NORMAL = 1; //商户类型： 1-普通商户
    public static final long TYPE_ISVSUB = 2; //商户类型： 2-特约商户

    /**
     * 订单默认超时时间
     */
    public static final String ORDER_EXPIRE_TIME="2h";
    /**
     * 订单过期单位
     */
    public static final String ORDER_EXPIRE_TIME_UNIT="m";
}
