package com.tostyle.tupay.common.core.model.params;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayIsvParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvParams;
import com.tostyle.tupay.common.core.model.params.ysf.YsfpayIsvParams;

/**
 * 抽象类 isv参数定义
 * @author tostyle
 * 2022/2/22 16:24
 */
public abstract class IsvParams {

    public static IsvParams factory(String ifCode, String paramsStr){

        if(PayConstant.CHANNEL_CODE.WXPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, WxpayIsvParams.class);
        }else if(PayConstant.CHANNEL_CODE.ALIPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, AlipayIsvParams.class);
        }else if(PayConstant.CHANNEL_CODE.YSFPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, YsfpayIsvParams.class);
        }
        return null;
    }

    /**
     *  敏感数据脱敏
     */
    public abstract String deSenData();
}
