package com.tostyle.tupay.common.core.model.params;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayIsvSubMchParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvSubMchParams;
import com.tostyle.tupay.common.core.model.params.ysf.YsfpayIsvSubMchParams;

/**
 * 抽象类 特约商户参数定义
 * @author tostyle
 * 2022/2/22 16:28
 */
public abstract class IsvSubMchParams {

    public static IsvSubMchParams factory(String ifCode, String paramsStr){

        if(PayConstant.CHANNEL_CODE.WXPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, WxpayIsvSubMchParams.class);
        }else if(PayConstant.CHANNEL_CODE.ALIPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, AlipayIsvSubMchParams.class);
        }else if(PayConstant.CHANNEL_CODE.YSFPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, YsfpayIsvSubMchParams.class);
        }
        return null;
    }
}
