package com.tostyle.tupay.common.core.model.params;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayNormalMchParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayNormalMchParams;

/**
 * @author tostyle
 * 2022/2/22 16:29
 */
public abstract class NormalMchParams {

    public static NormalMchParams factory(String ifCode, String paramsStr){

        if(PayConstant.CHANNEL_CODE.WXPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, WxpayNormalMchParams.class);
        }else if(PayConstant.CHANNEL_CODE.ALIPAY.equals(ifCode)){
            return JSON.parseObject(paramsStr, AlipayNormalMchParams.class);
        }
        return null;
    }

    /**
     *  敏感数据脱敏
     */
    public abstract String deSenData();
}
