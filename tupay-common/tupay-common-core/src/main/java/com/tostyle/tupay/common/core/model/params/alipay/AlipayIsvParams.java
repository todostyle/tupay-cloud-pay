package com.tostyle.tupay.common.core.model.params.alipay;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.params.IsvParams;
import com.tostyle.tupay.common.core.utils.StringUtils;
import lombok.Data;


/**
 * @author tostyle
 * 2022/2/22 16:32
 */
@Data
public class AlipayIsvParams extends IsvParams {


    /** 是否沙箱环境 */
    private long sandbox;

    /** pid */
    private String pid;

    /** appId */
    private String appId;

    /** privateKey */
    private String privateKey;

    /** alipayPublicKey */
    private String alipayPublicKey;

    /** 签名方式 **/
    private String signType;

    /** 是否使用证书方式 **/
    private long useCert;

    /** app 证书 **/
    private String appPublicCert;

    /** 支付宝公钥证书（.crt格式） **/
    private String alipayPublicCert;

    /** 支付宝根证书 **/
    private String alipayRootCert;

    @Override
    public String deSenData() {

        AlipayIsvParams isvParams = this;
        if (StringUtils.isNotBlank(this.privateKey)) {
            isvParams.setPrivateKey(StringUtils.str2Star(this.privateKey, 4, 4, 6));
        }
        if (StringUtils.isNotBlank(this.alipayPublicKey)) {
            isvParams.setAlipayPublicKey(StringUtils.str2Star(this.alipayPublicKey, 6, 6, 6));
        }
        return JSON.toJSONString(isvParams);
    }

}
