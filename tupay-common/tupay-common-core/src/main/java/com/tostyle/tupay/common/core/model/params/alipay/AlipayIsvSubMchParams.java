package com.tostyle.tupay.common.core.model.params.alipay;


import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import lombok.Data;

/**
 * 支付宝 特约商户参数定义
 * @author tostyle
 * 2022/2/22 17:03
 */
@Data
public class AlipayIsvSubMchParams extends IsvSubMchParams {

    private String appAuthToken;
}
