package com.tostyle.tupay.common.core.model.params.alipay;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.params.NormalMchParams;
import com.tostyle.tupay.common.core.utils.StringUtils;
import lombok.Data;


/**
 * 支付宝 普通商户参数定义
 * @author tostyle
 * 2022/2/22 17:04
 */
@Data
public class AlipayNormalMchParams extends NormalMchParams {

    /** 是否沙箱环境 */
    private Long sandbox;

    /** appId */
    private String appId;

    /** privateKey */
    private String privateKey;

    /** alipayPublicKey */
    private String alipayPublicKey;

    /** 签名方式 **/
    private String signType;

    /** 是否使用证书方式 **/
    private Long useCert;

    /** app 证书 **/
    private String appPublicCert;

    /** 支付宝公钥证书（.crt格式） **/
    private String alipayPublicCert;

    /** 支付宝根证书 **/
    private String alipayRootCert;

    @Override
    public String deSenData() {

        AlipayNormalMchParams mchParams = this;
        if (StringUtils.isNotBlank(this.privateKey)) {
            mchParams.setPrivateKey(StringUtils.str2Star(this.privateKey, 4, 4, 6));
        }
        if (StringUtils.isNotBlank(this.alipayPublicKey)) {
            mchParams.setAlipayPublicKey(StringUtils.str2Star(this.alipayPublicKey, 6, 6, 6));
        }
        return JSON.toJSONString(mchParams);
    }

}
