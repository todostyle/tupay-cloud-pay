package com.tostyle.tupay.common.core.model.params.wxpay;

import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import lombok.Data;

/**
 * @author tostyle
 * 2022/2/22 17:08
 */
@Data
public class WxpayIsvSubMchParams extends IsvSubMchParams {

    /** 子商户ID **/
    private String subMchId;

    /** 子账户appID **/
    private String subMchAppId;
}
