package com.tostyle.tupay.common.core.model.params.ysf;

import lombok.Data;

/**
 * @author tostyle
 * 2022/2/22 17:12
 */
@Data
public class YsfpayConfig {


    /** 网关地址 */
    public static String PROD_SERVER_URL = "https://partner.95516.com";
    public static String SANDBOX_SERVER_URL = "http://ysf.bcbip.cn:10240";
}
