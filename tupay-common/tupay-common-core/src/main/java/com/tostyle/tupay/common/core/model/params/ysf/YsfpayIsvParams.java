package com.tostyle.tupay.common.core.model.params.ysf;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.params.IsvParams;
import com.tostyle.tupay.common.core.utils.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author tostyle
 * 2022/2/22 17:13
 */
@Data
public class YsfpayIsvParams extends IsvParams {

    /** 是否沙箱环境 */
    private long sandbox;

    /** serProvId **/
    private String serProvId;

    /** isvPrivateCertFile 证书 **/
    private String isvPrivateCertFile;

    /** isvPrivateCertPwd **/
    private String isvPrivateCertPwd;

    /** ysfpayPublicKey **/
    private String ysfpayPublicKey;

    /** acqOrgCodeList 支付机构号 **/
    private String acqOrgCode;

    @Override
    public String deSenData() {

        YsfpayIsvParams isvParams = this;
        if (StringUtils.isNotBlank(this.isvPrivateCertPwd)) {
            isvParams.setIsvPrivateCertPwd(StringUtils.str2Star(this.isvPrivateCertPwd, 0, 3, 6));
        }
        if (StringUtils.isNotBlank(this.ysfpayPublicKey)) {
            isvParams.setYsfpayPublicKey(StringUtils.str2Star(this.ysfpayPublicKey, 6, 6, 6));
        }
        return JSON.toJSONString(isvParams);
    }

}
