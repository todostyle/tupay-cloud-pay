package com.tostyle.tupay.common.core.model.params.ysf;


import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import lombok.Data;

/**
 * 云闪付 配置信息
 * @author tostyle
 * 2022/2/22 17:14
 */
@Data
public class YsfpayIsvSubMchParams extends IsvSubMchParams {

    private String merId;   // 商户编号
}
