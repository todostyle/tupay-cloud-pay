package com.tostyle.tupay.common.core.utils.sign;


import cn.hutool.crypto.SecureUtil;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author tostyle
 * 2022/2/22 15:38
 */
public class Md5Utils {

    private static final Logger logger = LoggerFactory.getLogger(Md5Utils.class);

    public static byte[] AES_KEY = "4ChT08phkz59hquD795X7w==".getBytes();
    /**
     * <p><b>Description: </b>计算签名摘要
     * <p>2018年9月30日 上午11:32:46
     * @param map 参数Map
     * @param key 商户秘钥
     * @return
     */
    public static String getSign(Map<String, Object> map, String key){
        ArrayList<String> list = new ArrayList<String>();
        for(Map.Entry<String, Object> entry:map.entrySet()){
            if(null != entry.getValue() && !"".equals(entry.getValue())){
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;
        logger.info("signStr:{}", result);
        result = md5(result);
        logger.info("sign:{}", result);
        return result;
    }
    /**
     * <p><b>Description: </b>MD5
     * <p>2018年9月30日 上午11:33:19
     * @param value
     * @return
     */
    private static String md5(String value) {
        MessageDigest md = null;
        try {
            byte[] data = value.getBytes(StandardCharsets.UTF_8);
            md = MessageDigest.getInstance("MD5");
            byte[] digestData = md.digest(data);
            return toHex(digestData).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String toHex(byte[] input) {
        if (input == null) {
            return null;
        }
        StringBuilder output = new StringBuilder(input.length * 2);
        for (byte b : input) {
            int current = b & 0xff;
            if (current < 16) {
                output.append("0");
            }
            output.append(Integer.toString(current, 16));
        }

        return output.toString().toUpperCase();
    }

    /** 校验微信/支付宝二维码是否符合规范， 并根据支付类型返回对应的支付方式  **/
    public static String getPayWayCodeByBarCode(String barCode){

        if(StringUtils.isEmpty(barCode)) {
            throw new BizException("条码为空");
        }

        //微信 ： 用户付款码条形码规则：18位纯数字，以10、11、12、13、14、15开头
        //文档： https://pay.weixin.qq.com/wiki/doc/api/micropay.php?chapter=5_1
        if(barCode.length() == 18 && Pattern.matches("^(10|11|12|13|14|15)(.*)", barCode)){
            return PayConstant.PAY_WAY_CODE.WX_BAR;
        }
        //支付宝： 25~30开头的长度为16~24位的数字
        //文档： https://docs.open.alipay.com/api_1/alipay.trade.pay/
        else if(barCode.length() >= 16 && barCode.length() <= 24 && Pattern.matches("^(25|26|27|28|29|30)(.*)", barCode)){
            return PayConstant.PAY_WAY_CODE.ALI_BAR;
        }
        //云闪付： 二维码标准： 19位 + 62开头
        //文档：https://wenku.baidu.com/view/b2eddcd09a89680203d8ce2f0066f5335a8167fa.html
        else if(barCode.length() == 19 && Pattern.matches("^(62)(.*)", barCode)){
            return PayConstant.PAY_WAY_CODE.YSF_BAR;
        }
        else{  //暂时不支持的条码类型
            throw new BizException("不支持的条码");
        }
    }

    /**
     * 加密
     * @param str
     * @return
     */
    public static String aesEncode(String str){
        return SecureUtil.aes(AES_KEY).encryptHex(str);
    }

    /**
     * 解密
     * @param str
     * @return
     */
    public static String aesDecode(String str){
        return SecureUtil.aes(AES_KEY).decryptStr(str);
    }
}
