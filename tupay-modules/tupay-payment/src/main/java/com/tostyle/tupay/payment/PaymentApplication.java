package com.tostyle.tupay.payment;

import com.tostyle.tupay.common.security.annotation.EnableCustomConfig;
import com.tostyle.tupay.common.security.annotation.EnableRyFeignClients;
import com.tostyle.tupay.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author tostyle
 * 2022/3/24 16:31
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class PaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ 图培依支付服务启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
