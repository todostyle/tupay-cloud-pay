package com.tostyle.tupay.payment.beans;

import cn.hutool.core.util.URLUtil;
import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.constant.CacheConstants;
import com.tostyle.tupay.common.core.text.Convert;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * 系统应用配置项定义Bean
 * @author tostyle
 * 2022/4/18 13:31
 */
@Component
public class ApplicationConfig {

    @Autowired
    private RedisService redisService;

    public String getPaySiteUrl(){
        return Convert.toStr(redisService.getCacheObject(CacheConstants.PAYMENT_CACHE_KEY));
    }

    /** 生成  【jsapi统一收银台跳转地址】 **/
    public String genUniJsapiPayUrl(String tradeNo){
        return getPaySiteUrl() + "/cashier/index.html#/hub/" + Md5Utils.aesEncode(tradeNo);
    }

    /** 生成  【jsapi统一收银台】oauth2获取用户ID回调地址 **/
    public String genOauth2RedirectUrlEncode(String tradeNo){
        return URLUtil.encodeAll(getPaySiteUrl() + "/cashier/index.html#/oauth2Callback/" + Md5Utils.aesEncode(tradeNo));
    }

    /** 生成  【商户获取渠道用户ID接口】oauth2获取用户ID回调地址 **/
    public String genMchChannelUserIdApiOauth2RedirectUrlEncode(JSONObject param){
        return URLUtil.encodeAll(getPaySiteUrl() + "/api/channelUserId/oauth2Callback/" + Md5Utils.aesEncode(param.toJSONString()));
    }

    /** 生成  【jsapi统一收银台二维码图片地址】 **/
    public String genScanImgUrl(String url){
        return getPaySiteUrl() + "/api/scan/imgs/" + Md5Utils.aesEncode(url) + ".png";
    }
}
