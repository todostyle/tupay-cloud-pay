package com.tostyle.tupay.payment.beans;

import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 支付平台 获取系统文件工具类
 * @author tostyle
 * 2022/4/18 10:55
 */
@Component
public class ChannelCertBean {


    /**
     * 获取支付证书文件路径
     * @param certFilePath 证书文件路径
     * @return 路径地址
     */
    public String getCertFilePath(String certFilePath) {
        return getCertFile(certFilePath).getAbsolutePath();
    }


    /**
     * 获取支付证书文件
     * @param certFilePath 证书文件路径
     * @return 文件
     */
    public File getCertFile(String certFilePath){
        return null;
    }
}
