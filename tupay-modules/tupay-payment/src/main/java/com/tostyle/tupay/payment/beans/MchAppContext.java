package com.tostyle.tupay.payment.beans;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import com.tostyle.tupay.common.core.model.params.NormalMchParams;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.MchInfo;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 商户应用支付参数信息
 * 放置到内存， 避免多次查询操作
 * @author tostyle
 * 2022/4/18 10:23
 */
@Data
public class MchAppContext {

    /** 商户信息缓存 */
    private String mchNo;
    private String appId;
    private Long mchType;
    private MchInfo mchInfo;
    private MchApp mchApp;

    /** 商户支付配置信息缓存,  <接口代码, 支付参数>  */
    private Map<String, NormalMchParams> normalMchParamsMap = new HashMap<>();
    private Map<String, IsvSubMchParams> isvSubMchParamsMap = new HashMap<>();

    /** 放置所属服务商的信息 **/
    private IsvConfigContext isvConfigContext;


    /** 缓存支付宝client 对象 **/
    private AlipayClientWrapper alipayClientWrapper;

    /** 缓存 wxServiceWrapper 对象 **/
    private WxServiceWrapper wxServiceWrapper;

    /** 获取普通商户配置信息 **/
    public NormalMchParams getNormalMchParamsByChannelCode(String channelCode){
        return normalMchParamsMap.get(channelCode);
    }

    /** 获取isv配置信息 **/
    public <T> T getNormalMchParamsByChannelCode(String channelCode, Class<? extends NormalMchParams> cls){
        return (T)normalMchParamsMap.get(channelCode);
    }

    /** 获取特约商户配置信息 **/
    public IsvSubMchParams getIsvSubMchParamsByChannelCode(String channelCode){
        return isvSubMchParamsMap.get(channelCode);
    }

    /** 获取isv配置信息 **/
    public <T> T getIsvsubMchParamsByChannelCode(String channelCode, Class<? extends IsvSubMchParams> cls){
        return (T)isvSubMchParamsMap.get(channelCode);
    }

    /** 是否为 服务商特约商户 **/
    public boolean isIsvsubMch(){
        return this.mchType == PayConstant.TYPE_ISVSUB;
    }

    public AlipayClientWrapper getAlipayClientWrapper(){
        return isIsvsubMch() ? isvConfigContext.getAlipayClientWrapper(): alipayClientWrapper;
    }

    public WxServiceWrapper getWxServiceWrapper(){
        return isIsvsubMch() ? isvConfigContext.getWxServiceWrapper(): wxServiceWrapper;
    }

}
