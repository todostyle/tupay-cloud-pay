package com.tostyle.tupay.payment.beans;

import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.MchInfo;
import lombok.Data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 商户配置信息
 * 放置到内存， 避免多次查询操作
 * @author tostyle
 * 2022/4/18 15:51
 */
@Data
public class MchInfoContext {


    /** 商户信息缓存 */
    private String mchNo;
    private Long mchType;
    private MchInfo mchInfo;
    private Map<String, MchApp> appMap = new ConcurrentHashMap<>();

    /** 重置商户APP **/
    public void putMchApp(MchApp mchApp){
        appMap.put(mchApp.getAppId(), mchApp);
    }

    /** get商户APP **/
    public MchApp getMchApp(String appId){
        return appMap.get(appId);
    }
}
