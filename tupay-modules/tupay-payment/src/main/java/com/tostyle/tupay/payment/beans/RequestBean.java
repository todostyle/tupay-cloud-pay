package com.tostyle.tupay.payment.beans;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.enums.ApiCodeEnum;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.utils.ip.IpUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

/**
 * @author tostyle
 * 2022/4/18 9:42
 */
@Component
public class RequestBean {


    @Autowired(required = false)
    protected HttpServletRequest request;   //自动注入request

    /**
     * reqContext对象中的key: 转换好的json对象
     */
    private static final String REQUEST_CONTEXT_KEY_PARAM_JSON = "REQUEST_CONTEXT_KEY_PARAM_JSON";



    /** 获取json格式的请求参数 **/
    public JSONObject getReqParamJSON(){

        //将转换好的reqParam JSON格式的对象保存在当前请求上下文对象中进行保存；
        // 注意1： springMVC的CTRL默认单例模式， 不可使用局部变量保存，会出现线程安全问题；
        // 注意2： springMVC的请求模式为线程池，如果采用ThreadLocal保存对象信息，可能会出现不清空或者被覆盖的问题。
        Object reqParamObject = Objects.requireNonNull(RequestContextHolder.getRequestAttributes()).getAttribute(REQUEST_CONTEXT_KEY_PARAM_JSON, RequestAttributes.SCOPE_REQUEST);
        if(reqParamObject == null){
            JSONObject reqParam = reqParam2JSON();
            RequestContextHolder.getRequestAttributes().setAttribute(REQUEST_CONTEXT_KEY_PARAM_JSON, reqParam, RequestAttributes.SCOPE_REQUEST);
            return reqParam;
        }
        return (JSONObject) reqParamObject;
    }

    /**
     * 获取request参数，并转化为json格式
     * @return
     */
    public  JSONObject reqParam2JSON(){
        JSONObject returnObject = new JSONObject();
        if(isConvertJSON()){

            StringBuilder body = new StringBuilder();
            try {
                BufferedReader streamReader=new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
                String str;
                while((str = streamReader.readLine()) != null){
                    body.append(str);
                }
                if(StringUtils.isEmpty(body.toString())) {
                    return returnObject;
                }
                return JSONObject.parseObject(body.toString());

            } catch (Exception e) {
                throw new BizException(ApiCodeEnum.PARAMS_ERROR, "转换异常");
            }
        }
        // 参数Map
        Map properties = request.getParameterMap();
        // 返回值Map
        Iterator entries = properties.entrySet().iterator();
        Map.Entry entry;
        String name;
        String value = "";
        while (entries.hasNext()) {
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if(null == valueObj){
                value = "";
            }else if(valueObj instanceof String[]){
                String[] values = (String[])valueObj;
                for (String s : values) {
                    value = s + ",";
                }
                value = value.substring(0, value.length()-1);
            }else{
                value = valueObj.toString();
            }

            if(!name.contains("[")){
                returnObject.put(name, value);
                continue;
            }
            //添加对json对象解析的支持  example: {ps[abc] : 1}
            String mainKey = name.substring(0, name.indexOf("["));
            String subKey = name.substring(name.indexOf("[") + 1 , name.indexOf("]"));
            JSONObject subJson = new JSONObject();
            if(returnObject.get(mainKey) != null) {
                subJson = (JSONObject)returnObject.get(mainKey);
            }
            subJson.put(subKey, value);
            returnObject.put(mainKey, subJson);
        }
        return returnObject;
    }


    /** JSON 格式通过请求主体（BODY）传输  获取参数 **/
    public String getReqParamFromBody() {
        StringBuilder body = new StringBuilder();
        if(isConvertJSON()){
            try {
                BufferedReader streamReader=new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
                String str;
                while((str = streamReader.readLine()) != null){
                    body.append(str);
                }
                return body.toString();
            } catch (Exception e) {
                throw new BizException(ApiCodeEnum.PARAMS_ERROR, "转换异常");
            }
        }else {
            return body.toString();
        }
    }

    /**
     * 判断请求参数是否转换为json格式
     */
    private boolean isConvertJSON() {
        String contentType = request.getContentType();
        //有contentType  && json格式，  get请求不转换
        //application/json 需要转换为json格式；
        return contentType != null
                && contentType.toLowerCase().contains("application/json")
                && !request.getMethod().equalsIgnoreCase("GET");
    }

    /**
     * 获取客户端ip地址
     **/
    public String getClientIp() {
        return IpUtils.getIpAddr(request);
    }
}
