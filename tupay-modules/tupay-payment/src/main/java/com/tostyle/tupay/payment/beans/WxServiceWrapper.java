package com.tostyle.tupay.payment.beans;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayNormalMchParams;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.apache.commons.lang3.StringUtils;

/**
 * wxService 包装类
 * @author tostyle
 * 2022/4/18 10:51
 */
@Data
@AllArgsConstructor
public class WxServiceWrapper {

    /** 缓存微信API版本 **/
    private String apiVersion;

    /** 缓存 wxPayService 对象 **/
    private WxPayService wxPayService;

    /** 缓存 wxJavaService 对象 **/
    private WxMpService wxMpService;


    /**
     * 构建微信客户端
     * @param mchId
     * @param appId
     * @param appSecret
     * @param mchKey
     * @param apiVersion
     * @param apiV3Key
     * @param serialNo
     * @param cert
     * @param apiClientCert
     * @param apiClientKey
     * @return
     */
    public static WxServiceWrapper buildWxServiceWrapper(String mchId, String appId, String appSecret, String mchKey, String apiVersion, String apiV3Key,
                                                         String serialNo, String cert, String apiClientCert, String apiClientKey){

        WxPayConfig wxPayConfig = new WxPayConfig();
        wxPayConfig.setMchId(mchId);
        wxPayConfig.setAppId(appId);
        wxPayConfig.setMchKey(mchKey);
        if (PayConstant.PAY_IF_VERSION.WX_V2.equals(apiVersion)) { // 微信API  V2
            wxPayConfig.setSignType(WxPayConstants.SignType.MD5);
        }
        ChannelCertBean channelCertBean = SpringBeansUtil.getBean(ChannelCertBean.class);
        if(StringUtils.isNotBlank(apiV3Key)) {
            wxPayConfig.setApiV3Key(apiV3Key);
        }
        if(StringUtils.isNotBlank(serialNo)) {
            wxPayConfig.setCertSerialNo(serialNo);
        }
        if(channelCertBean!=null){
            if(StringUtils.isNotBlank(cert)){
                wxPayConfig.setKeyPath(channelCertBean.getCertFilePath(cert));
            }
            if(StringUtils.isNotBlank(apiClientCert)){
                wxPayConfig.setPrivateCertPath(channelCertBean.getCertFilePath(apiClientCert));
            }
            if(StringUtils.isNotBlank(apiClientKey)) {
                wxPayConfig.setPrivateKeyPath(channelCertBean.getCertFilePath(apiClientKey));
            }
        }
        WxPayService wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(wxPayConfig); //微信配置信息

        WxMpDefaultConfigImpl wxMpConfigStorage = new WxMpDefaultConfigImpl();
        wxMpConfigStorage.setAppId(appId);
        wxMpConfigStorage.setSecret(appSecret);

        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage); //微信配置信息

        return new WxServiceWrapper(apiVersion, wxPayService, wxMpService);
    }


    public static WxServiceWrapper buildWxServiceWrapper(WxpayIsvParams wxpayParams){
        //放置 wxJavaService
        return buildWxServiceWrapper(wxpayParams.getMchId(), wxpayParams.getAppId(),
                wxpayParams.getAppSecret(), wxpayParams.getKey(), wxpayParams.getApiVersion(), wxpayParams.getApiV3Key(),
                wxpayParams.getSerialNo(), wxpayParams.getCert(), wxpayParams.getApiClientCert(), wxpayParams.getApiClientKey());
    }

    public static WxServiceWrapper buildWxServiceWrapper(WxpayNormalMchParams wxpayParams){
        //放置 wxJavaService
        return buildWxServiceWrapper(wxpayParams.getMchId(), wxpayParams.getAppId(),
                wxpayParams.getAppSecret(), wxpayParams.getKey(), wxpayParams.getApiVersion(), wxpayParams.getApiV3Key(),
                wxpayParams.getSerialNo(), wxpayParams.getCert(), wxpayParams.getApiClientCert(), wxpayParams.getApiClientKey());
    }
}
