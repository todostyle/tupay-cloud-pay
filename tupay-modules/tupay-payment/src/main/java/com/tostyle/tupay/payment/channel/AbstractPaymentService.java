package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.beans.ApplicationConfig;
import com.tostyle.tupay.payment.beans.ChannelCertBean;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 支付接口抽象类
 * @author tostyle
 * 2022/4/18 17:50
 */
public abstract class AbstractPaymentService implements IPaymentService {

    @Autowired
    protected ContextService contextService;

    @Autowired
    protected ChannelCertBean channelCertBean;

    @Autowired
    protected  ApplicationConfig applicationConfig;


    /** 订单分账（一般用作 如微信订单将在下单处做标记） */
    protected boolean isDivisionOrder(PayOrder payOrder){
        //订单分账， 将冻结商户资金。
        return payOrder.getDivisionMode() != null && (PayConstant.DIVISION_MODE_AUTO == payOrder.getDivisionMode() || PayConstant.DIVISION_MODE_MANUAL == payOrder.getDivisionMode());
    }


    protected String getNotifyUrl(){
        return  applicationConfig.getPaySiteUrl()+"/api/pay/notify/" + getChannelCode();
    }

    protected String getNotifyUrl(String tradeNo){
        return applicationConfig.getPaySiteUrl() + "/api/pay/notify/" + getChannelCode() + "/" + tradeNo;
    }

    protected String getReturnUrl(){
        return applicationConfig.getPaySiteUrl() + "/api/pay/return/" + getChannelCode();
    }

    protected String getReturnUrl(String tradeNo){
        return applicationConfig.getPaySiteUrl() + "/api/pay/return/" + getChannelCode() + "/" + tradeNo;
    }
}
