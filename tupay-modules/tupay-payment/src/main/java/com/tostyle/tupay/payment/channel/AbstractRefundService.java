package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.payment.beans.ChannelCertBean;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 退款接口抽象类
 * @author tostyle
 * 2022/4/20 14:20
 */
public abstract class AbstractRefundService implements IRefundService{


    @Autowired
    protected ChannelCertBean channelCertBean;
    @Autowired
    protected ContextService contextService;

    protected String getNotifyUrl(){
        return "http://www.tostyle.fun" + "/api/refund/notify/" + getChannelCode();
    }

    protected String getNotifyUrl(String refundOrderId){
        return "http://www.tostyle.fun" + "/api/refund/notify/" + getChannelCode() + "/" + refundOrderId;
    }
}
