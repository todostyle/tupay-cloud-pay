package com.tostyle.tupay.payment.channel;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.payment.beans.MchAppContext;

/**
 * 301方式获取渠道侧用户ID， 如微信openId 支付宝的userId等
 * @author tostyle
 * 2022/4/20 14:13
 */
public interface IChannelUserService {
    /** 获取到接口code **/
    String getChannelCode();

    /** 获取重定向地址 **/
    String buildUserRedirectUrl(String callbackUrlEncode, MchAppContext mchAppContext);

    /** 获取渠道用户ID **/
    String getChannelUserId(JSONObject reqParams, MchAppContext mchAppContext);

}
