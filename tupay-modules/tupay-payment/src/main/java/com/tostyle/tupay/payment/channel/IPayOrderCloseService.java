package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.domain.PayOrder;

/**
 * 关闭订单（渠道侧）接口定义
 * @author tostyle
 * 2022/4/20 10:56
 */
public interface IPayOrderCloseService {

    /** 获取到接口code **/
    String getChannelCode();

    /** 查询订单 **/
    ChannelRetMsg close(PayOrder payOrder, MchAppContext mchAppContext) throws Exception;
}
