package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.domain.PayOrder;

/**
 * 查单（渠道侧）接口定义
 * @author tostyle
 * 2022/4/20 14:00
 */
public interface IPayOrderQueryService {

    /** 获取到接口code **/
    String getChannelCode();

    /** 查询订单 **/
    ChannelRetMsg query(PayOrder payOrder, MchAppContext mchAppContext) throws Exception;
}
