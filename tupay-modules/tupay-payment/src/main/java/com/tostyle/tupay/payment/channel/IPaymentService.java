package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;

/**
 * 调起上游渠道侧支付接口
 *
 * @author tostyle
 * 2022/4/18 14:09
 */
public interface IPaymentService {

    /**
     * 获取到接口code
     **/
    String getChannelCode();

    /**
     * 是否支持该支付方式
     */
    boolean isSupport(String wayCode);

    /**
     * 前置检查如参数等信息是否符合要求， 返回错误信息或直接抛出异常即可
     */
    String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder);

    /**
     * 调起支付接口，并响应数据；  内部处理普通商户和服务商模式
     **/
    AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception;
}
