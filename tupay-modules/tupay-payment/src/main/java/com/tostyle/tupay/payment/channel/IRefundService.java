package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.model.refund.RefundOrderReq;

/**
 * 调起上游渠道侧退款接口
 * @author tostyle
 * 2022/4/20 14:11
 */
public interface IRefundService {

    /** 获取到接口code **/
    String getChannelCode();

    /** 前置检查如参数等信息是否符合要求， 返回错误信息或直接抛出异常即可  */
    String preCheck(RefundOrderReq bizReq, RefundOrder refundOrder, PayOrder payOrder);

    /** 调起退款接口，并响应数据；  内部处理普通商户和服务商模式  **/
    ChannelRetMsg refund(RefundOrderReq bizReq, RefundOrder refundOrder, PayOrder payOrder, MchAppContext mchAppContext) throws Exception;

    /** 退款查单接口  **/
    ChannelRetMsg query(RefundOrder refundOrder, MchAppContext mchAppContext) throws Exception;
}
