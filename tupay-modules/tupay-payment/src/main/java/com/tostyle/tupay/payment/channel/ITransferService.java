package com.tostyle.tupay.payment.channel;

import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.transfer.TransferOrderReq;

/**
 * 转账接口
 * @author tostyle
 * 2022/4/20 14:10
 */
public interface ITransferService {

    /* 获取到接口code **/
    String getChannelCode();

    /** 是否支持该支付入账方式 */
    boolean isSupport(String entryType);

    /** 前置检查如参数等信息是否符合要求， 返回错误信息或直接抛出异常即可  */
    String preCheck(TransferOrderReq bizReq, TransferOrder transferOrder);

    /** 调起退款接口，并响应数据；  内部处理普通商户和服务商模式  **/
    ChannelRetMsg transfer(TransferOrderReq bizReq, TransferOrder transferOrder, MchAppContext mchAppContext) throws Exception;
}
