package com.tostyle.tupay.payment.channel.alipay;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.exception.ChannelException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayConfig;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayIsvParams;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayNormalMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IChannelUserService;
import com.tostyle.tupay.payment.service.ContextService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 14:25
 */
@Service
@Slf4j
public class AlipayChannelUserService implements IChannelUserService {

    @Autowired
    private ContextService contextService;


    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public String buildUserRedirectUrl(String callbackUrlEncode, MchAppContext mchAppContext) {
        String oauthUrl = AlipayConfig.PROD_OAUTH_URL;
        String appId = null;
        if(mchAppContext.isIsvsubMch()){
            AlipayIsvParams isvParams = (AlipayIsvParams) contextService.queryIsvParams(mchAppContext.getMchInfo().getIsvNo(), getChannelCode());
            if(isvParams == null) {
                throw new BizException("服务商支付宝接口没有配置！");
            }
            appId = isvParams.getAppId();
        }else{
            //获取商户配置信息
            AlipayNormalMchParams normalMchParams = (AlipayNormalMchParams) contextService.queryNormalMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
            if(normalMchParams == null) {
                throw new BizException("商户支付宝接口没有配置！");
            }
            appId = normalMchParams.getAppId();
            if(normalMchParams.getSandbox() != null && normalMchParams.getSandbox() == PayConstant.YES){
                oauthUrl = AlipayConfig.SANDBOX_OAUTH_URL;
            }
        }
        String alipayUserRedirectUrl = String.format(oauthUrl, appId, callbackUrlEncode);
        log.info("alipayUserRedirectUrl={}", alipayUserRedirectUrl);
        return alipayUserRedirectUrl;
    }

    @Override
    public String getChannelUserId(JSONObject reqParams, MchAppContext mchAppContext) {
        String authCode = reqParams.getString("auth_code");
        //通过code 换取openId
        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        request.setCode(authCode); request.setGrantType("authorization_code");
        try {
            return contextService.getAlipayClientWrapper(mchAppContext).execute(request).getUserId();
        } catch (ChannelException e) {
            e.printStackTrace();
            return null;
        }
    }
}
