package com.tostyle.tupay.payment.channel.alipay;

import com.alipay.api.domain.AlipayTradeCloseModel;
import com.alipay.api.request.AlipayTradeCloseRequest;
import com.alipay.api.response.AlipayTradeCloseResponse;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IPayOrderCloseService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 支付宝 关闭订单接口实现类
 * @author tostyle
 * 2022/4/20 10:57
 */
@Service
public class AlipayPayOrderCloseService implements IPayOrderCloseService {

    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public ChannelRetMsg close(PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        AlipayTradeCloseRequest req = new AlipayTradeCloseRequest();
        // 商户订单号，商户网站订单系统中唯一订单号，必填
        AlipayTradeCloseModel model = new AlipayTradeCloseModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        req.setBizModel(model);
        //通用字段
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        AlipayTradeCloseResponse resp = contextService.getAlipayClientWrapper(mchAppContext).execute(req);
        // 返回状态成功
        if (resp.isSuccess()) {
            return ChannelRetMsg.confirmSuccess(resp.getTradeNo());
        }else {
            return ChannelRetMsg.sysError(resp.getSubMsg());
        }
    }
}
