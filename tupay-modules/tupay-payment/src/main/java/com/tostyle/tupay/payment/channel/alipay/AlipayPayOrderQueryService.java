package com.tostyle.tupay.payment.channel.alipay;

import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IPayOrderQueryService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 支付宝 查单接口实现类
 * @author tostyle
 * 2022/4/20 13:59
 */
@Service
public class AlipayPayOrderQueryService implements IPayOrderQueryService {


    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public ChannelRetMsg query(PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        AlipayTradeQueryRequest req = new AlipayTradeQueryRequest();
        // 商户订单号，商户网站订单系统中唯一订单号，必填
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        req.setBizModel(model);
        //通用字段
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        AlipayTradeQueryResponse resp = contextService.getAlipayClientWrapper(mchAppContext).execute(req);
        String result = resp.getTradeStatus();
        if("TRADE_SUCCESS".equals(result)) {
            return ChannelRetMsg.confirmSuccess(resp.getTradeNo());  //支付成功
        }else if("WAIT_BUYER_PAY".equals(result)) {
            return ChannelRetMsg.waiting(); //支付中
        }
        return ChannelRetMsg.waiting(); //支付中
    }
}
