package com.tostyle.tupay.payment.channel.alipay;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.AbstractPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.util.PaywayUtil;
import org.springframework.stereotype.Service;

/**
 * 支付接口： 支付宝官方
 * 支付方式： 自适应
 * @author tostyle
 * 2022/4/18 17:56
 */
@Service
public class AlipayPaymentService extends AbstractPaymentService {
    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public boolean isSupport(String wayCode) {
        return true;
    }

    @Override
    public String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder) {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).preCheck(bizReq, payOrder);
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).pay(bizReq, payOrder, mchAppContext);
    }
}
