package com.tostyle.tupay.payment.channel.alipay;

import com.alipay.api.domain.AlipayTradeFastpayRefundQueryModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.AbstractRefundService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.model.refund.RefundOrderReq;

/**
 * 退款接口： 支付宝官方
 * @author tostyle
 * 2022/4/20 14:19
 */
public class AlipayRefundService extends AbstractRefundService {
    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public String preCheck(RefundOrderReq bizReq, RefundOrder refundOrder, PayOrder payOrder) {
        return null;
    }

    @Override
    public ChannelRetMsg refund(RefundOrderReq bizReq, RefundOrder refundOrder, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        AlipayTradeRefundModel model = new AlipayTradeRefundModel();
        model.setOutTradeNo(refundOrder.getTradeNo());
        model.setTradeNo(refundOrder.getChannelPayOrderNo());
        model.setOutRequestNo(refundOrder.getRefundOrderId());
        model.setRefundAmount(AmountUtil.convertCent2Dollar(refundOrder.getRefundAmount().toString()));
        model.setRefundReason(refundOrder.getRefundReason());
        request.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, request, model);
        AlipayTradeRefundResponse response = contextService.getAlipayClientWrapper(mchAppContext).execute(request);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        channelRetMsg.setChannelAttach(response.getBody());
        // 调用成功
        if(response.isSuccess()){
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
        }else{
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            channelRetMsg.setChannelErrCode(response.getSubCode());
            channelRetMsg.setChannelErrMsg(response.getSubMsg());
        }
        return channelRetMsg;
    }

    @Override
    public ChannelRetMsg query(RefundOrder refundOrder, MchAppContext mchAppContext) throws Exception {
        AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
        AlipayTradeFastpayRefundQueryModel model = new AlipayTradeFastpayRefundQueryModel();
        model.setTradeNo(refundOrder.getChannelPayOrderNo());
        model.setOutTradeNo(refundOrder.getTradeNo());
        model.setOutRequestNo(refundOrder.getRefundOrderId());
        request.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, request, model);
        AlipayTradeFastpayRefundQueryResponse response = contextService.getAlipayClientWrapper(mchAppContext).execute(request);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        channelRetMsg.setChannelAttach(response.getBody());
        // 调用成功 & 金额相等  （传入不存在的outRequestNo支付宝仍然返回响应成功只是数据不存在， 调用isSuccess() 仍是成功, 此处需判断金额是否相等）
        Long channelRefundAmount = response.getRefundAmount() == null ? null : Long.parseLong(AmountUtil.convertDollar2Cent(response.getRefundAmount()));
        if(response.isSuccess() && refundOrder.getRefundAmount().equals(channelRefundAmount)){
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
        }else{
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING); //认为是处理中
        }
        return channelRetMsg;
    }
}
