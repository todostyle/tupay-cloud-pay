package com.tostyle.tupay.payment.channel.alipay;

import com.alipay.api.domain.AlipayFundTransUniTransferModel;
import com.alipay.api.domain.Participant;
import com.alipay.api.request.AlipayFundTransUniTransferRequest;
import com.alipay.api.response.AlipayFundTransUniTransferResponse;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.ITransferService;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.transfer.TransferOrderReq;
import com.tostyle.tupay.payment.service.ContextService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 14:15
 */
@Service
@Slf4j
public class AlipayTransferService implements ITransferService {

    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.ALIPAY;
    }

    @Override
    public boolean isSupport(String entryType) {
        // 支付宝账户
        return PayConstant.ENTRY_ALIPAY_CASH.equals(entryType);
    }

    @Override
    public String preCheck(TransferOrderReq bizReq, TransferOrder transferOrder) {
        return null;
    }

    @Override
    public ChannelRetMsg transfer(TransferOrderReq bizReq, TransferOrder transferOrder, MchAppContext mchAppContext) throws Exception {
        AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();
        AlipayFundTransUniTransferModel model = new AlipayFundTransUniTransferModel();
        model.setTransAmount(AmountUtil.convertCent2Dollar(transferOrder.getAmount())); //转账金额，单位：元。
        model.setOutBizNo(transferOrder.getTransferId()); //商户转账唯一订单号
        model.setRemark(transferOrder.getTransferDesc()); //转账备注
        model.setProductCode("TRANS_ACCOUNT_NO_PWD");   // 销售产品码。单笔无密转账固定为 TRANS_ACCOUNT_NO_PWD
        model.setBizScene("DIRECT_TRANSFER");           // 业务场景 单笔无密转账固定为 DIRECT_TRANSFER。
        model.setOrderTitle("转账");                     // 转账业务的标题，用于在支付宝用户的账单里显示。
        model.setBusinessParams(transferOrder.getChannelExtra());   // 转账业务请求的扩展参数 {\"payer_show_name_use_alias\":\"xx公司\"}
        Participant accPayeeInfo = new Participant();
        accPayeeInfo.setName(StringUtils.defaultString(transferOrder.getAccountName(), null)); //收款方真实姓名
        accPayeeInfo.setIdentityType("ALIPAY_LOGON_ID");    //ALIPAY_USERID： 支付宝用户ID      ALIPAY_LOGONID:支付宝登录账号
        accPayeeInfo.setIdentity(transferOrder.getAccountNo()); //收款方账户
        model.setPayeeInfo(accPayeeInfo);
        request.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, request, model);
        // 调起支付宝接口
        AlipayFundTransUniTransferResponse response = contextService.getAlipayClientWrapper(mchAppContext).execute(request);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        channelRetMsg.setChannelAttach(response.getBody());
        // 调用成功
        if(response.isSuccess()){
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
            channelRetMsg.setChannelOrderId(response.getOrderId());
        }else{
            //若 系统繁忙， 无法确认结果
            if("SYSTEM_ERROR".equalsIgnoreCase(response.getSubCode())){
                return ChannelRetMsg.waiting();
            }
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            channelRetMsg.setChannelErrCode(response.getSubCode());
            channelRetMsg.setChannelErrMsg(response.getSubMsg());
        }
        return channelRetMsg;
    }
}
