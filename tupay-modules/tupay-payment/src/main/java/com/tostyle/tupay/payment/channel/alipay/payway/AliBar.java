package com.tostyle.tupay.payment.channel.alipay.payway;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.response.AlipayTradePayResponse;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.alipay.AlipayKit;
import com.tostyle.tupay.payment.channel.alipay.AlipayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliBarOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 支付宝 条码支付
 * @author tostyle
 * 2022/4/18 18:09
 */
@Service("alipayPaymentByAliBarService")
@Slf4j
public class AliBar extends AlipayPaymentService {



    @Override
    public String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder) {
        AliBarOrderReq aliBarOrderReq = (AliBarOrderReq) bizReq;
        if(StringUtils.isEmpty(aliBarOrderReq.getAuthCode())){
            throw new BizException("用户支付条码[authCode]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        AliBarOrderReq aliBarOrderReq = (AliBarOrderReq) bizReq;
        AlipayTradePayRequest req = new AlipayTradePayRequest();
        AlipayTradePayModel model = new AlipayTradePayModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        model.setScene("bar_code"); //条码支付 bar_code ; 声波支付 wave_code
        model.setAuthCode(aliBarOrderReq.getAuthCode().trim()); //支付授权码
        model.setSubject(payOrder.getSubject()); //订单标题
        model.setBody(payOrder.getBody()); //订单描述信息
        model.setTotalAmount(AmountUtil.convertCent2Dollar(payOrder.getAmount().toString()));  //支付金额
        req.setNotifyUrl(getNotifyUrl()); // 设置异步通知地址
        req.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        log.info("支付宝条码支付请求参数为：{}", JSON.toJSONString(req));
        //调起支付宝 （如果异常， 将直接跑出   ChannelException ）
        AlipayTradePayResponse alipayResp = contextService.getAlipayClientWrapper(mchAppContext).execute(req);
        log.info("支付宝条码支付响应结果为：{}", JSON.toJSONString(alipayResp));
        // 构造函数响应数据
        AliBarOrderResp aliBarOrderResp = ApiRespBuilder.buildSuccess(AliBarOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        aliBarOrderResp.setChannelRetMsg(channelRetMsg);
        //放置 响应数据
        channelRetMsg.setChannelAttach(alipayResp.getBody());
        channelRetMsg.setChannelOrderId(alipayResp.getTradeNo());
        channelRetMsg.setChannelUserId(alipayResp.getBuyerUserId()); //渠道用户标识
        // ↓↓↓↓↓↓ 调起接口成功后业务判断务必谨慎！！ 避免因代码编写bug，导致不能正确返回订单状态信息  ↓↓↓↓↓↓
        //当条码重复发起时，支付宝返回的code = 10003, subCode = null [等待用户支付], 此时需要特殊判断 = = 。
        if("10000".equals(alipayResp.getCode()) && alipayResp.isSuccess()){ //支付成功, 更新订单成功 || 等待支付宝的异步回调接口
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
        }else if("10003".equals(alipayResp.getCode())){ //10003 表示为 处理中, 例如等待用户输入密码
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        }else{  //其他状态, 表示下单失败
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            channelRetMsg.setChannelErrCode(AlipayKit.appendErrCode(alipayResp.getCode(), alipayResp.getSubCode()));
            channelRetMsg.setChannelErrMsg(AlipayKit.appendErrMsg(alipayResp.getMsg(), alipayResp.getSubMsg()));
        }
        return aliBarOrderResp;
    }
}
