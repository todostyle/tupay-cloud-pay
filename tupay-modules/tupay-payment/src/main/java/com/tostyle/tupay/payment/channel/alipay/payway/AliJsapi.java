package com.tostyle.tupay.payment.channel.alipay.payway;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alipay.api.domain.AlipayTradeCreateModel;
import com.alipay.api.request.AlipayTradeCreateRequest;
import com.alipay.api.response.AlipayTradeCreateResponse;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.alipay.AlipayKit;
import com.tostyle.tupay.payment.channel.alipay.AlipayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractReq;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliJsapiOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 支付宝 jsapi支付
 * @author tostyle
 * 2022/4/20 14:37
 */
@Service("alipayPaymentByJsapiService") //Service Name需保持全局唯一性
public class AliJsapi extends AlipayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder) {
        AliJsapiOrderReq bizRQ = (AliJsapiOrderReq) bizReq;
        if(StringUtils.isEmpty(bizRQ.getBuyerUserId())){
            throw new BizException("[buyerUserId]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception{
        AliJsapiOrderReq bizRQ = (AliJsapiOrderReq) bizReq;
        AlipayTradeCreateRequest req = new AlipayTradeCreateRequest();
        AlipayTradeCreateModel model = new AlipayTradeCreateModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        model.setSubject(payOrder.getSubject()); //订单标题
        model.setBody(payOrder.getBody()); //订单描述信息
        model.setTotalAmount(AmountUtil.convertCent2Dollar(payOrder.getAmount().toString()));  //支付金额
        model.setTimeoutExpress(bizRQ.getExpiredTime()!=null?bizRQ.getExpiredTime()+ PayConstant.ORDER_EXPIRE_TIME_UNIT: PayConstant.ORDER_EXPIRE_TIME);  // 订单超时时间
        model.setBuyerId(bizRQ.getBuyerUserId());
        req.setNotifyUrl(getNotifyUrl()); // 设置异步通知地址
        req.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        //调起支付宝 （如果异常， 将直接跑出   ChannelException ）
        AlipayTradeCreateResponse alipayResp = contextService.getAlipayClientWrapper(mchAppContext).execute(req);
        // 构造函数响应数据
        AliJsapiOrderResp bizResp = ApiRespBuilder.buildSuccess(AliJsapiOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        bizResp.setChannelRetMsg(channelRetMsg);
        //放置 响应数据
        channelRetMsg.setChannelAttach(alipayResp.getBody());
        // ↓↓↓↓↓↓ 调起接口成功后业务判断务必谨慎！！ 避免因代码编写bug，导致不能正确返回订单状态信息  ↓↓↓↓↓↓
        bizResp.setAlipayTradeNo(alipayResp.getTradeNo());
        channelRetMsg.setChannelOrderId(alipayResp.getTradeNo());
        if(alipayResp.isSuccess()){ //业务处理成功
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        }else{
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            channelRetMsg.setChannelErrCode(AlipayKit.appendErrCode(alipayResp.getCode(), alipayResp.getSubCode()));
            channelRetMsg.setChannelErrMsg(AlipayKit.appendErrMsg(alipayResp.getMsg(), alipayResp.getSubMsg()));
        }
        return bizResp;
    }
}
