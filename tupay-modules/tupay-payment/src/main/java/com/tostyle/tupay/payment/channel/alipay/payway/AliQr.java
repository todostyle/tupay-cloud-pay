package com.tostyle.tupay.payment.channel.alipay.payway;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.alipay.AlipayKit;
import com.tostyle.tupay.payment.channel.alipay.AlipayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliQrOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliQrOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.springframework.stereotype.Service;

/**
 * 支付宝 QR支付
 * @author tostyle
 * 2022/4/20 14:48
 */
@Service("alipayPaymentByAliQrService") //Service Name需保持全局唯一性
public class AliQr extends AlipayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext){
        AliQrOrderReq aliQrOrderRQ = (AliQrOrderReq)rq;
        AlipayTradePrecreateRequest req = new AlipayTradePrecreateRequest();
        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        model.setSubject(payOrder.getSubject()); //订单标题
        model.setBody(payOrder.getBody()); //订单描述信息
        model.setTotalAmount(AmountUtil.convertCent2Dollar(payOrder.getAmount().toString()));  //支付金额
        model.setTimeoutExpress(DateUtil.format(payOrder.getExpiredTime(), DatePattern.NORM_DATETIME_FORMAT));  // 订单超时时间
        req.setNotifyUrl(getNotifyUrl()); // 设置异步通知地址
        req.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        //调起支付宝 （如果异常， 将直接跑出   ChannelException ）
        AlipayTradePrecreateResponse alipayResp = contextService.getAlipayClientWrapper(mchAppContext).execute(req);
        // 构造函数响应数据
        AliQrOrderResp res = ApiRespBuilder.buildSuccess(AliQrOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        //放置 响应数据
        channelRetMsg.setChannelAttach(alipayResp.getBody());
        // ↓↓↓↓↓↓ 调起接口成功后业务判断务必谨慎！！ 避免因代码编写bug，导致不能正确返回订单状态信息  ↓↓↓↓↓↓
        if(alipayResp.isSuccess()){ //处理成功
            if(PayConstant.PAY_DATA_TYPE.CODE_IMG_URL.equals(aliQrOrderRQ.getPayDataType())){ //二维码地址
                //todo 这边需要查询
                //res.setCodeImgUrl(sysConfigService.getDBApplicationConfig().genScanImgUrl(alipayResp.getQrCode()));
                res.setCodeImgUrl("http://www.tostyle.fun");
            }else{ //默认都为跳转地址方式
                res.setCodeUrl(alipayResp.getQrCode());
            }
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        }else{  //其他状态, 表示下单失败
            res.setOrderState(PayConstant.STATE_FAIL);  //支付失败
            channelRetMsg.setChannelErrCode(AlipayKit.appendErrCode(alipayResp.getCode(), alipayResp.getSubCode()));
            channelRetMsg.setChannelErrMsg(AlipayKit.appendErrMsg(alipayResp.getMsg(), alipayResp.getSubMsg()));
        }
        return res;
    }

}
