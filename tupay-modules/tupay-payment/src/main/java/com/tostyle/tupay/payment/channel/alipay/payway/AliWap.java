package com.tostyle.tupay.payment.channel.alipay.payway;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.tostyle.tupay.common.core.exception.ChannelException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.alipay.AlipayKit;
import com.tostyle.tupay.payment.channel.alipay.AlipayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliWapOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliWapOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 14:53
 */
@Service("alipayPaymentByAliWapService") //Service Name需保持全局唯一性
public class AliWap extends AlipayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext){
        AliWapOrderReq bizReq = (AliWapOrderReq)rq;
        AlipayTradeWapPayRequest req = new AlipayTradeWapPayRequest();
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setOutTradeNo(payOrder.getTradeNo());
        model.setSubject(payOrder.getSubject()); //订单标题
        model.setBody(payOrder.getBody()); //订单描述信息
        model.setTotalAmount(AmountUtil.convertCent2Dollar(payOrder.getAmount().toString()));  //支付金额
        model.setTimeExpire(DateUtil.format(payOrder.getExpiredTime(), DatePattern.NORM_DATETIME_FORMAT));  // 订单超时时间
        model.setProductCode("QUICK_WAP_PAY");
        req.setNotifyUrl(getNotifyUrl()); // 设置异步通知地址
        req.setReturnUrl(getReturnUrl()); // 同步跳转地址
        req.setBizModel(model);
        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppContext, req, model);
        // 构造函数响应数据
        AliWapOrderResp res = ApiRespBuilder.buildSuccess(AliWapOrderResp.class);
        try {
            if(PayConstant.PAY_DATA_TYPE.FORM.equals(bizReq.getPayDataType())){ //表单方式
                res.setFormContent(contextService.getAlipayClientWrapper(mchAppContext).getAlipayClient().pageExecute(req).getBody());
            }else if (PayConstant.PAY_DATA_TYPE.CODE_IMG_URL.equals(bizReq.getPayDataType())){ //二维码图片地址
                String payUrl = contextService.getAlipayClientWrapper(mchAppContext).getAlipayClient().pageExecute(req, "GET").getBody();
                res.setCodeImgUrl(applicationConfig.genScanImgUrl(payUrl));
            }else{ // 默认都为 payUrl方式
                res.setPayUrl(contextService.getAlipayClientWrapper(mchAppContext).getAlipayClient().pageExecute(req, "GET").getBody());
            }
        }catch (AlipayApiException e) {
            throw ChannelException.sysError(e.getMessage());
        }
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        //放置 响应数据
        channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        return res;
    }
}
