package com.tostyle.tupay.payment.channel.wxpay;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayNormalMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.IChannelUserService;
import com.tostyle.tupay.payment.service.ContextService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 15:23
 */
@Service
@Slf4j
public class WxpayChannelUserService implements IChannelUserService {

    @Autowired
    private ContextService contextService;

    /** 默认官方跳转地址 **/
    private static final String DEFAULT_OAUTH_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.WXPAY;
    }

    @Override
    public String buildUserRedirectUrl(String callbackUrlEncode, MchAppContext mchAppContext) {
        String appId = null;
        String oauth2Url = "";
        if(mchAppContext.isIsvsubMch()){
            WxpayIsvParams wxpayIsvParams = (WxpayIsvParams)contextService.queryIsvParams(mchAppContext.getMchInfo().getIsvNo(), getChannelCode());
            if(wxpayIsvParams == null) {
                throw new BizException("服务商微信支付接口没有配置！");
            }
            appId = wxpayIsvParams.getAppId();
            oauth2Url = wxpayIsvParams.getOauth2Url();
        }else{
            //获取商户配置信息
            WxpayNormalMchParams normalMchParams = (WxpayNormalMchParams)contextService.queryNormalMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
            if(normalMchParams == null) {
                throw new BizException("商户微信支付接口没有配置！");
            }
            appId = normalMchParams.getAppId();
            oauth2Url = normalMchParams.getOauth2Url();
        }
        if(StringUtils.isBlank(oauth2Url)){
            oauth2Url = DEFAULT_OAUTH_URL;
        }
        String wxUserRedirectUrl = String.format(oauth2Url + "?appid=%s&scope=snsapi_base&state=&redirect_uri=%s&response_type=code#wechat_redirect", appId, callbackUrlEncode);
        log.info("wxUserRedirectUrl={}", wxUserRedirectUrl);
        return wxUserRedirectUrl;
    }

    @Override
    public String getChannelUserId(JSONObject reqParams, MchAppContext mchAppContext) {
        String code = reqParams.getString("code");
        try {
            WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
            return wxServiceWrapper.getWxMpService().getOAuth2Service().getAccessToken(code).getOpenId();
        } catch (WxErrorException e) {
            e.printStackTrace();
            return null;
        }
    }
}
