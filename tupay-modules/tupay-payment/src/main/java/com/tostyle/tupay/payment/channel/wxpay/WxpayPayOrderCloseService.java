package com.tostyle.tupay.payment.channel.wxpay;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.request.WxPayOrderCloseRequest;
import com.github.binarywang.wxpay.bean.result.WxPayOrderCloseResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvSubMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.IPayOrderCloseService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信关闭订单
 * @author tostyle
 * 2022/4/20 15:20
 */
@Service
public class WxpayPayOrderCloseService implements IPayOrderCloseService {

    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.WXPAY;
    }

    @Override
    public ChannelRetMsg close(PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        try {
            WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
            if (PayConstant.PAY_IF_VERSION.WX_V2.equals(wxServiceWrapper.getApiVersion())) {  //V2
                WxPayOrderCloseRequest req = new WxPayOrderCloseRequest();
                //放置isv信息
                WxpayKit.putApiIsvInfo(mchAppContext, req);
                req.setOutTradeNo(payOrder.getTradeNo());
                WxPayService wxPayService = wxServiceWrapper.getWxPayService();
                WxPayOrderCloseResult result = wxPayService.closeOrder(req);
                if("SUCCESS".equals(result.getResultCode())){ //关闭订单成功
                    return ChannelRetMsg.confirmSuccess(null);
                }else if("FAIL".equals(result.getResultCode())){ //关闭订单失败
                    return ChannelRetMsg.confirmFail(); //关闭失败
                }else{
                    return ChannelRetMsg.waiting(); //关闭中
                }
            }else if (PayConstant.PAY_IF_VERSION.WX_V3.equals(wxServiceWrapper.getApiVersion())) {   //V3
                String reqUrl;
                JSONObject reqJson = new JSONObject();
                if(mchAppContext.isIsvsubMch()){ // 特约商户
                    WxpayIsvSubMchParams isvsubMchParams = (WxpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
                    reqUrl = String.format("/v3/pay/partner/transactions/out-trade-no/%s/close", payOrder.getTradeNo());
                    reqJson.put("sp_mchid", wxServiceWrapper.getWxPayService().getConfig().getMchId());
                    reqJson.put("sub_mchid", isvsubMchParams.getSubMchId());
                }else {
                    reqUrl = String.format("/v3/pay/transactions/out-trade-no/%s/close", payOrder.getTradeNo());
                    reqJson.put("mchid", wxServiceWrapper.getWxPayService().getConfig().getMchId());
                }
                WxpayV3Util.closeOrderV3(reqUrl, reqJson, wxServiceWrapper.getWxPayService());
                return ChannelRetMsg.confirmSuccess(null);
            }
            return ChannelRetMsg.confirmFail();
        } catch (WxPayException e) {
            return ChannelRetMsg.sysError(e.getReturnMsg());
        } catch (Exception e) {
            return ChannelRetMsg.sysError(e.getMessage());
        }
    }
}
