package com.tostyle.tupay.payment.channel.wxpay;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.request.WxPayOrderQueryRequest;
import com.github.binarywang.wxpay.bean.result.WxPayOrderQueryResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvSubMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.IPayOrderQueryService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信查单接口
 * @author tostyle
 * 2022/4/20 15:16
 */
@Service
public class WxpayPayOrderQueryService implements IPayOrderQueryService {

    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.WXPAY;
    }

    @Override
    public ChannelRetMsg query(PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        try {
            WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
            if (PayConstant.PAY_IF_VERSION.WX_V2.equals(wxServiceWrapper.getApiVersion())) {  //V2
                WxPayOrderQueryRequest req = new WxPayOrderQueryRequest();
                //放置isv信息
                WxpayKit.putApiIsvInfo(mchAppContext, req);
                req.setOutTradeNo(payOrder.getTradeNo());
                WxPayService wxPayService = wxServiceWrapper.getWxPayService();
                WxPayOrderQueryResult result = wxPayService.queryOrder(req);
                if("SUCCESS".equals(result.getTradeState())){ //支付成功
                    return ChannelRetMsg.confirmSuccess(result.getTransactionId());
                }else if("USERPAYING".equals(result.getTradeState())){ //支付中，等待用户输入密码
                    return ChannelRetMsg.waiting(); //支付中
                }else if("CLOSED".equals(result.getTradeState())
                        || "REVOKED".equals(result.getTradeState())
                        || "PAYERROR".equals(result.getTradeState())){  //CLOSED—已关闭， REVOKED—已撤销(刷卡支付), PAYERROR--支付失败(其他原因，如银行返回失败)
                    return ChannelRetMsg.confirmFail(); //支付失败
                }else{
                    return ChannelRetMsg.unknown();
                }
            }else if (PayConstant.PAY_IF_VERSION.WX_V3.equals(wxServiceWrapper.getApiVersion())) {   //V3
                String reqUrl;
                String query;
                if(mchAppContext.isIsvsubMch()){ // 特约商户
                    WxpayIsvSubMchParams isvsubMchParams = (WxpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
                    reqUrl = String.format("/v3/pay/partner/transactions/out-trade-no/%s", payOrder.getTradeNo());
                    query = String.format("?sp_mchid=%s&sub_mchid=%s", wxServiceWrapper.getWxPayService().getConfig().getMchId(), isvsubMchParams.getSubMchId());
                }else {
                    reqUrl = String.format("/v3/pay/transactions/out-trade-no/%s", payOrder.getTradeNo());
                    query = String.format("?mchid=%s", wxServiceWrapper.getWxPayService().getConfig().getMchId());
                }
                JSONObject resultJSON = WxpayV3Util.queryOrderV3(reqUrl + query, wxServiceWrapper.getWxPayService());
                String channelState = resultJSON.getString("trade_state");
                if ("SUCCESS".equals(channelState)) {
                    return ChannelRetMsg.confirmSuccess(resultJSON.getString("transaction_id"));
                }else if("USERPAYING".equals(channelState)){ //支付中，等待用户输入密码
                    return ChannelRetMsg.waiting(); //支付中
                }else if("CLOSED".equals(channelState)
                        || "REVOKED".equals(channelState)
                        || "PAYERROR".equals(channelState)){  //CLOSED—已关闭， REVOKED—已撤销(刷卡支付), PAYERROR--支付失败(其他原因，如银行返回失败)
                    return ChannelRetMsg.confirmFail(); //支付失败
                }else{
                    return ChannelRetMsg.unknown();
                }

            }else {
                return ChannelRetMsg.unknown();
            }

        } catch (WxPayException e) {
            return ChannelRetMsg.sysError(e.getReturnMsg());
        } catch (Exception e) {
            return ChannelRetMsg.sysError(e.getMessage());
        }
    }
}
