package com.tostyle.tupay.payment.channel.wxpay;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvSubMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.AbstractPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.util.PaywayUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 支付接口： 微信官方 支付方式： 自适应
 * @author tostyle
 * 2022/4/20 14:58
 */
@Service
public class WxpayPaymentService extends AbstractPaymentService {
    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.WXPAY;
    }

    @Override
    public boolean isSupport(String wayCode) {
        return true;
    }

    @Override
    public String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder) {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).preCheck(bizReq, payOrder);
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        // 微信API版本
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        String apiVersion = wxServiceWrapper.getApiVersion();
        if (PayConstant.PAY_IF_VERSION.WX_V2.equals(apiVersion)) {
            return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).pay(bizReq, payOrder, mchAppContext);
        } else if (PayConstant.PAY_IF_VERSION.WX_V3.equals(apiVersion)) {
            return PaywayUtil.getRealPaywayV3Service(this, payOrder.getWayCode()).pay(bizReq, payOrder, mchAppContext);
        } else {
            throw new BizException("不支持的微信支付API版本");
        }
    }


    /**
     * 构建微信统一下单请求数据
     * @param payOrder
     * @return
     */
    public WxPayUnifiedOrderRequest buildUnifiedOrderRequest(PayOrder payOrder, MchAppContext mchAppContext) {
        String payOrderId = payOrder.getTradeNo();
        // 微信统一下单请求对象
        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
        request.setOutTradeNo(payOrderId);
        request.setBody(payOrder.getSubject());
        request.setDetail(payOrder.getBody());
        request.setFeeType("CNY");
        request.setTotalFee(payOrder.getAmount().intValue());
        request.setSpbillCreateIp(payOrder.getClientIp());
        request.setNotifyUrl(getNotifyUrl());
        request.setProductId(System.currentTimeMillis()+"");
        request.setTimeExpire(DateUtil.format(payOrder.getExpiredTime(), DatePattern.PURE_DATETIME_PATTERN));
        //订单分账， 将冻结商户资金。
        if(isDivisionOrder(payOrder)){
            request.setProfitSharing("Y");
        }
        // 特约商户
        if(mchAppContext.isIsvsubMch()){
            WxpayIsvSubMchParams isvsubMchParams = (WxpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
            request.setSubMchId(isvsubMchParams.getSubMchId());
            if (StringUtils.isNotBlank(isvsubMchParams.getSubMchAppId())) {
                request.setSubAppId(isvsubMchParams.getSubMchAppId());
            }
        }
        return request;
    }

    /**
     * 构建微信APIV3接口  统一下单请求数据
     * @param payOrder
     * @return
     */
    public JSONObject buildV3OrderRequest(PayOrder payOrder, MchAppContext mchAppContext) {
        String payOrderId = payOrder.getTradeNo();
        // 微信统一下单请求对象
        JSONObject reqJSON = new JSONObject();
        reqJSON.put("out_trade_no", payOrderId);
        reqJSON.put("description", payOrder.getSubject());
        // 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE,示例值：2018-06-08T10:34:56+08:00
        reqJSON.put("time_expire", String.format("%sT%s+08:00", DateUtil.format(payOrder.getExpiredTime(), DatePattern.NORM_DATE_FORMAT), DateUtil.format(payOrder.getExpiredTime(), DatePattern.NORM_TIME_FORMAT)));
        reqJSON.put("notify_url", getNotifyUrl(payOrderId));
        JSONObject amount = new JSONObject();
        amount.put("total", payOrder.getAmount().intValue());
        amount.put("currency", "CNY");
        reqJSON.put("amount", amount);
        JSONObject sceneInfo = new JSONObject();
        sceneInfo.put("payer_client_ip", payOrder.getClientIp());
        reqJSON.put("scene_info", sceneInfo);
        //订单分账， 将冻结商户资金。
        if(isDivisionOrder(payOrder)){
            JSONObject settleInfo = new JSONObject();
            settleInfo.put("profit_sharing", true);
            reqJSON.put("settle_info", settleInfo);
        }
        WxPayService wxPayService = contextService.getWxServiceWrapper(mchAppContext).getWxPayService();
        if(mchAppContext.isIsvsubMch()){ // 特约商户
            WxpayIsvSubMchParams isvsubMchParams = (WxpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), getChannelCode());
            reqJSON.put("sp_appid", wxPayService.getConfig().getAppId());
            reqJSON.put("sp_mchid", wxPayService.getConfig().getMchId());
            reqJSON.put("sub_mchid", isvsubMchParams.getSubMchId());
            if (StringUtils.isNotBlank(isvsubMchParams.getSubMchAppId())) {
                reqJSON.put("sub_appid", isvsubMchParams.getSubMchAppId());
            }
        }else { // 普通商户
            reqJSON.put("appid", wxPayService.getConfig().getAppId());
            reqJSON.put("mchid", wxPayService.getConfig().getMchId());
        }
        return reqJSON;
    }
}
