package com.tostyle.tupay.payment.channel.wxpay;

import com.github.binarywang.wxpay.bean.entpay.EntPayRequest;
import com.github.binarywang.wxpay.bean.entpay.EntPayResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.ITransferService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.transfer.TransferOrderReq;
import com.tostyle.tupay.payment.service.ContextService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 转账接口： 微信官方
 * @author tostyle
 * 2022/4/20 15:04
 */
@Slf4j
@Service
public class WxpayTransferService implements ITransferService {

    @Autowired
    private ContextService contextService;

    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.WXPAY;
    }

    @Override
    public boolean isSupport(String entryType) {
        // 微信仅支持 零钱 和 银行卡入账方式
        return PayConstant.ENTRY_WX_CASH.equals(entryType) || PayConstant.ENTRY_BANK_CARD.equals(entryType);
    }

    @Override
    public String preCheck(TransferOrderReq bizReq, TransferOrder transferOrder) {
        if(transferOrder.getMchType() == PayConstant.MCH_TYPE_ISVSUB){
            return "微信子商户暂不支持转账业务";
        }
        return null;
    }

    @Override
    public ChannelRetMsg transfer(TransferOrderReq bizReq, TransferOrder transferOrder, MchAppContext mchAppContext) throws Exception {
        try {
            EntPayRequest request = new EntPayRequest();
            WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
            request.setMchAppid(wxServiceWrapper.getWxPayService().getConfig().getAppId());  // 商户账号appid
            request.setMchId(wxServiceWrapper.getWxPayService().getConfig().getMchId());  //商户号
            request.setPartnerTradeNo(transferOrder.getTransferId()); //商户订单号
            request.setOpenid(transferOrder.getAccountNo()); //openid
            request.setAmount(transferOrder.getAmount().intValue()); //付款金额，单位为分
            request.setSpbillCreateIp(transferOrder.getClientIp());
            request.setDescription(transferOrder.getTransferDesc()); //付款备注
            if(StringUtils.isNotEmpty(transferOrder.getAccountName())){
                request.setReUserName(transferOrder.getAccountName());
                request.setCheckName("FORCE_CHECK");
            }else{
                request.setCheckName("NO_CHECK");
            }
            EntPayResult entPayResult = wxServiceWrapper.getWxPayService().getEntPayService().entPay(request);
            // SUCCESS/FAIL，注意：当状态为FAIL时，存在业务结果未明确的情况。如果状态为FAIL，请务必关注错误代码（err_code字段），通过查询接口确认此次付款的结果。
            if("SUCCESS".equalsIgnoreCase(entPayResult.getResultCode())){
                return ChannelRetMsg.confirmSuccess(entPayResult.getPaymentNo());
            }else{
                return ChannelRetMsg.waiting();
            }
        } catch (WxPayException e) {
            //出现未明确的错误码时（SYSTEMERROR等），请务必用原商户订单号重试，或通过查询接口确认此次付款的结果。
            if("SYSTEMERROR".equalsIgnoreCase(e.getErrCode())){
                return ChannelRetMsg.waiting();
            }
            return ChannelRetMsg.confirmFail(null,
                    WxpayKit.appendErrCode(e.getReturnMsg(), e.getErrCode()),
                    WxpayKit.appendErrMsg(e.getReturnMsg(), StringUtils.defaultIfEmpty(e.getErrCodeDes(), e.getCustomErrorMsg())));
        } catch (Exception e) {
            log.error("转账异常：", e);
            return ChannelRetMsg.waiting();
        }
    }
}
