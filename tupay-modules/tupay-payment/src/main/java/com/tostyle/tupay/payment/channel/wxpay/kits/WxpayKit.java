package com.tostyle.tupay.payment.channel.wxpay.kits;

import com.github.binarywang.wxpay.bean.request.BaseWxPayRequest;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvSubMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * 【微信支付】支付通道工具包
 * @author tostyle
 * 2022/4/20 15:08
 */
public class WxpayKit {

    /** 放置 isv特殊信息 **/
    public static void putApiIsvInfo(MchAppContext mchAppContext, BaseWxPayRequest req){
        //不是特约商户， 无需放置此值
        if(!mchAppContext.isIsvsubMch()){
            return ;
        }
        ContextService contextService = SpringBeansUtil.getBean(ContextService.class);
        WxpayIsvSubMchParams isvsubMchParams =
                (WxpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), PayConstant.CHANNEL_CODE.WXPAY);
        req.setSubMchId(isvsubMchParams.getSubMchId());
        req.setSubAppId(isvsubMchParams.getSubMchAppId());
    }

    public static String appendErrCode(String code, String subCode){
        return StringUtils.defaultIfEmpty(subCode, code); //优先： subCode
    }

    public static String appendErrMsg(String msg, String subMsg){
        if(StringUtils.isNotEmpty(msg) && StringUtils.isNotEmpty(subMsg) ){
            return msg + "【" + subMsg + "】";
        }
        return StringUtils.defaultIfEmpty(subMsg, msg);
    }

    public static void commonSetErrInfo(ChannelRetMsg channelRetMsg, WxPayException wxPayException){
        channelRetMsg.setChannelErrCode(appendErrCode( wxPayException.getReturnCode(), wxPayException.getErrCode() ));
        channelRetMsg.setChannelErrMsg(appendErrMsg( "OK".equalsIgnoreCase(wxPayException.getReturnMsg()) ? null : wxPayException.getReturnMsg(), wxPayException.getErrCodeDes() ));
        // 如果仍然为空
        if(StringUtils.isEmpty(channelRetMsg.getChannelErrMsg())){
            channelRetMsg.setChannelErrMsg(StringUtils.defaultIfEmpty(wxPayException.getCustomErrorMsg(), wxPayException.getMessage()));
        }

    }
}
