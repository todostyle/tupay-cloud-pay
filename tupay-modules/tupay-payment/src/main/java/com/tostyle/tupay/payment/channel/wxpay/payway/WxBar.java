package com.tostyle.tupay.payment.channel.wxpay.payway;

import com.github.binarywang.wxpay.bean.request.WxPayMicropayRequest;
import com.github.binarywang.wxpay.bean.result.WxPayMicropayResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxBarOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 微信 bar
 * @author tostyle
 * 2022/4/20 15:41
 */
@Service("wxpayPaymentByBarService") //Service Name需保持全局唯一性
public class WxBar extends WxpayPaymentService {


    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        WxBarOrderReq bizRQ = (WxBarOrderReq) rq;
        if(StringUtils.isEmpty(bizRQ.getAuthCode())){
            throw new BizException("用户支付条码[authCode]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception{

        WxBarOrderReq bizReq = (WxBarOrderReq) rq;

        // 微信统一下单请求对象
        WxPayMicropayRequest request = new WxPayMicropayRequest();
        request.setOutTradeNo(payOrder.getTradeNo());
        request.setBody(payOrder.getSubject());
        request.setDetail(payOrder.getBody());
        request.setFeeType("CNY");
        request.setTotalFee(payOrder.getAmount().intValue());
        request.setSpbillCreateIp(payOrder.getClientIp());
        request.setAuthCode(bizReq.getAuthCode().trim());
        //订单分账， 将冻结商户资金。
        if(isDivisionOrder(payOrder)){
            request.setProfitSharing("Y");
        }
        //放置isv信息
        WxpayKit.putApiIsvInfo(mchAppContext, request);
        // 构造函数响应数据
        WxBarOrderResp res = ApiRespBuilder.buildSuccess(WxBarOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        try {
            WxPayMicropayResult wxPayMicropayResult = wxPayService.micropay(request);

            channelRetMsg.setChannelOrderId(wxPayMicropayResult.getTransactionId());
            channelRetMsg.setChannelUserId(wxPayMicropayResult.getOpenid());
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);

        } catch (WxPayException e) {
            //微信返回支付状态为【支付结果未知】, 需进行查单操作
            if("SYSTEMERROR".equals(e.getErrCode()) || "USERPAYING".equals(e.getErrCode()) ||  "BANKERROR".equals(e.getErrCode())){
                //轮询查询订单
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
                channelRetMsg.setNeedQuery(true);
            }else {
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
                WxpayKit.commonSetErrInfo(channelRetMsg, e);
            }
        }
        return res;
    }
}
