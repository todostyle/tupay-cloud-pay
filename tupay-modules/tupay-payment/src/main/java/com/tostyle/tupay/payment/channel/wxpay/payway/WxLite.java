package com.tostyle.tupay.payment.channel.wxpay.payway;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 微信 小程序支付
 * @author tostyle
 * 2022/4/20 15:50
 */
@Service("wxpayPaymentByLiteService") //Service Name需保持全局唯一性
@Slf4j
public class WxLite extends WxpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        WxJsapiOrderReq bizReq = (WxJsapiOrderReq) rq;
        if(StringUtils.isEmpty(bizReq.getOpenid())){
            throw new BizException("[openid]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception{
        WxJsapiOrderReq bizReq = (WxJsapiOrderReq) rq;
        WxPayUnifiedOrderRequest req = buildUnifiedOrderRequest(payOrder, mchAppContext);
        req.setTradeType(WxPayConstants.TradeType.JSAPI);
        if(mchAppContext.isIsvsubMch() && StringUtils.isNotBlank(req.getSubAppId())){ // 特约商户 && 传了子商户appId
            req.setSubOpenid(bizReq.getOpenid()); // 用户在子商户appid下的唯一标识
        }else {
            req.setOpenid(bizReq.getOpenid());
        }
        // 构造函数响应数据
        WxJsapiOrderResp res = ApiRespBuilder.buildSuccess(WxJsapiOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        try {
            WxPayMpOrderResult payResult = wxPayService.createOrder(req);
            JSONObject resJSON = (JSONObject) JSON.toJSON(payResult);
            resJSON.put("package", payResult.getPackageValue());
            res.setPayInfo(resJSON.toJSONString());
            // 支付中
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        } catch (WxPayException e) {
            log.error("WxPayException:", e);
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }
        return res;
    }

}
