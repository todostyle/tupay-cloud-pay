package com.tostyle.tupay.payment.channel.wxpay.paywayV3;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxAppOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 15:56
 */
@Service("wxpayPaymentByAppV3Service") //Service Name需保持全局唯一性
public class WxAppV3 extends WxpayPaymentService {


    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) {
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        wxPayService.getConfig().setTradeType(WxPayConstants.TradeType.APP);
        // 构造请求数据
        JSONObject reqJSON = buildV3OrderRequest(payOrder, mchAppContext);
        // wxPayConfig 添加子商户参数
        if(mchAppContext.isIsvsubMch()){
            wxPayService.getConfig().setSubMchId(reqJSON.getString("sub_mchid"));
            if (StringUtils.isNotBlank(reqJSON.getString("sub_appid"))) {
                wxPayService.getConfig().setSubAppId(reqJSON.getString("sub_appid"));
            }
        }
        String reqUrl;  // 请求地址
        if(mchAppContext.isIsvsubMch()){ // 特约商户
            reqUrl = WxpayV3Util.ISV_URL_MAP.get(WxPayConstants.TradeType.APP);
        }else {
            reqUrl = WxpayV3Util.NORMALMCH_URL_MAP.get(WxPayConstants.TradeType.APP);
        }
        // 构造函数响应数据
        WxAppOrderResp res = ApiRespBuilder.buildSuccess(WxAppOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        try {
            JSONObject resJSON = WxpayV3Util.unifiedOrderV3(reqUrl, reqJSON, wxPayService);
            res.setPayInfo(resJSON.toJSONString());
            // 支付中
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);

        } catch (WxPayException e) {
            //明确失败
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }
        return res;
    }

}
