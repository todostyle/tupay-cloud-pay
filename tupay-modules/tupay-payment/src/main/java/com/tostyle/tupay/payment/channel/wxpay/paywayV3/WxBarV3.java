package com.tostyle.tupay.payment.channel.wxpay.paywayV3;

import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信 条码支付
 * @author tostyle
 * 2022/4/20 15:58
 */
@Service("wxpayPaymentByBarV3Service") //Service Name需保持全局唯一性
public class WxBarV3 extends WxpayPaymentService {

    @Autowired
    private com.tostyle.tupay.payment.channel.wxpay.payway.WxBar wxpayPaymentByBarService;

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return wxpayPaymentByBarService.preCheck(rq, payOrder);
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        return wxpayPaymentByBarService.pay(rq, payOrder, mchAppContext);
    }
}
