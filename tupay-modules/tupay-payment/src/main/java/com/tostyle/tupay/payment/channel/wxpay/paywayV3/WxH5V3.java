package com.tostyle.tupay.payment.channel.wxpay.paywayV3;

import cn.hutool.core.codec.Base64;
import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxH5OrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxH5OrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 16:01
 */
@Service("wxpayPaymentByH5V3Service") //Service Name需保持全局唯一性
public class WxH5V3 extends WxpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) {
        WxH5OrderReq bizRQ = (WxH5OrderReq) rq;
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        wxPayService.getConfig().setTradeType(WxPayConstants.TradeType.MWEB);
        // 构造请求数据
        JSONObject reqJSON = buildV3OrderRequest(payOrder, mchAppContext);
        JSONObject sceneInfo = reqJSON.getJSONObject("scene_info");
        JSONObject h5Info = new JSONObject();
        h5Info.put("type", "iOS, Android, Wap");
        sceneInfo.put("h5_info", h5Info);
        reqJSON.put("scene_info", sceneInfo);
        String reqUrl;  // 请求地址
        if(mchAppContext.isIsvsubMch()){ // 特约商户
            reqUrl = WxpayV3Util.ISV_URL_MAP.get(WxPayConstants.TradeType.MWEB);
        }else {
            reqUrl = WxpayV3Util.NORMALMCH_URL_MAP.get(WxPayConstants.TradeType.MWEB);
        }
        // 构造函数响应数据
        WxH5OrderResp res = ApiRespBuilder.buildSuccess(WxH5OrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        try {
            JSONObject resJSON = WxpayV3Util.unifiedOrderV3(reqUrl, reqJSON, wxPayService);
            String payUrl = resJSON.getString("h5_url");
            payUrl = applicationConfig.getPaySiteUrl() + "/api/common/payUrl/" + Base64.encode(payUrl);
            if (PayConstant.PAY_DATA_TYPE.CODE_IMG_URL.equals(bizRQ.getPayDataType())){ //二维码图片地址
                res.setCodeImgUrl(applicationConfig.genScanImgUrl(payUrl));
            }else{ // 默认都为 payUrl方式
                res.setPayUrl(payUrl);
            }
            // 支付中
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        } catch (WxPayException e) {
            //明确失败
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }
        return res;
    }


}
