package com.tostyle.tupay.payment.channel.wxpay.paywayV3;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 微信 jsapi支付
 * @author tostyle
 * 2022/4/20 16:04
 */
@Service("wxpayPaymentByJsapiV3Service") //Service Name需保持全局唯一性
public class WxJsapiV3 extends WxpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        // 使用的是V2接口的预先校验
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception{

        WxJsapiOrderReq bizReq = (WxJsapiOrderReq) rq;
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        wxPayService.getConfig().setTradeType(WxPayConstants.TradeType.JSAPI);
        // 构造请求数据
        JSONObject reqJSON = buildV3OrderRequest(payOrder, mchAppContext);
        // wxPayConfig 添加子商户参数
        if(mchAppContext.isIsvsubMch()){
            wxPayService.getConfig().setSubMchId(reqJSON.getString("sub_mchid"));
            if (StringUtils.isNotBlank(reqJSON.getString("sub_appid"))) {
                wxPayService.getConfig().setSubAppId(reqJSON.getString("sub_appid"));
            }
        }
        String reqUrl;
        if(mchAppContext.isIsvsubMch()){ // 特约商户
            reqUrl = WxpayV3Util.ISV_URL_MAP.get(WxPayConstants.TradeType.JSAPI);
            reqJSON.put("payer", WxpayV3Util.processIsvPayer(reqJSON.getString("sub_appid"), bizReq.getOpenid()));
        }else {
            reqUrl = WxpayV3Util.NORMALMCH_URL_MAP.get(WxPayConstants.TradeType.JSAPI);
            JSONObject payer = new JSONObject();
            payer.put("openid", bizReq.getOpenid());
            reqJSON.put("payer", payer);
        }
        // 构造函数响应数据
        WxJsapiOrderResp res = ApiRespBuilder.buildSuccess(WxJsapiOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        try {
            JSONObject resJSON = WxpayV3Util.unifiedOrderV3(reqUrl, reqJSON, wxPayService);
            res.setPayInfo(resJSON.toJSONString());
            // 支付中
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        } catch (WxPayException e) {
            //明确失败
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }
        return res;
    }
}
