package com.tostyle.tupay.payment.channel.wxpay.paywayV3;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.channel.wxpay.WxpayPaymentService;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayKit;
import com.tostyle.tupay.payment.channel.wxpay.kits.WxpayV3Util;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxNativeOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxNativeOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.springframework.stereotype.Service;

/**
 * 微信 native支付
 * @author tostyle
 * 2022/4/20 16:09
 */
@Service("wxpayPaymentByNativeV3Service") //Service Name需保持全局唯一性
public class WxNativeV3 extends WxpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) {
        WxNativeOrderReq bizReq = (WxNativeOrderReq) rq;
        WxServiceWrapper wxServiceWrapper = contextService.getWxServiceWrapper(mchAppContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        // 构造请求数据
        JSONObject reqJSON = buildV3OrderRequest(payOrder, mchAppContext);
        wxPayService.getConfig().setTradeType(WxPayConstants.TradeType.NATIVE);
        String reqUrl;
        if(mchAppContext.isIsvsubMch()){ // 特约商户
            reqUrl = WxpayV3Util.ISV_URL_MAP.get(WxPayConstants.TradeType.NATIVE);
        }else {
            reqUrl = WxpayV3Util.NORMALMCH_URL_MAP.get(WxPayConstants.TradeType.NATIVE);
        }
        // 构造函数响应数据
        WxNativeOrderResp res = ApiRespBuilder.buildSuccess(WxNativeOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        try {
            JSONObject resJSON = WxpayV3Util.unifiedOrderV3(reqUrl, reqJSON, wxPayService);
            String codeUrl = resJSON.getString("code_url");
            if (PayConstant.PAY_DATA_TYPE.CODE_IMG_URL.equals(bizReq.getPayDataType())){ //二维码图片地址
                res.setCodeImgUrl(applicationConfig.genScanImgUrl(codeUrl));
            }else{
                res.setCodeUrl(codeUrl);
            }
            // 支付中
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
        } catch (WxPayException e) {
            //明确失败
            channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }
        return res;
    }
}
