package com.tostyle.tupay.payment.channel.ysfpay;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.ysf.YsfpayConfig;
import com.tostyle.tupay.common.core.model.params.ysf.YsfpayIsvParams;
import com.tostyle.tupay.common.core.model.params.ysf.YsfpayIsvSubMchParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.AbstractPaymentService;
import com.tostyle.tupay.payment.channel.ysfpay.utils.YsfHttpUtil;
import com.tostyle.tupay.payment.channel.ysfpay.utils.YsfSignUtils;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.util.PaywayUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 云闪付下单
 * @author tostyle
 * 2022/4/20 16:16
 */
@Service
@Slf4j
public class YsfpayPaymentService extends AbstractPaymentService {
    @Override
    public String getChannelCode() {
        return PayConstant.CHANNEL_CODE.YSFPAY;
    }

    @Override
    public boolean isSupport(String wayCode) {
        return true;
    }

    @Override
    public String preCheck(UnifiedOrderReq bizReq, PayOrder payOrder) {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).preCheck(bizReq, payOrder);
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq bizReq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).pay(bizReq, payOrder, mchAppContext);
    }

    /** 封装参数 & 统一请求 **/
    public JSONObject packageParamAndReq(String apiUri, JSONObject reqParams, String logPrefix, MchAppContext mchAppConfigContext) throws Exception {

        YsfpayIsvParams isvParams = (YsfpayIsvParams)contextService.queryIsvParams(mchAppConfigContext.getMchInfo().getIsvNo(), getChannelCode());

        if (isvParams.getSerProvId() == null) {
            log.error("服务商配置为空：isvParams：{}", isvParams);
            throw new BizException("服务商配置为空。");
        }

        reqParams.put("serProvId", isvParams.getSerProvId()); //云闪付服务商标识
        YsfpayIsvSubMchParams isvsubMchParams = (YsfpayIsvSubMchParams) contextService.queryIsvSubMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), getChannelCode());
        reqParams.put("merId", isvsubMchParams.getMerId()); // 商户号

        //签名
        String isvPrivateCertFile = channelCertBean.getCertFilePath(isvParams.getIsvPrivateCertFile());
        String isvPrivateCertPwd = isvParams.getIsvPrivateCertPwd();
        reqParams.put("signature", YsfSignUtils.signBy256(reqParams, isvPrivateCertFile, isvPrivateCertPwd)); //RSA 签名串

        // 调起上游接口
        log.info("{} reqJSON={}", logPrefix, reqParams);
        String resText = YsfHttpUtil.doPostJson(getYsfpayHost4env(isvParams) + apiUri, null, reqParams);
        log.info("{} resJSON={}", logPrefix, resText);

        if(StringUtils.isEmpty(resText)){
            return null;
        }
        return JSONObject.parseObject(resText);
    }

    /** 获取云闪付正式环境/沙箱HOST地址   **/
    public static String getYsfpayHost4env(YsfpayIsvParams isvParams){
        return PayConstant.YES == isvParams.getSandbox() ? YsfpayConfig.SANDBOX_SERVER_URL : YsfpayConfig.PROD_SERVER_URL;
    }

    /** 云闪付 jsapi下单请求统一发送参数 **/
    public static void jsapiParamsSet(JSONObject reqParams, PayOrder payOrder, String notifyUrl, String returnUrl) {
        String orderType = YsfHttpUtil.getOrderTypeByJSapi(payOrder.getWayCode());
        reqParams.put("orderType", orderType); //订单类型： alipayJs-支付宝， wechatJs-微信支付， upJs-银联二维码
        ysfPublicParams(reqParams, payOrder);
        reqParams.put("backUrl", notifyUrl); //交易通知地址
        reqParams.put("frontUrl", returnUrl); //前台通知地址
    }

    /** 云闪付 bar下单请求统一发送参数 **/
    public static void barParamsSet(JSONObject reqParams, PayOrder payOrder) {
        String orderType = YsfHttpUtil.getOrderTypeByBar(payOrder.getWayCode());
        reqParams.put("orderType", orderType); //订单类型： alipay-支付宝， wechat-微信支付， -unionpay银联二维码
        ysfPublicParams(reqParams, payOrder);
        // TODO 终端编号暂时写死
        reqParams.put("termId", "01727367"); // 终端编号
    }

    /** 云闪付公共参数赋值 **/
    public static void ysfPublicParams(JSONObject reqParams, PayOrder payOrder) {
        //获取订单类型
        reqParams.put("orderNo", payOrder.getTradeNo()); //订单号
        reqParams.put("orderTime", DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN)); //订单时间 如：20180702142900
        reqParams.put("txnAmt", payOrder.getAmount()); //交易金额 单位：分，不带小数点
        reqParams.put("currencyCode", "156"); //交易币种 不出现则默认为人民币-156
        reqParams.put("orderInfo", payOrder.getSubject()); //订单信息 订单描述信息，如：京东生鲜食品
    }
}
