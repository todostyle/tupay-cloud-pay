package com.tostyle.tupay.payment.channel.ysfpay.payway;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.ysfpay.YsfpayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliBarOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 云闪付 支付宝 条码支付
 * @author tostyle
 * 2022/4/20 16:45
 */
@Service("ysfPaymentByAliBarService") //Service Name需保持全局唯一性
public class YsfAliBar extends YsfpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {
        AliBarOrderReq bizReq = (AliBarOrderReq) rq;
        if(StringUtils.isEmpty(bizReq.getAuthCode())){
            throw new BizException("用户支付条码[authCode]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        String logPrefix = "【云闪付条码(alipay)支付】";
        AliBarOrderReq bizRQ = (AliBarOrderReq) rq;
        AliBarOrderResp res = ApiRespBuilder.buildSuccess(AliBarOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        JSONObject reqParams = new JSONObject();
        reqParams.put("authCode", bizRQ.getAuthCode().trim()); //付款码： 用户 APP 展示的付款条码或二维码
        // 云闪付 bar 统一参数赋值
        barParamsSet(reqParams, payOrder);
        //客户端IP
        reqParams.put("termInfo", "{\"ip\": \""+StringUtils.defaultIfEmpty(payOrder.getClientIp(), "127.0.0.1")+"\"}"); //终端信息
        // 发送请求
        JSONObject resJSON = packageParamAndReq("/gateway/api/pay/micropay", reqParams, logPrefix, mchAppContext);
        //请求 & 响应成功， 判断业务逻辑
        String respCode = resJSON.getString("respCode"); //应答码
        String respMsg = resJSON.getString("respMsg"); //应答信息
        try {
            //00-交易成功， 02-用户支付中 , 12-交易重复， 需要发起查询处理    其他认为失败
            if("00".equals(respCode)){
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS);
                res.setPayData(resJSON.getString("payData"));
            }else if("02".equals(respCode) ||"12".equals(respCode) || "99".equals(respCode)){
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
                channelRetMsg.setNeedQuery(true); // 开启轮询查单
            }else{
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
                channelRetMsg.setChannelErrCode(respCode);
                channelRetMsg.setChannelErrMsg(respMsg);
            }
        }catch (Exception e) {
            channelRetMsg.setChannelErrCode(respCode);
            channelRetMsg.setChannelErrMsg(respMsg);
        }
        return res;
    }
}
