package com.tostyle.tupay.payment.channel.ysfpay.payway;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvParams;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.ysfpay.YsfpayPaymentService;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderResp;
import com.tostyle.tupay.payment.util.ApiRespBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 云闪付 微信jsapi
 * @author tostyle
 * 2022/4/20 16:52
 */
@Service("ysfpayPaymentByWxJsapiService") //Service Name需保持全局唯一性
public class YsfWxJsapi extends YsfpayPaymentService {

    @Override
    public String preCheck(UnifiedOrderReq rq, PayOrder payOrder) {

        WxJsapiOrderReq bizRQ = (WxJsapiOrderReq) rq;
        if(StringUtils.isEmpty(bizRQ.getOpenid())){
            throw new BizException("[openId]不可为空");
        }
        return null;
    }

    @Override
    public AbstractResp pay(UnifiedOrderReq rq, PayOrder payOrder, MchAppContext mchAppContext) throws Exception {
        String logPrefix = "【云闪付(wechatJs)jsapi支付】";
        JSONObject reqParams = new JSONObject();
        WxJsapiOrderResp res = ApiRespBuilder.buildSuccess(WxJsapiOrderResp.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);
        // 请求参数赋值
        jsapiParamsSet(reqParams, payOrder, getNotifyUrl(), getReturnUrl());
        WxJsapiOrderReq bizReq = (WxJsapiOrderReq) rq;
        //云闪付扫一扫支付， 需要传入openId参数
        reqParams.put("userId", bizReq.getOpenid()); // openId
        //客户端IP
        reqParams.put("customerIp", StringUtils.defaultIfEmpty(payOrder.getClientIp(), "127.0.0.1"));
        // 获取微信官方配置 的appId
        WxpayIsvParams wxpayIsvParams = (WxpayIsvParams)contextService.queryIsvParams(mchAppContext.getMchInfo().getIsvNo(),getChannelCode());
        reqParams.put("subAppId", wxpayIsvParams.getAppId()); //用户ID
        // 发送请求并返回订单状态
        JSONObject resJSON = packageParamAndReq("/gateway/api/pay/unifiedorder", reqParams, logPrefix, mchAppContext);
        //请求 & 响应成功， 判断业务逻辑
        String respCode = resJSON.getString("respCode"); //应答码
        String respMsg = resJSON.getString("respMsg"); //应答信息
        try {
            //00-交易成功， 02-用户支付中 , 12-交易重复， 需要发起查询处理    其他认为失败
            if("00".equals(respCode)){
                //付款信息
                res.setPayInfo(resJSON.getString("payData"));
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);
            }else{
                channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.CONFIRM_FAIL);
                channelRetMsg.setChannelErrCode(respCode);
                channelRetMsg.setChannelErrMsg(respMsg);
            }
        }catch (Exception e) {
            channelRetMsg.setChannelErrCode(respCode);
            channelRetMsg.setChannelErrMsg(respMsg);
        }
        return res;
    }
}
