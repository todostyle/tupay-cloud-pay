package com.tostyle.tupay.payment.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.controller.api.base.BaseApiController;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import com.tostyle.tupay.payment.domain.model.AbstractReq;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.ValidateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tostyle
 * 2022/4/18 9:13
 */
public abstract class ApiController extends BaseApiController {


    @Autowired
    private ValidateService validateService;

    @Autowired
    private ContextService contextService;


    /** 获取请求参数并转换为对象，通用验证  **/
    protected <T extends AbstractReq> T getReq(Class<T> cls){
        T bizRQ = getObject(cls);
        // [1]. 验证通用字段规则
        validateService.validate(bizRQ);
        return bizRQ;
    }


    /**
     * 获取请求参数并转换为对象，商户通用验证
     **/
    protected <T extends AbstractReq> T getReqByWithMchSign(Class<T> cls) {
        //获取请求Req, and 通用验证
        T bizReq = getReq(cls);
        AbstractMchAppReq abstractMchAppReq = (AbstractMchAppReq) bizReq;
        //业务校验， 包括： 验签， 商户状态是否可用， 是否支持该支付方式下单等。
        String mchNo = abstractMchAppReq.getMchNo();
        String appId = abstractMchAppReq.getAppId();
        String sign = bizReq.getSign();
        if (StringUtils.isAnyBlank(mchNo, appId, sign)) {
            throw new BizException("参数有误！");
        }
        MchAppContext mchAppContext = contextService.queryMchInfoAndAppInfo(mchNo, appId);
        if(mchAppContext == null){
            throw new BizException("商户或商户应用不存在");
        }
        if(mchAppContext.getMchApp() == null || mchAppContext.getMchInfo().getState() != PayConstant.YES){
            throw new BizException("商户信息不存在或商户状态不可用");
        }
        MchApp mchApp = mchAppContext.getMchApp();
        if(mchApp == null || mchApp.getState() != PayConstant.YES){
            throw new BizException("商户应用不存在或应用状态不可用");
        }
        if(!mchApp.getMchNo().equals(mchNo)){
            throw new BizException("参数appId与商户号不匹配");
        }
        // 验签
        String appSecret = mchApp.getAppSecret();
        // 转换为 JSON
        JSONObject bizReqJSON = (JSONObject)JSONObject.toJSON(bizReq);
        bizReqJSON.remove("sign");
        if(!sign.equalsIgnoreCase(Md5Utils.getSign(bizReqJSON, appSecret))){
            throw new BizException("验签失败");
        }
        return bizReq;
    }


    /** 获取客户端ip地址 **/
    protected String getClientIp() {
        String ipAddress = null;
        ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) {
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

}
