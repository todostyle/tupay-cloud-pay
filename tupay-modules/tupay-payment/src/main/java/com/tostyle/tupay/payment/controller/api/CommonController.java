package com.tostyle.tupay.payment.controller.api;

import cn.hutool.core.codec.Base64;
import com.tostyle.tupay.payment.controller.api.base.BaseApiController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tostyle
 * 2022/4/18 11:19
 */
@Slf4j
@Controller
@RequestMapping("/api/common")
public class CommonController extends BaseApiController {


    /**
     * 跳转到支付页面(适合网关支付form表单输出)
     * @param payData
     * @return
     */
    @RequestMapping(value = "/payForm/{payData}")
    private String toPayForm(@PathVariable("payData") String payData){
        request.setAttribute("payHtml", Base64.decodeStr(payData));
        return "common/toPay";
    }


    /**
     * 跳转到支付页面(适合微信H5跳转与referer一致)
     * @param payData
     * @return
     */
    @RequestMapping(value = "/payUrl/{payData}")
    private String toPayUrl(@PathVariable("payData") String payData) {
        String payUrl = Base64.decodeStr(payData);
        request.setAttribute("payHtml", "<script>window.location.href = '"+payUrl+"';</script>");
        return "common/toPay";
    }

}
