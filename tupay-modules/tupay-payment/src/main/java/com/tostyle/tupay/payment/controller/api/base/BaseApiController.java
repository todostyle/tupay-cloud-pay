package com.tostyle.tupay.payment.controller.api.base;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.enums.ApiCodeEnum;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.payment.beans.RequestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 抽象公共控制层
 *
 * @author tostyle
 * 2022/4/18 9:13
 */
public abstract class BaseApiController {


    @Autowired
    protected HttpServletRequest request;   //自动注入request

    @Autowired
    protected HttpServletResponse response;  //自动注入response

    @Autowired
    protected RequestBean requestBean;

    /**
     * 获取json格式的请求参数
     **/
    protected JSONObject getReqParamJSON() {
        return requestBean.getReqParamJSON();
    }

    /**
     * 获取对象类型
     **/
    protected <T> T getObject(Class<T> clazz) {
        JSONObject paramJSON = getReqParamJSON();
        return paramJSON.toJavaObject(clazz);
    }

    protected String getValStringRequired(String key) {
        return getValRequired(key, String.class);
    }

    /** 获取请求参数值 [ T 类型 ], [ 必填 ] **/
    protected <T> T getValRequired(String key, Class<T> cls) {
        T value = getVal(key, cls);
        if(ObjectUtils.isEmpty(value)) {
            throw new BizException(ApiCodeEnum.PARAMS_ERROR, genParamRequiredMsg(key));
        }
        return value;
    }

    /** 获取请求参数值 [ T 类型 ], [ 非必填 ] **/
    protected <T> T getVal(String key, Class<T> cls) {
        return getReqParamJSON().getObject(key, cls);
    }

    /** 生成参数必填错误信息 **/
    private String genParamRequiredMsg(String key) {
        return "参数" + key + "必填";
    }
}
