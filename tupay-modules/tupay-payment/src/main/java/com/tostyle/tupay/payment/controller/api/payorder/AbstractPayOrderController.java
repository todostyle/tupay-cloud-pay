package com.tostyle.tupay.payment.controller.api.payorder;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.exception.ChannelException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.AmountUtil;
import com.tostyle.tupay.common.core.utils.IdUtils;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.beans.ApplicationConfig;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IPaymentService;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.MchInfo;
import com.tostyle.tupay.payment.domain.MchPayChannel;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import com.tostyle.tupay.payment.domain.model.payorder.juhepay.QrCashierOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.juhepay.QrCashierOrderResp;
import com.tostyle.tupay.payment.service.*;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 创建支付订单抽象类
 *
 * @author tostyle
 * 2022/4/18 11:22
 */
@Slf4j
public abstract class AbstractPayOrderController extends ApiController {

    @Autowired
    protected IPayOrderService payOrderService;
    @Autowired
    protected ContextService contextService;
    @Autowired
    protected IMchPayChannelService mchPayChannelService;
    @Autowired
    protected PayOrderProcessService payOrderProcessService;
    @Autowired
    protected ApplicationConfig applicationConfig;

    /**
     * 统一下单 (新建订单模式)
     *
     * @param wayCode 支付方式
     * @param bizReq  请求参数
     * @return ApiResp
     */
    protected ApiResp unifiedOrder(String wayCode, UnifiedOrderReq bizReq) {
        return unifiedOrder(wayCode, bizReq, null);
    }

    /**
     * 统一下单
     *
     * @param wayCode  支付方式
     * @param bizReq   请求参数
     * @param payOrder 订单信息
     * @return
     */
    protected ApiResp unifiedOrder(String wayCode, UnifiedOrderReq bizReq, PayOrder payOrder) {
        // 响应数据
        UnifiedOrderResp bizResp = null;
        //是否新订单模式 [  一般接口都为新订单模式，  由于QR_CASHIER支付方式，需要先 在DB插入一个新订单， 导致此处需要特殊判断下。 如果已存在则直接更新，否则为插入。  ]
        boolean isNewOrder = payOrder == null;
        try {
            if (payOrder != null) { //当订单存在时，封装公共参数。
                if (payOrder.getState() != PayConstant.STATE_INIT) {
                    throw new BizException("订单状态异常");
                }
                payOrder.setWayCode(wayCode); // 需要将订单更新 支付方式
                payOrder.setChannelUser(bizReq.getChannelUserId()); //更新渠道用户信息
                bizReq.setMchNo(payOrder.getMchNo());
                bizReq.setAppId(payOrder.getAppId());
                bizReq.setMchOrderNo(payOrder.getMchOrderNo());
                bizReq.setWayCode(wayCode);
                bizReq.setAmount(payOrder.getAmount());
                bizReq.setCurrency(payOrder.getCurrency());
                bizReq.setClientIp(payOrder.getClientIp());
                bizReq.setSubject(payOrder.getSubject());
                bizReq.setNotifyUrl(payOrder.getNotifyUrl());
                bizReq.setReturnUrl(payOrder.getReturnUrl());
                bizReq.setChannelExtra(payOrder.getChannelExtra());
                bizReq.setExtParam(payOrder.getExtParam());
                bizReq.setDivisionMode(payOrder.getDivisionMode());
            }
            String mchNo = bizReq.getMchNo();
            String appId = bizReq.getAppId();
            // 只有新订单模式，进行校验
            if (isNewOrder && payOrderService.checkMchOrder(mchNo, bizReq.getMchOrderNo())) {
                throw new BizException("商户订单[" + bizReq.getMchOrderNo() + "]已存在");
            }
            if (StringUtils.isNotEmpty(bizReq.getNotifyUrl()) && !StringUtils.isAvailableUrl(bizReq.getNotifyUrl())) {
                throw new BizException("异步通知地址协议仅支持http:// 或 https:// !");
            }
            if (StringUtils.isNotEmpty(bizReq.getReturnUrl()) && !StringUtils.isAvailableUrl(bizReq.getReturnUrl())) {
                throw new BizException("同步通知地址协议仅支持http:// 或 https:// !");
            }
            //获取支付参数 (缓存数据) 和 商户信息
            MchAppContext mchAppContext = contextService.queryMchInfoAndAppInfo(mchNo, appId);
            if (mchAppContext == null) {
                throw new BizException("获取商户应用信息失败");
            }
            MchInfo mchInfo = mchAppContext.getMchInfo();
            MchApp mchApp = mchAppContext.getMchApp();
            //收银台支付并且只有新订单需要走这里，  收银台二次下单的wayCode应该为实际支付方式。
            if (isNewOrder && PayConstant.PAY_WAY_CODE.QR_CASHIER.equals(wayCode)) {
                //生成订单
                payOrder = genPayOrder(bizReq, mchInfo, mchApp, null, null);
                String tradeNo = payOrder.getTradeNo();
                //订单入库 订单状态： 生成状态  此时没有和任何上游渠道产生交互。
                payOrderService.insertPayOrder(payOrder);
                QrCashierOrderResp qrCashierOrderResp = new QrCashierOrderResp();
                QrCashierOrderReq qrCashierOrderReq = (QrCashierOrderReq) bizReq;
                String payUrl = applicationConfig.genUniJsapiPayUrl(tradeNo);
                if (PayConstant.PAY_DATA_TYPE.CODE_IMG_URL.equals(qrCashierOrderReq.getPayDataType())) { //二维码地址
                    qrCashierOrderResp.setCodeImgUrl(applicationConfig.genScanImgUrl(payUrl));
                } else { //默认都为跳转地址方式
                    qrCashierOrderResp.setPayUrl(payUrl);
                }
                return packageApiResByPayOrder(bizReq, qrCashierOrderResp, payOrder);
            }
            // 根据支付方式， 查询出 该商户 可用的支付接口
            MchPayChannel mchPayChannel = mchPayChannelService.getMchPayChannel(mchAppContext.getMchNo(), mchAppContext.getAppId(), wayCode);
            if (mchPayChannel == null) {
                throw new BizException("商户应用不支持该支付方式");
            }
            //获取支付接口
            IPaymentService paymentService = checkMchWayCodeAndGetService(mchAppContext, mchPayChannel);
            String channelCode = paymentService.getChannelCode();
            //生成订单
            if (isNewOrder) {
                payOrder = genPayOrder(bizReq, mchInfo, mchApp, channelCode, mchPayChannel);
            } else {
                payOrder.setChannelCode(channelCode);
                // 查询支付方式的费率，并 在更新ing时更新费率信息
                payOrder.setMchFeeRate(mchPayChannel.getRate());
                payOrder.setMchFeeAmount(AmountUtil.calPercentageFee(payOrder.getAmount(), payOrder.getMchFeeRate())); //商户手续费,单位分
            }
            //预先校验
            String errMsg = paymentService.preCheck(bizReq, payOrder);
            if (StringUtils.isNotEmpty(errMsg)) {
                throw new BizException(errMsg);
            }
            if (isNewOrder) {
                //订单入库 订单状态： 生成状态  此时没有和任何上游渠道产生交互。
                payOrderService.insertPayOrder(payOrder);
            }
            //调起上游支付接口
            bizResp = (UnifiedOrderResp) paymentService.pay(bizReq, payOrder, mchAppContext);
            //处理上游返回数据
            this.processChannelMsg(bizResp.getChannelRetMsg(), payOrder);
            return packageApiResByPayOrder(bizReq, bizResp, payOrder);
        } catch (BizException e) {
            return ApiResp.customFail(e.getMessage());
        } catch (ChannelException e) {
            //处理上游返回数据
            this.processChannelMsg(e.getChannelRetMsg(), payOrder);

            if (e.getChannelRetMsg().getChannelState() == ChannelRetMsg.ChannelState.SYS_ERROR) {
                return ApiResp.customFail(e.getMessage());
            }
            return this.packageApiResByPayOrder(bizReq, bizResp, payOrder);
        } catch (Exception e) {
            return ApiResp.customFail("系统异常");
        }

    }
    /**
     * 校验： 商户的支付方式是否可用
     * 返回： 支付接口
     * **/
    private IPaymentService checkMchWayCodeAndGetService(MchAppContext mchAppContext, MchPayChannel mchPayChannel){
        // 接口代码
        String channelCode = mchPayChannel.getChannelCode();
        IPaymentService paymentService = SpringBeansUtil.getBean(channelCode + "PaymentService", IPaymentService.class);
        if(paymentService == null){
            throw new BizException("无此支付通道接口");
        }
        if(!paymentService.isSupport(mchPayChannel.getWayCode())){
            throw new BizException("接口不支持该支付方式");
        }
        if(mchAppContext.getMchType() == PayConstant.TYPE_NORMAL){ //普通商户
            if(contextService.queryNormalMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), channelCode) == null){
                throw new BizException("商户应用参数未配置");
            }
        }else if(mchAppContext.getMchType() == PayConstant.TYPE_ISVSUB){ //特约商户
            if(contextService.queryIsvSubMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), channelCode) == null){
                throw new BizException("特约商户参数未配置");
            }
            if(contextService.queryIsvParams(mchAppContext.getMchInfo().getIsvNo(), channelCode) == null){
                throw new BizException("服务商参数未配置");
            }
        }
        return paymentService;
    }

    /**
     * 处理返回的渠道信息，并更新订单状态
     * payOrder将对部分信息进行 赋值操作。
     **/
    private void processChannelMsg(ChannelRetMsg channelRetMsg, PayOrder payOrder) {
        //对象为空 || 上游返回状态为空， 则无需操作
        if(channelRetMsg == null || channelRetMsg.getChannelState() == null){
            return ;
        }
        String tradeNo = payOrder.getTradeNo();
        //明确成功
        if(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS == channelRetMsg.getChannelState()) {
            this.updateInitOrderStateThrowException(PayConstant.STATE_SUCCESS, payOrder, channelRetMsg);
            //订单支付成功，其他业务逻辑
            payOrderProcessService.confirmSuccess(payOrder);
            //明确失败
        }else if(ChannelRetMsg.ChannelState.CONFIRM_FAIL == channelRetMsg.getChannelState()) {
            this.updateInitOrderStateThrowException(PayConstant.STATE_FAIL, payOrder, channelRetMsg);
            // 上游处理中 || 未知 || 上游接口返回异常  订单为支付中状态
        }else if( ChannelRetMsg.ChannelState.WAITING == channelRetMsg.getChannelState() ||
                ChannelRetMsg.ChannelState.UNKNOWN == channelRetMsg.getChannelState() ||
                ChannelRetMsg.ChannelState.API_RET_ERROR == channelRetMsg.getChannelState()
        ){
            this.updateInitOrderStateThrowException(PayConstant.STATE_ING, payOrder, channelRetMsg);
            // 系统异常：  订单不再处理。  为： 生成状态
        }else if( ChannelRetMsg.ChannelState.SYS_ERROR == channelRetMsg.getChannelState()){

        }else{

            throw new BizException("ChannelState 返回异常！");
        }

        //判断是否需要轮询查单
        if(channelRetMsg.isNeedQuery()){
            //mqSender.send(PayOrderReissueMQ.build(tradeNo, 1), 5);
        }

    }
    /** 更新订单状态 --》 订单生成--》 其他状态  (向外抛出异常) **/
    private void updateInitOrderStateThrowException(Long orderState, PayOrder payOrder, ChannelRetMsg channelRetMsg){
        payOrder.setState(orderState);
        payOrder.setChannelOrderNo(channelRetMsg.getChannelOrderId());
        payOrder.setErrCode(channelRetMsg.getChannelErrCode());
        payOrder.setErrMsg(channelRetMsg.getChannelErrMsg());
        // 聚合码场景 订单对象存在会员信息， 不可全部以上游为准。
        if(StringUtils.isNotEmpty(channelRetMsg.getChannelUserId())){
            payOrder.setChannelUser(channelRetMsg.getChannelUserId());
        }
        boolean isSuccess = payOrderService.updateInit2Ing(payOrder.getTradeNo(), payOrder);
        if(!isSuccess){
            throw new BizException("更新订单异常!");
        }
        isSuccess = payOrderService.updateIng2SuccessOrFail(payOrder.getTradeNo(), payOrder.getState(),
                channelRetMsg.getChannelOrderId(), channelRetMsg.getChannelUserId(), channelRetMsg.getChannelErrCode(), channelRetMsg.getChannelErrMsg());
        if(!isSuccess){
            throw new BizException("更新订单异常!");
        }
    }

    /**
     * 统一封装订单数据
     **/
    private ApiResp packageApiResByPayOrder(UnifiedOrderReq bizReq, UnifiedOrderResp bizResp, PayOrder payOrder) {
        // 返回接口数据
        bizResp.setTradeNo(payOrder.getTradeNo());
        bizResp.setOrderState(payOrder.getState());
        bizResp.setMchOrderNo(payOrder.getMchOrderNo());
        if (payOrder.getState() == PayConstant.STATE_FAIL) {
            bizResp.setErrCode(bizResp.getChannelRetMsg() != null ? bizResp.getChannelRetMsg().getChannelErrCode() : null);
            bizResp.setErrMsg(bizResp.getChannelRetMsg() != null ? bizResp.getChannelRetMsg().getChannelErrMsg() : null);
        }
        return ApiResp.okWithSign(bizResp, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());
    }


    /**
     * 生成订单
     *
     * @param unifiedOrderReq 请求参数
     * @param mchInfo         商户信息
     * @param mchApp          商户应用
     * @param channelCode     支付渠道
     * @param mchPayChannel   商户支付渠道
     * @return 订单信息
     */
    private PayOrder genPayOrder(UnifiedOrderReq unifiedOrderReq, MchInfo mchInfo, MchApp mchApp, String channelCode, MchPayChannel mchPayChannel) {
        PayOrder payOrder = new PayOrder();
        payOrder.setTradeNo(IdUtils.genTradeNo()); //生成订单ID
        payOrder.setMchNo(mchInfo.getMchNo()); //商户号
        payOrder.setIsvNo(mchInfo.getIsvNo()); //服务商号
        payOrder.setMchName(mchInfo.getMchName()); //商户名称（简称）
        payOrder.setMchType(mchInfo.getType()); //商户类型
        payOrder.setMchOrderNo(unifiedOrderReq.getMchOrderNo()); //商户订单号
        payOrder.setAppId(mchApp.getAppId()); //商户应用appId
        payOrder.setChannelCode(channelCode); //接口代码
        payOrder.setWayCode(unifiedOrderReq.getWayCode()); //支付方式
        payOrder.setAmount(unifiedOrderReq.getAmount()); //订单金额
        if (mchPayChannel != null) {
            payOrder.setMchFeeRate(mchPayChannel.getRate()); //商户手续费费率快照
        } else {
            payOrder.setMchFeeRate(BigDecimal.ZERO); //预下单模式， 按照0计算入库， 后续进行更新
        }
        payOrder.setMchFeeAmount(AmountUtil.calPercentageFee(payOrder.getAmount(), payOrder.getMchFeeRate())); //商户手续费,单位分
        payOrder.setCurrency(unifiedOrderReq.getCurrency()); //币种
        payOrder.setState(PayConstant.STATE_INIT); //订单状态, 默认订单生成状态
        payOrder.setClientIp(StringUtils.defaultIfEmpty(unifiedOrderReq.getClientIp(), getClientIp())); //客户端IP
        payOrder.setSubject(unifiedOrderReq.getSubject()); //商品标题
        payOrder.setBody(unifiedOrderReq.getBody()); //商品描述信息
//        payOrder.setChannelExtra(rq.getChannelExtra()); //特殊渠道发起的附件额外参数,  是否应该删除该字段了？？ 比如authCode不应该记录， 只是在传输阶段存在的吧？  之前的为了在payOrder对象需要传参。
        payOrder.setChannelUser(unifiedOrderReq.getChannelUserId()); //渠道用户标志
        payOrder.setExtParam(unifiedOrderReq.getExtParam()); //商户扩展参数
        payOrder.setNotifyUrl(unifiedOrderReq.getNotifyUrl()); //异步通知地址
        payOrder.setReturnUrl(unifiedOrderReq.getReturnUrl()); //页面跳转地址
        // 分账模式
        payOrder.setDivisionMode(ObjectUtils.defaultIfNull(unifiedOrderReq.getDivisionMode(), PayConstant.DIVISION_MODE_FORBID));
        Date nowDate = new Date();
        //订单过期时间 单位： 秒
        if (unifiedOrderReq.getExpiredTime() != null) {
            payOrder.setExpiredTime(DateUtil.offsetSecond(nowDate, unifiedOrderReq.getExpiredTime()));
        } else {
            payOrder.setExpiredTime(DateUtil.offsetHour(nowDate, 2)); //订单过期时间 默认两个小时
        }
        payOrder.setCreateTime(nowDate); //订单创建时间
        return payOrder;
    }
}
