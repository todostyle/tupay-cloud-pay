package com.tostyle.tupay.payment.controller.api.payorder;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.exception.ResponseException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IChannelNoticeService;
import com.tostyle.tupay.payment.controller.api.base.BaseApiController;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IPayMchNotifyService;
import com.tostyle.tupay.payment.service.IPayOrderService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tostyle
 * 2022/4/20 11:01
 */
@Slf4j
@Controller
public class ChannelNoticeController extends BaseApiController {

    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private ContextService contextService;
    @Autowired
    private IPayMchNotifyService payMchNotifyService;


    /** 同步通知入口 **/
    @RequestMapping(value= {"/api/pay/return/{channelCode}", "/api/pay/return/{channelCode}/{tradeNo}"})
    public String doReturn(HttpServletRequest request, @PathVariable("channelCode") String channelCode, @PathVariable(value = "tradeNo", required = false) String urlTradeNo){

        String tradeNo = null;
        String logPrefix = "进入[" +channelCode+ "]支付同步跳转：urlOrderId：["+ StringUtils.defaultIfEmpty(urlTradeNo, "") + "] ";
        log.info("===== {} =====" , logPrefix);

        try {

            // 参数有误
            if(StringUtils.isEmpty(channelCode)){
                return this.toReturnPage("channelCode is empty");
            }

            //查询支付接口是否存在
            IChannelNoticeService payNotifyService = SpringBeansUtil.getBean(channelCode + "ChannelNoticeService", IChannelNoticeService.class);

            // 支付通道接口实现不存在
            if(payNotifyService == null){
                log.error("{}, interface not exists ", logPrefix);
                return this.toReturnPage("[" + channelCode + "] interface not exists");
            }

            // 解析订单号 和 请求参数
            MutablePair<String, Object> mutablePair = payNotifyService.parseParams(request, urlTradeNo, IChannelNoticeService.NoticeTypeEnum.DO_RETURN);
            if(mutablePair == null){ // 解析数据失败， 响应已处理
                log.error("{}, mutablePair is null ", logPrefix);
                throw new BizException("解析数据异常！"); //需要实现类自行抛出ResponseException, 不应该在这抛此异常。
            }

            //解析到订单号
            tradeNo = mutablePair.left;
            log.info("{}, 解析数据为：tradeNo:{}, params:{}", logPrefix, tradeNo, mutablePair.getRight());

            if(StringUtils.isNotEmpty(urlTradeNo) && !urlTradeNo.equals(tradeNo)){
                log.error("{}, 订单号不匹配. urlOrderId={}, tradeNo={} ", logPrefix, urlTradeNo, tradeNo);
                throw new BizException("订单号不匹配！");
            }

            //获取订单号 和 订单数据
            PayOrder payOrder = payOrderService.selectPayOrderByTradeNo(tradeNo);

            // 订单不存在
            if(payOrder == null){
                log.error("{}, 订单不存在. tradeNo={} ", logPrefix, tradeNo);
                return this.toReturnPage("支付订单不存在");
            }

            //查询出商户应用的配置信息
            MchAppContext mchAppConfigContext = contextService.queryMchInfoAndAppInfo(payOrder.getMchNo(), payOrder.getAppId());

            //调起接口的回调判断
            ChannelRetMsg notifyResult = payNotifyService.doNotice(request, mutablePair.getRight(), payOrder, mchAppConfigContext, IChannelNoticeService.NoticeTypeEnum.DO_RETURN);

            // 返回null 表明出现异常， 无需处理通知下游等操作。
            if(notifyResult == null || notifyResult.getChannelState() == null || notifyResult.getResponseEntity() == null){
                log.error("{}, 处理回调事件异常  notifyResult data error, notifyResult ={} ",logPrefix, notifyResult);
                throw new BizException("处理回调事件异常！"); //需要实现类自行抛出ResponseException, 不应该在这抛此异常。
            }

            //判断订单状态
            if(notifyResult.getChannelState() == ChannelRetMsg.ChannelState.CONFIRM_SUCCESS) {
                payOrder.setState(PayConstant.STATE_SUCCESS);
            }else if(notifyResult.getChannelState() == ChannelRetMsg.ChannelState.CONFIRM_FAIL) {
                payOrder.setState(PayConstant.STATE_FAIL);
            }
            boolean hasReturnUrl = StringUtils.isNotBlank(payOrder.getReturnUrl());
            log.info("===== {}, 订单通知完成。 tradeNo={}, parseState = {}, hasReturnUrl={} =====", logPrefix, tradeNo, notifyResult.getChannelState(), hasReturnUrl);
            //包含通知地址时
            if(hasReturnUrl){
                // 重定向
                response.sendRedirect(payMchNotifyService.createReturnUrl(payOrder, mchAppConfigContext.getMchApp().getAppSecret()));
                return null;
            }else{
                //跳转到支付成功页面
                return this.toReturnPage(null);
            }
        } catch (BizException e) {
            log.error("{}, tradeNo={}, BizException", logPrefix, tradeNo, e);
            return this.toReturnPage(e.getMessage());

        } catch (ResponseException e) {
            log.error("{}, tradeNo={}, ResponseException", logPrefix, tradeNo, e);
            return this.toReturnPage(e.getMessage());

        } catch (Exception e) {
            log.error("{}, tradeNo={}, 系统异常", logPrefix, tradeNo, e);
            return this.toReturnPage(e.getMessage());
        }
    }


    /*  跳转到支付成功页面 **/
    private String toReturnPage(String errInfo){
        return "cashier/returnPage";
    }
}
