package com.tostyle.tupay.payment.controller.api.payorder;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IPayOrderCloseService;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.payorder.ClosePayOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.ClosePayOrderResp;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IPayOrderService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 关闭订单 controller
 *
 * @author tostyle
 * 2022/4/20 10:50
 */
@Slf4j
@RestController
public class CloseOrderController extends ApiController {


    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private ContextService contextService;



    @RequestMapping("/api/pay/close")
    public ApiResp queryOrder(){
        //获取参数 & 验签
        ClosePayOrderReq bizReq = getReqByWithMchSign(ClosePayOrderReq.class);
        if(StringUtils.isEmpty(bizReq.getTradeNo())){
            throw new BizException("tradeNo不能为空");
        }
        PayOrder payOrder = payOrderService.queryMchOrder(bizReq.getMchNo(), bizReq.getTradeNo(), bizReq.getMchOrderNo());
        if(payOrder == null){
            throw new BizException("订单不存在");
        }
        if (payOrder.getState() != PayConstant.STATE_INIT && payOrder.getState() != PayConstant.STATE_ING) {
            throw new BizException("当前订单不可关闭");
        }
        ClosePayOrderResp bizRes = new ClosePayOrderResp();
        try {
            String tradeNo = payOrder.getTradeNo();
            //查询支付接口是否存在
            IPayOrderCloseService closeService = SpringBeansUtil.getBean(payOrder.getChannelCode() + "PayOrderCloseService", IPayOrderCloseService.class);
            // 支付通道接口实现不存在
            if(closeService == null){
                log.error("{} interface not exists!", payOrder.getChannelCode());
                return null;
            }
            //查询出商户应用的配置信息
            MchAppContext mchAppContext = contextService.queryMchInfoAndAppInfo(payOrder.getMchNo(), payOrder.getAppId());
            ChannelRetMsg channelRetMsg = closeService.close(payOrder, mchAppContext);
            if(channelRetMsg == null){
                log.error("channelRetMsg is null");
                return null;
            }
            log.info("关闭订单[{}]结果为：{}", tradeNo, channelRetMsg);
            // 关闭订单 成功
            if(channelRetMsg.getChannelState() == ChannelRetMsg.ChannelState.CONFIRM_SUCCESS) {
                payOrderService.updateIng2Close(tradeNo);
            }else {
                return ApiResp.customFail(channelRetMsg.getChannelErrMsg());
            }
            bizRes.setChannelRetMsg(channelRetMsg);
        } catch (Exception e) {  // 关闭订单异常
            log.error("error payOrderId = {}", payOrder.getTradeNo(), e);
            return null;
        }
        return ApiResp.okWithSign(bizRes, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());
    }
}
