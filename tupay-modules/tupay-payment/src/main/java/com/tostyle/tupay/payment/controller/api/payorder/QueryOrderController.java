package com.tostyle.tupay.payment.controller.api.payorder;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.payorder.QueryPayOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.QueryPayOrderResp;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商户查单controller
 * @author tostyle
 * 2022/4/20 10:46
 */
@Slf4j
@RestController
public class QueryOrderController extends ApiController {

    @Autowired
    private IPayOrderService payOrderService;

    @Autowired
    private ContextService contextService;

    /**
     * 查单接口
     * **/
    @RequestMapping("/api/pay/query")
    public ApiResp queryOrder(){
        //获取参数 & 验签
        QueryPayOrderReq bizReq = getReqByWithMchSign(QueryPayOrderReq.class);
        if(StringUtils.isAllEmpty(bizReq.getMchOrderNo(), bizReq.getTradeNo())){
            throw new BizException("mchOrderNo 和 tradeNo不能同时为空");
        }
        PayOrder payOrder = payOrderService.queryMchOrder(bizReq.getMchNo(), bizReq.getTradeNo(), bizReq.getMchOrderNo());
        if(payOrder == null){
            throw new BizException("订单不存在");
        }
        QueryPayOrderResp bizResp = QueryPayOrderResp.buildByPayOrder(payOrder);
        return ApiResp.okWithSign(bizResp, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());
    }
}
