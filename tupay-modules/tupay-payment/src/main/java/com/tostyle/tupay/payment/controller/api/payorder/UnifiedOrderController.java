package com.tostyle.tupay.payment.controller.api.payorder;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.common.redis.service.RedisService;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import com.tostyle.tupay.payment.domain.model.payorder.juhepay.AutoBarOrderReq;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IPayWayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 统一下单
 * @author tostyle
 * 2022/4/18 11:21
 */
@Slf4j
@RestController
public class UnifiedOrderController extends AbstractPayOrderController{

    @Autowired
    private ContextService contextService;
    @Autowired
    private IPayWayService payWayService;

    @PostMapping("/api/pay/unifiedOrder")
    public ApiResp unifiedOrder(){
        //获取参数 & 验签
        UnifiedOrderReq req=getReqByWithMchSign(UnifiedOrderReq.class);
        UnifiedOrderReq bizReq = buildBizReq(req);
        //实现子类的res
        ApiResp apiResp = unifiedOrder(bizReq.getWayCode(), bizReq);
        if(apiResp.getData() == null){
            return apiResp;
        }
        UnifiedOrderResp bizRes=(UnifiedOrderResp)apiResp.getData();
        //聚合接口，返回的参数
        UnifiedOrderResp res = new UnifiedOrderResp();
        BeanUtils.copyProperties(bizRes, res);
        //只有 订单生成（QR_CASHIER） || 支付中 || 支付成功返回该数据
        if(bizRes.getOrderState() != null && (bizRes.getOrderState() == PayConstant.STATE_INIT || bizRes.getOrderState() == PayConstant.STATE_ING || bizRes.getOrderState() == PayConstant.STATE_SUCCESS) ){
            res.setPayDataType(bizRes.buildPayDataType());
            res.setPayData(bizRes.buildPayData());
        }
        return ApiResp.okWithSign(res, contextService.queryMchApp(req.getMchNo(), req.getAppId()).getAppSecret());
    }


    /**
     * 构建请求参数
     * @param rq
     * @return
     */
    private UnifiedOrderReq buildBizReq(UnifiedOrderReq rq){
        //支付方式  比如： ali_bar
        String wayCode = rq.getWayCode();
        //jsapi 收银台聚合支付场景 (不校验是否存在payWayCode)
        if(PayConstant.PAY_WAY_CODE.QR_CASHIER.equals(wayCode)){
            return rq.buildBizReq();
        }
        //如果是自动分类条码
        if(PayConstant.PAY_WAY_CODE.AUTO_BAR.equals(wayCode)){
            AutoBarOrderReq bizReq = (AutoBarOrderReq)rq.buildBizReq();
            wayCode = Md5Utils.getPayWayCodeByBarCode(bizReq.getAuthCode());
            rq.setWayCode(wayCode.trim());
        }
        if(payWayService.checkWayCode(wayCode)){
            throw new BizException("不支持的支付方式");
        }
        //转换为 bizRQ
        return rq.buildBizReq();
    }
}
