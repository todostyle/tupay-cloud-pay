package com.tostyle.tupay.payment.controller.api.payorder.payway;

import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.controller.api.payorder.AbstractPayOrderController;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliBarOrderReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝 条码支付 controller
 * @author tostyle
 * 2022/4/22 14:09
 */
@Slf4j
@RestController
public class AliBarOrderController extends AbstractPayOrderController {


    /**
     * 统一下单接口
     * **/
    @PostMapping("/api/pay/aliBarOrder")
    public ApiResp aliBarOrder(){
        //获取参数 & 验证
        AliBarOrderReq bizReq = getReqByWithMchSign(AliBarOrderReq.class);
        // 统一下单接口
        return unifiedOrder(PayConstant.PAY_WAY_CODE.ALI_BAR, bizReq);
    }

}
