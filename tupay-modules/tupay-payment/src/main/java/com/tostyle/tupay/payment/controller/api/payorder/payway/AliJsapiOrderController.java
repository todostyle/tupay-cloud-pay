package com.tostyle.tupay.payment.controller.api.payorder.payway;

import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.controller.api.payorder.AbstractPayOrderController;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliJsapiOrderReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝 jspai controller
 * @author tostyle
 * 2022/4/22 14:11
 */
@Slf4j
@RestController
public class AliJsapiOrderController extends AbstractPayOrderController {


    /**
     * 统一下单接口
     * **/
    @PostMapping("/api/pay/aliJsapiOrder")
    public ApiResp aliJsapiOrder(){
        //获取参数 & 验证
        AliJsapiOrderReq bizRQ = getReqByWithMchSign(AliJsapiOrderReq.class);
        // 统一下单接口
        return unifiedOrder(PayConstant.PAY_WAY_CODE.ALI_JSAPI, bizRQ);
    }
}
