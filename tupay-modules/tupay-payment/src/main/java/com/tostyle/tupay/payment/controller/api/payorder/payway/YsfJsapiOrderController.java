package com.tostyle.tupay.payment.controller.api.payorder.payway;

import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.controller.api.payorder.AbstractPayOrderController;
import com.tostyle.tupay.payment.domain.model.payorder.ysfpay.YsfJsapiOrderReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 云闪付 jsapi支付 controller
 * @author tostyle
 * 2022/4/22 14:13
 */
@Slf4j
@RestController
public class YsfJsapiOrderController extends AbstractPayOrderController {


    /**
     * 统一下单接口
     * **/
    @PostMapping("/api/pay/ysfJsapiOrder")
    public ApiResp aliJsapiOrder(){
        //获取参数 & 验证
        YsfJsapiOrderReq bizReq = getReqByWithMchSign(YsfJsapiOrderReq.class);
        // 统一下单接口
        return unifiedOrder(PayConstant.PAY_WAY_CODE.YSF_JSAPI, bizReq);
    }
}
