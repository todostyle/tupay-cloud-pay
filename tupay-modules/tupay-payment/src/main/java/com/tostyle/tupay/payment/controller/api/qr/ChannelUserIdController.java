package com.tostyle.tupay.payment.controller.api.qr;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IChannelUserService;
import com.tostyle.tupay.payment.controller.api.payorder.AbstractPayOrderController;
import com.tostyle.tupay.payment.domain.model.ChannelUserIdReq;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tostyle
 * 2022/4/20 17:05
 */
@RestController
@RequestMapping("/api/channelUserId")
public class ChannelUserIdController extends AbstractPayOrderController {

    @Autowired
    private ContextService contextService;


    /**  重定向到微信地址  **/
    @RequestMapping("/jump")
    public void jump() throws Exception {
        //获取请求数据
        ChannelUserIdReq bizReq = getReqByWithMchSign(ChannelUserIdReq.class);
        String channelCode = "AUTO".equalsIgnoreCase(bizReq.getChannelCode()) ? getIfCodeByUA() : bizReq.getChannelCode();
        // 获取接口
        IChannelUserService channelUserService = SpringBeansUtil.getBean(channelCode + "ChannelUserService", IChannelUserService.class);
        if(channelUserService == null){
            throw new BizException("不支持的客户端");
        }
        if(!StringUtils.isAvailableUrl(bizReq.getRedirectUrl())){
            throw new BizException("跳转地址有误！");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mchNo", bizReq.getMchNo());
        jsonObject.put("appId", bizReq.getAppId());
        jsonObject.put("extParam", bizReq.getExtParam());
        jsonObject.put("channelCode", channelCode);
        jsonObject.put("redirectUrl", bizReq.getRedirectUrl());
        //回调地址
        String callbackUrl = applicationConfig.genMchChannelUserIdApiOauth2RedirectUrlEncode(jsonObject);
        //获取商户配置信息
        MchAppContext mchAppConfigContext = contextService.queryMchInfoAndAppInfo(bizReq.getMchNo(), bizReq.getAppId());
        String redirectUrl = channelUserService.buildUserRedirectUrl(callbackUrl, mchAppConfigContext);
        response.sendRedirect(redirectUrl);

    }


    /**  回调地址  **/
    @RequestMapping("/oauth2Callback/{aesData}")
    public void oauth2Callback(@PathVariable("aesData") String aesData) throws Exception {
        JSONObject callbackData = JSON.parseObject(Md5Utils.aesDecode(aesData));
        String mchNo = callbackData.getString("mchNo");
        String appId = callbackData.getString("appId");
        String ifCode = callbackData.getString("ifCode");
        String extParam = callbackData.getString("extParam");
        String redirectUrl = callbackData.getString("redirectUrl");
        // 获取接口
        IChannelUserService channelUserService = SpringBeansUtil.getBean(ifCode + "ChannelUserService", IChannelUserService.class);
        if(channelUserService == null){
            throw new BizException("不支持的客户端");
        }
        //获取商户配置信息
        MchAppContext mchAppConfigContext = contextService.queryMchInfoAndAppInfo(mchNo, appId);
        //获取渠道用户ID
        String channelUserId = channelUserService.getChannelUserId(getReqParamJSON(), mchAppConfigContext);
        //同步跳转
        JSONObject appendParams = new JSONObject();
        appendParams.put("appId", appId);
        appendParams.put("channelUserId", channelUserId);
        appendParams.put("extParam", extParam);
        response.sendRedirect(StringUtils.appendUrlQuery(redirectUrl, appendParams));
    }


    /** 根据UA获取支付接口 */
    private String getIfCodeByUA() {
        String ua = request.getHeader("User-Agent");
        // 无法识别扫码客户端
        if (org.apache.commons.lang3.StringUtils.isBlank(ua)) {
            return null;
        }
        if(ua.contains("Alipay")) {
            return PayConstant.CHANNEL_CODE.ALIPAY;  //支付宝服务窗支付
        }else if(ua.contains("MicroMessenger")) {
            return PayConstant.CHANNEL_CODE.WXPAY;
        }
        return null;
    }


}
