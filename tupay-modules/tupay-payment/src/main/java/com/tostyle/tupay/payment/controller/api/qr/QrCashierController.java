package com.tostyle.tupay.payment.controller.api.qr;

import com.alipay.api.AlipayApiException;
import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.IChannelUserService;
import com.tostyle.tupay.payment.controller.api.payorder.AbstractPayOrderController;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.AliJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderReq;
import com.tostyle.tupay.payment.service.IPayMchNotifyService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 聚合码支付二维码收银台controller
 * @author tostyle
 * 2022/4/21 15:27
 */
@RestController
@RequestMapping("/api/cashier")
public class QrCashierController extends AbstractPayOrderController {


    private IPayMchNotifyService payMchNotifyService;

    /**
     * 返回 oauth2【获取uerId跳转地址】
     * **/
    @PostMapping("/redirectUrl")
    public ApiResp redirectUrl(){
        //查询订单
        PayOrder payOrder = getPayOrder();
        //回调地址
        String redirectUrlEncode = applicationConfig.genOauth2RedirectUrlEncode(payOrder.getTradeNo());
        //获取商户配置信息
        MchAppContext mchAppContext = contextService.queryMchInfoAndAppInfo(payOrder.getMchNo(), payOrder.getAppId());
        //获取接口并返回数据
        IChannelUserService channelUserService = getServiceByWayCode(getWayCode(), "ChannelUserService", IChannelUserService.class);
        return ApiResp.ok(channelUserService.buildUserRedirectUrl(redirectUrlEncode, mchAppContext));

    }

    /**
     * 获取userId
     * **/
    @PostMapping("/channelUserId")
    public ApiResp channelUserId() throws Exception {
        //查询订单
        PayOrder payOrder = getPayOrder();
        String wayCode = getWayCode();
        //获取商户配置信息
        MchAppContext mchAppConfigContext = contextService.queryMchInfoAndAppInfo(payOrder.getMchNo(), payOrder.getAppId());
        IChannelUserService channelUserService = getServiceByWayCode(wayCode, "ChannelUserService", IChannelUserService.class);
        return ApiResp.ok(channelUserService.getChannelUserId(getReqParamJSON(), mchAppConfigContext));

    }


    /**
     * 获取订单支付信息
     * **/
    @PostMapping("/payOrderInfo")
    public ApiResp payOrderInfo() throws Exception {

        //查询订单
        PayOrder payOrder = getPayOrder();

        PayOrder resOrder = new PayOrder();
        resOrder.setTradeNo(payOrder.getTradeNo());
        resOrder.setMchOrderNo(payOrder.getMchOrderNo());
        resOrder.setMchName(payOrder.getMchName());
        resOrder.setAmount(payOrder.getAmount());
        resOrder.setReturnUrl(payMchNotifyService.createReturnUrl(payOrder, contextService.queryMchInfoAndAppInfo(payOrder.getMchNo(), payOrder.getAppId()).getMchApp().getAppSecret()));
        return ApiResp.ok(resOrder);
    }


    /** 调起下单接口, 返回支付数据包  **/
    @PostMapping("/pay")
    public ApiResp pay() throws Exception {

        //查询订单
        PayOrder payOrder = getPayOrder();

        String wayCode = getWayCode();

        ApiResp apiResp = null;

        if(wayCode.equals(PayConstant.PAY_WAY_CODE.ALI_JSAPI)){
            apiResp = packageAlipayPayPackage(payOrder);
        }else if(wayCode.equals(PayConstant.PAY_WAY_CODE.WX_JSAPI)){
            apiResp = packageWxpayPayPackage(payOrder);
        }

        return ApiResp.ok(apiResp);
    }


    /** 获取支付宝的 支付参数 **/
    private ApiResp packageAlipayPayPackage(PayOrder payOrder) throws AlipayApiException {

        String channelUserId = getValStringRequired("channelUserId");
        AliJsapiOrderReq rq = new AliJsapiOrderReq();
        rq.setBuyerUserId(channelUserId);
        return this.unifiedOrder(getWayCode(), rq, payOrder);
    }


    /** 获取微信的 支付参数 **/
    private ApiResp packageWxpayPayPackage(PayOrder payOrder) throws AlipayApiException {

        String openId = getValStringRequired("channelUserId");
        WxJsapiOrderReq rq = new WxJsapiOrderReq();
        rq.setOpenid(openId);
        return this.unifiedOrder(getWayCode(), rq, payOrder);
    }


    private String getToken(){
        return getValStringRequired("token");
    }

    private String getWayCode(){
        return getValStringRequired("wayCode");
    }

    private PayOrder getPayOrder(){
        String tradeNo = Md5Utils.aesDecode(getToken()); //解析token
        PayOrder payOrder = payOrderService.selectPayOrderByTradeNo(tradeNo);
        if(payOrder == null || payOrder.getState() != PayConstant.STATE_INIT){
            throw new BizException("订单不存在或状态不正确");
        }
        return payOrder;
    }


    private <T> T getServiceByWayCode(String wayCode, String serviceSuffix, Class<T> cls){
        if(PayConstant.PAY_WAY_CODE.ALI_JSAPI.equals(wayCode)){
            return SpringBeansUtil.getBean(PayConstant.CHANNEL_CODE.ALIPAY + serviceSuffix, cls);
        }else if(PayConstant.PAY_WAY_CODE.WX_JSAPI.equals(wayCode)){
            return SpringBeansUtil.getBean(PayConstant.CHANNEL_CODE.WXPAY + serviceSuffix, cls);
        }
        return null;
    }

}
