package com.tostyle.tupay.payment.controller.api.refund;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.model.refund.QueryRefundOrderReq;
import com.tostyle.tupay.payment.domain.model.refund.QueryRefundOrderResp;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IRefundOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tostyle
 * 2022/4/22 11:05
 */
@Slf4j
@RestController
public class QueryRefundOrderController extends ApiController {

    @Autowired
    private IRefundOrderService refundOrderService;
    @Autowired
    private ContextService contextService;


    /**
     * 查单接口
     * **/
    @RequestMapping("/api/refund/query")
    public ApiResp queryRefundOrder(){
        //获取参数 & 验签
        QueryRefundOrderReq bizReq = getReqByWithMchSign(QueryRefundOrderReq.class);
        if(StringUtils.isAllEmpty(bizReq.getMchRefundNo(), bizReq.getRefundOrderId())){
            throw new BizException("mchRefundNo 和 refundOrderId不能同时为空");
        }
        RefundOrder refundOrder = refundOrderService.queryMchOrder(bizReq.getMchNo(), bizReq.getMchRefundNo(), bizReq.getRefundOrderId());
        if(refundOrder == null){
            throw new BizException("订单不存在");
        }
        QueryRefundOrderResp bizRes = QueryRefundOrderResp.buildByRefundOrder(refundOrder);
        return ApiResp.okWithSign(bizRes, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());
    }
}
