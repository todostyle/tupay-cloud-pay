package com.tostyle.tupay.payment.controller.api.transfer;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.transfer.QueryTransferOrderReq;
import com.tostyle.tupay.payment.domain.model.transfer.QueryTransferOrderResp;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.ITransferOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商户转账单查询controller
 * @author tostyle
 * 2022/4/22 9:46
 */
@Slf4j
@RestController
public class QueryTransferOrderController extends ApiController {

    @Autowired
    private ITransferOrderService transferOrderService;
    @Autowired
    private ContextService contextService;


    /**
     * 查单接口
     * **/
    @RequestMapping("/api/transfer/query")
    public ApiResp queryTransferOrder(){
        //获取参数 & 验签
        QueryTransferOrderReq bizReq = getReqByWithMchSign(QueryTransferOrderReq.class);
        if(StringUtils.isAllEmpty(bizReq.getMchOrderNo(), bizReq.getTransferId())){
            throw new BizException("mchOrderNo 和 transferId不能同时为空");
        }
        TransferOrder refundOrder = transferOrderService.queryMchOrder(bizReq.getMchNo(), bizReq.getMchOrderNo(), bizReq.getTransferId());
        if(refundOrder == null){
            throw new BizException("订单不存在");
        }
        QueryTransferOrderResp bizRes = QueryTransferOrderResp.buildByRecord(refundOrder);
        return ApiResp.okWithSign(bizRes, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());
    }
}
