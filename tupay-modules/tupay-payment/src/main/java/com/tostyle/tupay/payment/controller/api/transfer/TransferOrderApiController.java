package com.tostyle.tupay.payment.controller.api.transfer;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.exception.ChannelException;
import com.tostyle.tupay.common.core.model.ApiResp;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.common.core.utils.IdUtils;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.channel.ITransferService;
import com.tostyle.tupay.payment.controller.api.ApiController;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.MchInfo;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.transfer.TransferOrderReq;
import com.tostyle.tupay.payment.domain.model.transfer.TransferOrderResp;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IPayChannelParamService;
import com.tostyle.tupay.payment.service.IPayMchNotifyService;
import com.tostyle.tupay.payment.service.ITransferOrderService;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 转账接口
 * @author tostyle
 * 2022/4/21 15:38
 */
@Slf4j
@RestController
public class TransferOrderApiController extends ApiController {

    @Autowired
    private ContextService contextService;
    @Autowired
    private ITransferOrderService transferOrderService;
    @Autowired
    private IPayChannelParamService payChannelParamService;
    @Autowired
    private IPayMchNotifyService payMchNotifyService;


    /**
     * 转账
     * **/
    @PostMapping("/api/transferOrder")
    public ApiResp transferOrder(){
        TransferOrder transferOrder = null;
        //获取参数 & 验签
        TransferOrderReq bizReq = getReqByWithMchSign(TransferOrderReq.class);
        try {
            String mchNo = bizReq.getMchNo();
            String appId = bizReq.getAppId();
            String channelCode = bizReq.getChannelCode();
            // 商户订单号是否重复
            if(transferOrderService.checkMchOrderNo(mchNo,bizReq.getMchOrderNo())){
                throw new BizException("商户订单["+bizReq.getMchOrderNo()+"]已存在");
            }
            if(StringUtils.isNotEmpty(bizReq.getNotifyUrl()) && !StringUtils.isAvailableUrl(bizReq.getNotifyUrl())){
                throw new BizException("异步通知地址协议仅支持http:// 或 https:// !");
            }
            // 商户配置信息
            MchAppContext mchAppConfigContext = contextService.queryMchInfoAndAppInfo(mchNo, appId);
            if(mchAppConfigContext == null){
                throw new BizException("获取商户应用信息失败");
            }
            MchInfo mchInfo = mchAppConfigContext.getMchInfo();
            MchApp mchApp = mchAppConfigContext.getMchApp();
            // 是否已正确配置
            if(!payChannelParamService.mchAppHasAvailableChannelCode(appId, channelCode)){
                throw new BizException("应用未开通此接口配置!");
            }
            ITransferService transferService = SpringBeansUtil.getBean(channelCode + "TransferService", ITransferService.class);
            if(transferService == null){
                throw new BizException("无此转账通道接口");
            }
            if(!transferService.isSupport(bizReq.getEntryType())){
                throw new BizException("该接口不支持该入账方式");
            }
            transferOrder = genTransferOrder(bizReq, mchInfo, mchApp, channelCode);
            //预先校验
            String errMsg = transferService.preCheck(bizReq, transferOrder);
            if(StringUtils.isNotEmpty(errMsg)){
                throw new BizException(errMsg);
            }
            // 入库
            transferOrderService.insertTransferOrder(transferOrder);
            // 调起上游接口
            ChannelRetMsg channelRetMsg = transferService.transfer(bizReq, transferOrder, mchAppConfigContext);
            //处理退款单状态
            this.processChannelMsg(channelRetMsg, transferOrder);
            TransferOrderResp bizRes = TransferOrderResp.buildByRecord(transferOrder);
            return ApiResp.okWithSign(bizRes, mchApp.getAppSecret());
        }  catch (BizException e) {
            return ApiResp.customFail(e.getMessage());
        } catch (ChannelException e) {
            //处理上游返回数据
            this.processChannelMsg(e.getChannelRetMsg(), transferOrder);

            if(e.getChannelRetMsg().getChannelState() == ChannelRetMsg.ChannelState.SYS_ERROR ){
                return ApiResp.customFail(e.getMessage());
            }
            TransferOrderResp bizRes = TransferOrderResp.buildByRecord(transferOrder);
            return ApiResp.okWithSign(bizRes, contextService.queryMchApp(bizReq.getMchNo(), bizReq.getAppId()).getAppSecret());

        } catch (Exception e) {
            log.error("系统异常：{}", e.getMessage());
            return ApiResp.customFail("系统异常");
        }
    }


    private TransferOrder genTransferOrder(TransferOrderReq bizReq, MchInfo mchInfo, MchApp mchApp, String channelCode){

        TransferOrder transferOrder = new TransferOrder();
        transferOrder.setTransferId(IdUtils.genTransferNo()); //生成转账订单号
        transferOrder.setMchNo(mchInfo.getMchNo()); //商户号
        transferOrder.setIsvNo(mchInfo.getIsvNo()); //服务商号
        transferOrder.setAppId(mchApp.getAppId()); //商户应用appId
        transferOrder.setMchName(mchInfo.getShortName()); //商户名称（简称）
        transferOrder.setMchType(mchInfo.getType()); //商户类型
        transferOrder.setMchOrderNo(bizReq.getMchOrderNo()); //商户订单号
        transferOrder.setChannelCode(channelCode); //接口代码
        transferOrder.setEntryType(bizReq.getEntryType()); //入账方式
        transferOrder.setAmount(bizReq.getAmount()); //订单金额
        transferOrder.setCurrency(bizReq.getCurrency()); //币种
        transferOrder.setClientIp(StringUtils.defaultIfEmpty(bizReq.getClientIp(), getClientIp())); //客户端IP
        transferOrder.setState(PayConstant.TRANSFER_STATE_INIT); //订单状态, 默认订单生成状态
        transferOrder.setAccountNo(bizReq.getAccountNo()); //收款账号
        transferOrder.setAccountName(bizReq.getAccountName()); //账户姓名
        transferOrder.setBankName(bizReq.getBankName()); //银行名称
        transferOrder.setTransferDesc(bizReq.getTransferDesc()); //转账备注
        transferOrder.setExtParam(bizReq.getExtParam()); //商户扩展参数
        transferOrder.setNotifyUrl(bizReq.getNotifyUrl()); //异步通知地址
        transferOrder.setCreateTime(new Date()); //订单创建时间
        return transferOrder;

    }


    /**
     * 处理返回的渠道信息，并更新订单状态
     *  TransferOrder将对部分信息进行 赋值操作。
     * **/
    private void processChannelMsg(ChannelRetMsg channelRetMsg, TransferOrder transferOrder){
        //对象为空 || 上游返回状态为空， 则无需操作
        if(channelRetMsg == null || channelRetMsg.getChannelState() == null){
            return ;
        }
        //明确成功
        if(ChannelRetMsg.ChannelState.CONFIRM_SUCCESS == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(PayConstant.TRANSFER_STATE_SUCCESS, transferOrder, channelRetMsg);
            payMchNotifyService.transferOrderNotify(transferOrder);
            //明确失败
        }else if(ChannelRetMsg.ChannelState.CONFIRM_FAIL == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(PayConstant.TRANSFER_STATE_FAIL, transferOrder, channelRetMsg);
            payMchNotifyService.transferOrderNotify(transferOrder);

            // 上游处理中 || 未知 || 上游接口返回异常  订单为支付中状态
        }else if( ChannelRetMsg.ChannelState.WAITING == channelRetMsg.getChannelState() ||
                ChannelRetMsg.ChannelState.UNKNOWN == channelRetMsg.getChannelState() ||
                ChannelRetMsg.ChannelState.API_RET_ERROR == channelRetMsg.getChannelState()

        ){
            this.updateInitOrderStateThrowException(PayConstant.TRANSFER_STATE_ING, transferOrder, channelRetMsg);
            // 系统异常：  订单不再处理。  为： 生成状态
        }else if( ChannelRetMsg.ChannelState.SYS_ERROR == channelRetMsg.getChannelState()){

        }else{
            throw new BizException("ChannelState 返回异常！");
        }

    }


    /** 更新订单状态 --》 订单生成--》 其他状态  (向外抛出异常) **/
    private void updateInitOrderStateThrowException(Long orderState, TransferOrder transferOrder, ChannelRetMsg channelRetMsg){
        transferOrder.setState(orderState);
        transferOrder.setChannelOrderNo(channelRetMsg.getChannelOrderId());
        transferOrder.setErrCode(channelRetMsg.getChannelErrCode());
        transferOrder.setErrMsg(channelRetMsg.getChannelErrMsg());
        boolean isSuccess = transferOrderService.updateInit2Ing(transferOrder.getTransferId());
        if(!isSuccess){
            throw new BizException("更新转账订单异常!");
        }
        isSuccess = transferOrderService.updateIng2SuccessOrFail(transferOrder.getTransferId(), transferOrder.getState(),
                channelRetMsg.getChannelOrderId(), channelRetMsg.getChannelErrCode(), channelRetMsg.getChannelErrMsg());
        if(!isSuccess){
            throw new BizException("更新转账订单异常!");
        }
    }


}
