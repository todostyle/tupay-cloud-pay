package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.service.IMchAppService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 应用列表Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/mchapp")
public class MchAppController extends BaseController
{
    @Autowired
    private IMchAppService mchAppService;

    /**
     * 查询应用列表列表
     */
    @RequiresPermissions("payment:mchapp:list")
    @GetMapping("/list")
    public TableDataInfo list(MchApp mchApp)
    {
        startPage();
        List<MchApp> list = mchAppService.selectMchAppList(mchApp);
        return getDataTable(list);
    }

    /**
     * 导出应用列表列表
     */
    @RequiresPermissions("payment:mchapp:export")
    @Log(title = "应用列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchApp mchApp)
    {
        List<MchApp> list = mchAppService.selectMchAppList(mchApp);
        ExcelUtil<MchApp> util = new ExcelUtil<MchApp>(MchApp.class);
        util.exportExcel(response, list, "应用列表数据");
    }

    /**
     * 获取应用列表详细信息
     */
    @RequiresPermissions("payment:mchapp:query")
    @GetMapping(value = "/{appId}")
    public AjaxResult getInfo(@PathVariable("appId") String appId)
    {
        return AjaxResult.success(mchAppService.selectMchAppByAppId(appId));
    }

    /**
     * 新增应用列表
     */
    @RequiresPermissions("payment:mchapp:add")
    @Log(title = "应用列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchApp mchApp)
    {
        return toAjax(mchAppService.insertMchApp(mchApp));
    }

    /**
     * 修改应用列表
     */
    @RequiresPermissions("payment:mchapp:edit")
    @Log(title = "应用列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchApp mchApp)
    {
        return toAjax(mchAppService.updateMchApp(mchApp));
    }

    /**
     * 删除应用列表
     */
    @RequiresPermissions("payment:mchapp:remove")
    @Log(title = "应用列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appIds}")
    public AjaxResult remove(@PathVariable String[] appIds)
    {
        return toAjax(mchAppService.deleteMchAppByAppIds(appIds));
    }
}
