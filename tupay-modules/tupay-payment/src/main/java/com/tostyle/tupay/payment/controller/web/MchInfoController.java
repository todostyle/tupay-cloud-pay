package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.MchInfo;
import com.tostyle.tupay.payment.service.IMchInfoService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 商户列表Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/mchinfo")
public class MchInfoController extends BaseController
{
    @Autowired
    private IMchInfoService mchInfoService;

    /**
     * 查询商户列表列表
     */
    @RequiresPermissions("payment:mchinfo:list")
    @GetMapping("/list")
    public TableDataInfo list(MchInfo mchInfo)
    {
        startPage();
        List<MchInfo> list = mchInfoService.selectMchInfoList(mchInfo);
        return getDataTable(list);
    }

    /**
     * 导出商户列表列表
     */
    @RequiresPermissions("payment:mchinfo:export")
    @Log(title = "商户列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchInfo mchInfo)
    {
        List<MchInfo> list = mchInfoService.selectMchInfoList(mchInfo);
        ExcelUtil<MchInfo> util = new ExcelUtil<MchInfo>(MchInfo.class);
        util.exportExcel(response, list, "商户列表数据");
    }

    /**
     * 获取商户列表详细信息
     */
    @RequiresPermissions("payment:mchinfo:query")
    @GetMapping(value = "/{mchNo}")
    public AjaxResult getInfo(@PathVariable("mchNo") String mchNo)
    {
        return AjaxResult.success(mchInfoService.selectMchInfoByMchNo(mchNo));
    }

    /**
     * 新增商户列表
     */
    @RequiresPermissions("payment:mchinfo:add")
    @Log(title = "商户列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchInfo mchInfo)
    {
        return toAjax(mchInfoService.insertMchInfo(mchInfo));
    }

    /**
     * 修改商户列表
     */
    @RequiresPermissions("payment:mchinfo:edit")
    @Log(title = "商户列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchInfo mchInfo)
    {
        return toAjax(mchInfoService.updateMchInfo(mchInfo));
    }

    /**
     * 删除商户列表
     */
    @RequiresPermissions("payment:mchinfo:remove")
    @Log(title = "商户列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{mchNos}")
    public AjaxResult remove(@PathVariable String[] mchNos)
    {
        return toAjax(mchInfoService.deleteMchInfoByMchNos(mchNos));
    }
}
