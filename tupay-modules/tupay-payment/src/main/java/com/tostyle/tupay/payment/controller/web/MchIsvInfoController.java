package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.MchIsvInfo;
import com.tostyle.tupay.payment.service.IMchIsvInfoService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 服务商列表Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/isvinfo")
public class MchIsvInfoController extends BaseController
{
    @Autowired
    private IMchIsvInfoService mchIsvInfoService;

    /**
     * 查询服务商列表列表
     */
    @RequiresPermissions("payment:isvinfo:list")
    @GetMapping("/list")
    public TableDataInfo list(MchIsvInfo mchIsvInfo)
    {
        startPage();
        List<MchIsvInfo> list = mchIsvInfoService.selectMchIsvInfoList(mchIsvInfo);
        return getDataTable(list);
    }

    /**
     * 导出服务商列表列表
     */
    @RequiresPermissions("payment:isvinfo:export")
    @Log(title = "服务商列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchIsvInfo mchIsvInfo)
    {
        List<MchIsvInfo> list = mchIsvInfoService.selectMchIsvInfoList(mchIsvInfo);
        ExcelUtil<MchIsvInfo> util = new ExcelUtil<MchIsvInfo>(MchIsvInfo.class);
        util.exportExcel(response, list, "服务商列表数据");
    }

    /**
     * 获取服务商列表详细信息
     */
    @RequiresPermissions("payment:isvinfo:query")
    @GetMapping(value = "/{isvNo}")
    public AjaxResult getInfo(@PathVariable("isvNo") String isvNo)
    {
        return AjaxResult.success(mchIsvInfoService.selectMchIsvInfoByIsvNo(isvNo));
    }

    /**
     * 新增服务商列表
     */
    @RequiresPermissions("payment:isvinfo:add")
    @Log(title = "服务商列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchIsvInfo mchIsvInfo)
    {
        return toAjax(mchIsvInfoService.insertMchIsvInfo(mchIsvInfo));
    }

    /**
     * 修改服务商列表
     */
    @RequiresPermissions("payment:isvinfo:edit")
    @Log(title = "服务商列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchIsvInfo mchIsvInfo)
    {
        return toAjax(mchIsvInfoService.updateMchIsvInfo(mchIsvInfo));
    }

    /**
     * 删除服务商列表
     */
    @RequiresPermissions("payment:isvinfo:remove")
    @Log(title = "服务商列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{isvNos}")
    public AjaxResult remove(@PathVariable String[] isvNos)
    {
        return toAjax(mchIsvInfoService.deleteMchIsvInfoByIsvNos(isvNos));
    }
}
