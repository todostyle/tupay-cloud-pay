package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.MchNotify;
import com.tostyle.tupay.payment.service.IMchNotifyService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 商户通知Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/mchnotify")
public class MchNotifyController extends BaseController
{
    @Autowired
    private IMchNotifyService mchNotifyService;

    /**
     * 查询商户通知列表
     */
    @RequiresPermissions("payment:mchnotify:list")
    @GetMapping("/list")
    public TableDataInfo list(MchNotify mchNotify)
    {
        startPage();
        List<MchNotify> list = mchNotifyService.selectMchNotifyList(mchNotify);
        return getDataTable(list);
    }

    /**
     * 导出商户通知列表
     */
    @RequiresPermissions("payment:mchnotify:export")
    @Log(title = "商户通知", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchNotify mchNotify)
    {
        List<MchNotify> list = mchNotifyService.selectMchNotifyList(mchNotify);
        ExcelUtil<MchNotify> util = new ExcelUtil<MchNotify>(MchNotify.class);
        util.exportExcel(response, list, "商户通知数据");
    }

    /**
     * 获取商户通知详细信息
     */
    @RequiresPermissions("payment:mchnotify:query")
    @GetMapping(value = "/{notifyId}")
    public AjaxResult getInfo(@PathVariable("notifyId") Long notifyId)
    {
        return AjaxResult.success(mchNotifyService.selectMchNotifyByNotifyId(notifyId));
    }

    /**
     * 新增商户通知
     */
    @RequiresPermissions("payment:mchnotify:add")
    @Log(title = "商户通知", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchNotify mchNotify)
    {
        return toAjax(mchNotifyService.insertMchNotify(mchNotify));
    }

    /**
     * 修改商户通知
     */
    @RequiresPermissions("payment:mchnotify:edit")
    @Log(title = "商户通知", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchNotify mchNotify)
    {
        return toAjax(mchNotifyService.updateMchNotify(mchNotify));
    }

    /**
     * 删除商户通知
     */
    @RequiresPermissions("payment:mchnotify:remove")
    @Log(title = "商户通知", businessType = BusinessType.DELETE)
	@DeleteMapping("/{notifyIds}")
    public AjaxResult remove(@PathVariable Long[] notifyIds)
    {
        return toAjax(mchNotifyService.deleteMchNotifyByNotifyIds(notifyIds));
    }
}
