package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.PayChannelDefine;
import com.tostyle.tupay.payment.service.IPayChannelDefineService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 支付渠道定义Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/define")
public class PayChannelDefineController extends BaseController
{
    @Autowired
    private IPayChannelDefineService payChannelDefineService;

    /**
     * 查询支付渠道定义列表
     */
    @RequiresPermissions("payment:define:list")
    @GetMapping("/list")
    public TableDataInfo list(PayChannelDefine payChannelDefine)
    {
        startPage();
        List<PayChannelDefine> list = payChannelDefineService.selectPayChannelDefineList(payChannelDefine);
        return getDataTable(list);
    }

    /**
     * 导出支付渠道定义列表
     */
    @RequiresPermissions("payment:define:export")
    @Log(title = "支付渠道定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayChannelDefine payChannelDefine)
    {
        List<PayChannelDefine> list = payChannelDefineService.selectPayChannelDefineList(payChannelDefine);
        ExcelUtil<PayChannelDefine> util = new ExcelUtil<PayChannelDefine>(PayChannelDefine.class);
        util.exportExcel(response, list, "支付渠道定义数据");
    }

    /**
     * 获取支付渠道定义详细信息
     */
    @RequiresPermissions("payment:define:query")
    @GetMapping(value = "/{channelCode}")
    public AjaxResult getInfo(@PathVariable("channelCode") String channelCode)
    {
        return AjaxResult.success(payChannelDefineService.selectPayChannelDefineByChannelCode(channelCode));
    }

    /**
     * 新增支付渠道定义
     */
    @RequiresPermissions("payment:define:add")
    @Log(title = "支付渠道定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayChannelDefine payChannelDefine)
    {
        return toAjax(payChannelDefineService.insertPayChannelDefine(payChannelDefine));
    }

    /**
     * 修改支付渠道定义
     */
    @RequiresPermissions("payment:define:edit")
    @Log(title = "支付渠道定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayChannelDefine payChannelDefine)
    {
        return toAjax(payChannelDefineService.updatePayChannelDefine(payChannelDefine));
    }

    /**
     * 删除支付渠道定义
     */
    @RequiresPermissions("payment:define:remove")
    @Log(title = "支付渠道定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{channelCodes}")
    public AjaxResult remove(@PathVariable String[] channelCodes)
    {
        return toAjax(payChannelDefineService.deletePayChannelDefineByChannelCodes(channelCodes));
    }
}
