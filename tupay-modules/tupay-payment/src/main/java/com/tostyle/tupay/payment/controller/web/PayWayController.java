package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.PayWay;
import com.tostyle.tupay.payment.service.IPayWayService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 支付方式Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/payway")
public class PayWayController extends BaseController
{
    @Autowired
    private IPayWayService payWayService;

    /**
     * 查询支付方式列表
     */
    @RequiresPermissions("payment:payway:list")
    @GetMapping("/list")
    public TableDataInfo list(PayWay payWay)
    {
        startPage();
        List<PayWay> list = payWayService.selectPayWayList(payWay);
        return getDataTable(list);
    }

    /**
     * 导出支付方式列表
     */
    @RequiresPermissions("payment:payway:export")
    @Log(title = "支付方式", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayWay payWay)
    {
        List<PayWay> list = payWayService.selectPayWayList(payWay);
        ExcelUtil<PayWay> util = new ExcelUtil<PayWay>(PayWay.class);
        util.exportExcel(response, list, "支付方式数据");
    }

    /**
     * 获取支付方式详细信息
     */
    @RequiresPermissions("payment:payway:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payWayService.selectPayWayById(id));
    }

    /**
     * 新增支付方式
     */
    @RequiresPermissions("payment:payway:add")
    @Log(title = "支付方式", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayWay payWay)
    {
        return toAjax(payWayService.insertPayWay(payWay));
    }

    /**
     * 修改支付方式
     */
    @RequiresPermissions("payment:payway:edit")
    @Log(title = "支付方式", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayWay payWay)
    {
        return toAjax(payWayService.updatePayWay(payWay));
    }

    /**
     * 删除支付方式
     */
    @RequiresPermissions("payment:payway:remove")
    @Log(title = "支付方式", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payWayService.deletePayWayByIds(ids));
    }
}
