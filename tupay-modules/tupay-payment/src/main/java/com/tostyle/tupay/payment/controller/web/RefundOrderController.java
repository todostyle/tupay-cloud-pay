package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.service.IRefundOrderService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 退款订单Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/refundorder")
public class RefundOrderController extends BaseController
{
    @Autowired
    private IRefundOrderService refundOrderService;

    /**
     * 查询退款订单列表
     */
    @RequiresPermissions("payment:refundorder:list")
    @GetMapping("/list")
    public TableDataInfo list(RefundOrder refundOrder)
    {
        startPage();
        List<RefundOrder> list = refundOrderService.selectRefundOrderList(refundOrder);
        return getDataTable(list);
    }

    /**
     * 导出退款订单列表
     */
    @RequiresPermissions("payment:refundorder:export")
    @Log(title = "退款订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RefundOrder refundOrder)
    {
        List<RefundOrder> list = refundOrderService.selectRefundOrderList(refundOrder);
        ExcelUtil<RefundOrder> util = new ExcelUtil<RefundOrder>(RefundOrder.class);
        util.exportExcel(response, list, "退款订单数据");
    }

    /**
     * 获取退款订单详细信息
     */
    @RequiresPermissions("payment:refundorder:query")
    @GetMapping(value = "/{refundOrderId}")
    public AjaxResult getInfo(@PathVariable("refundOrderId") String refundOrderId)
    {
        return AjaxResult.success(refundOrderService.selectRefundOrderByRefundOrderId(refundOrderId));
    }

    /**
     * 新增退款订单
     */
    @RequiresPermissions("payment:refundorder:add")
    @Log(title = "退款订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RefundOrder refundOrder)
    {
        return toAjax(refundOrderService.insertRefundOrder(refundOrder));
    }

    /**
     * 修改退款订单
     */
    @RequiresPermissions("payment:refundorder:edit")
    @Log(title = "退款订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RefundOrder refundOrder)
    {
        return toAjax(refundOrderService.updateRefundOrder(refundOrder));
    }

    /**
     * 删除退款订单
     */
    @RequiresPermissions("payment:refundorder:remove")
    @Log(title = "退款订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{refundOrderIds}")
    public AjaxResult remove(@PathVariable String[] refundOrderIds)
    {
        return toAjax(refundOrderService.deleteRefundOrderByRefundOrderIds(refundOrderIds));
    }
}
