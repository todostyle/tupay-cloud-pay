package com.tostyle.tupay.payment.controller.web;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tostyle.tupay.common.log.annotation.Log;
import com.tostyle.tupay.common.log.enums.BusinessType;
import com.tostyle.tupay.common.security.annotation.RequiresPermissions;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.service.ITransferOrderService;
import com.tostyle.tupay.common.core.web.controller.BaseController;
import com.tostyle.tupay.common.core.web.domain.AjaxResult;
import com.tostyle.tupay.common.core.utils.poi.ExcelUtil;
import com.tostyle.tupay.common.core.web.page.TableDataInfo;

/**
 * 转账订单Controller
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@RestController
@RequestMapping("/transferorder")
public class TransferOrderController extends BaseController
{
    @Autowired
    private ITransferOrderService transferOrderService;

    /**
     * 查询转账订单列表
     */
    @RequiresPermissions("payment:transferorder:list")
    @GetMapping("/list")
    public TableDataInfo list(TransferOrder transferOrder)
    {
        startPage();
        List<TransferOrder> list = transferOrderService.selectTransferOrderList(transferOrder);
        return getDataTable(list);
    }

    /**
     * 导出转账订单列表
     */
    @RequiresPermissions("payment:transferorder:export")
    @Log(title = "转账订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TransferOrder transferOrder)
    {
        List<TransferOrder> list = transferOrderService.selectTransferOrderList(transferOrder);
        ExcelUtil<TransferOrder> util = new ExcelUtil<TransferOrder>(TransferOrder.class);
        util.exportExcel(response, list, "转账订单数据");
    }

    /**
     * 获取转账订单详细信息
     */
    @RequiresPermissions("payment:transferorder:query")
    @GetMapping(value = "/{transferId}")
    public AjaxResult getInfo(@PathVariable("transferId") String transferId)
    {
        return AjaxResult.success(transferOrderService.selectTransferOrderByTransferId(transferId));
    }

    /**
     * 新增转账订单
     */
    @RequiresPermissions("payment:transferorder:add")
    @Log(title = "转账订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TransferOrder transferOrder)
    {
        return toAjax(transferOrderService.insertTransferOrder(transferOrder));
    }

    /**
     * 修改转账订单
     */
    @RequiresPermissions("payment:transferorder:edit")
    @Log(title = "转账订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TransferOrder transferOrder)
    {
        return toAjax(transferOrderService.updateTransferOrder(transferOrder));
    }

    /**
     * 删除转账订单
     */
    @RequiresPermissions("payment:transferorder:remove")
    @Log(title = "转账订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{transferIds}")
    public AjaxResult remove(@PathVariable String[] transferIds)
    {
        return toAjax(transferOrderService.deleteTransferOrderByTransferIds(transferIds));
    }
}