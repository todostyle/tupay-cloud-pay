package com.tostyle.tupay.payment.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 应用列表对象 mch_app
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class MchApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String appName;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 应用状态: 0-停用, 1-正常 */
    @Excel(name = "应用状态: 0-停用, 1-正常")
    private Long state;

    /** 应用私钥 */
    private String appSecret;

    /** 授权回调地址 */
    @Excel(name = "授权回调地址")
    private String notifyUrl;

    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setAppName(String appName) 
    {
        this.appName = appName;
    }

    public String getAppName() 
    {
        return appName;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setAppSecret(String appSecret) 
    {
        this.appSecret = appSecret;
    }

    public String getAppSecret() 
    {
        return appSecret;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appId", getAppId())
            .append("appName", getAppName())
            .append("mchNo", getMchNo())
            .append("state", getState())
            .append("appSecret", getAppSecret())
            .append("notifyUrl", getNotifyUrl())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
