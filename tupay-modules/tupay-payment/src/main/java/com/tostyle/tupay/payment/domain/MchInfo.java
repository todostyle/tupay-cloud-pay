package com.tostyle.tupay.payment.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 商户列表对象 mch_info
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class MchInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 商户简称 */
    @Excel(name = "商户简称")
    private String shortName;

    /** 商户类型: 1-普通商户, 2-特约商户(服务商模式) */
    @Excel(name = "商户类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Long type;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String name;

    /** 联系人手机号 */
    @Excel(name = "联系人手机号")
    private String mobile;

    /** 联系人邮箱 */
    @Excel(name = "联系人邮箱")
    private String email;

    /** 商户状态: 0-停用, 1-正常 */
    @Excel(name = "商户状态: 0-停用, 1-正常")
    private Long state;

    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setShortName(String shortName) 
    {
        this.shortName = shortName;
    }

    public String getShortName() 
    {
        return shortName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("mchNo", getMchNo())
            .append("mchName", getMchName())
            .append("shortName", getShortName())
            .append("type", getType())
            .append("isvNo", getIsvNo())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("email", getEmail())
            .append("state", getState())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
