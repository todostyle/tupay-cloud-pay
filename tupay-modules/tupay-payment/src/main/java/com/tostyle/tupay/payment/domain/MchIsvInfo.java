package com.tostyle.tupay.payment.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 服务商列表对象 mch_isv_info
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class MchIsvInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 服务商名称 */
    @Excel(name = "服务商名称")
    private String isvName;

    /** 服务商简称 */
    @Excel(name = "服务商简称")
    private String isvShortName;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String name;

    /** 联系人手机号 */
    @Excel(name = "联系人手机号")
    private String mobile;

    /** 联系人邮箱 */
    private String email;

    /** 状态: 0-停用, 1-正常 */
    @Excel(name = "状态: 0-停用, 1-正常")
    private Long state;

    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setIsvName(String isvName) 
    {
        this.isvName = isvName;
    }

    public String getIsvName() 
    {
        return isvName;
    }
    public void setIsvShortName(String isvShortName) 
    {
        this.isvShortName = isvShortName;
    }

    public String getIsvShortName() 
    {
        return isvShortName;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("isvNo", getIsvNo())
            .append("isvName", getIsvName())
            .append("isvShortName", getIsvShortName())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("email", getEmail())
            .append("state", getState())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
