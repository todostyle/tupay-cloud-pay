package com.tostyle.tupay.payment.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 商户通知对象 mch_notify
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class MchNotify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户通知记录ID */
    @Excel(name = "商户通知记录ID")
    private Long notifyId;

    /** 订单ID */
    @Excel(name = "订单ID")
    private String orderId;

    /** 订单类型:1-支付,2-退款 */
    @Excel(name = "订单类型:1-支付,2-退款")
    private Long orderType;

    /** 商户订单号 */
    @Excel(name = "商户订单号")
    private String mchOrderNo;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 通知地址 */
    @Excel(name = "通知地址")
    private String notifyUrl;

    /** 通知响应结果 */
    @Excel(name = "通知响应结果")
    private String resResult;

    /** 通知次数 */
    @Excel(name = "通知次数")
    private Long notifyCount;

    /** 最大通知次数, 默认6次 */
    @Excel(name = "最大通知次数, 默认6次")
    private Long notifyCountLimit;

    /** 通知状态,1-通知中,2-通知成功,3-通知失败 */
    @Excel(name = "通知状态,1-通知中,2-通知成功,3-通知失败")
    private Long state;

    /** 最后一次通知时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后一次通知时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastNotifyTime;

    public void setNotifyId(Long notifyId) 
    {
        this.notifyId = notifyId;
    }

    public Long getNotifyId() 
    {
        return notifyId;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setOrderType(Long orderType) 
    {
        this.orderType = orderType;
    }

    public Long getOrderType() 
    {
        return orderType;
    }
    public void setMchOrderNo(String mchOrderNo) 
    {
        this.mchOrderNo = mchOrderNo;
    }

    public String getMchOrderNo() 
    {
        return mchOrderNo;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setResResult(String resResult) 
    {
        this.resResult = resResult;
    }

    public String getResResult() 
    {
        return resResult;
    }
    public void setNotifyCount(Long notifyCount) 
    {
        this.notifyCount = notifyCount;
    }

    public Long getNotifyCount() 
    {
        return notifyCount;
    }
    public void setNotifyCountLimit(Long notifyCountLimit) 
    {
        this.notifyCountLimit = notifyCountLimit;
    }

    public Long getNotifyCountLimit() 
    {
        return notifyCountLimit;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setLastNotifyTime(Date lastNotifyTime) 
    {
        this.lastNotifyTime = lastNotifyTime;
    }

    public Date getLastNotifyTime() 
    {
        return lastNotifyTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("notifyId", getNotifyId())
            .append("orderId", getOrderId())
            .append("orderType", getOrderType())
            .append("mchOrderNo", getMchOrderNo())
            .append("mchNo", getMchNo())
            .append("isvNo", getIsvNo())
            .append("appId", getAppId())
            .append("notifyUrl", getNotifyUrl())
            .append("resResult", getResResult())
            .append("notifyCount", getNotifyCount())
            .append("notifyCountLimit", getNotifyCountLimit())
            .append("state", getState())
            .append("lastNotifyTime", getLastNotifyTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
