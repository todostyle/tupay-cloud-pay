package com.tostyle.tupay.payment.domain;

import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 商户支付通道对象 mch_pay_channel
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
public class MchPayChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 支付接口 */
    @Excel(name = "支付接口")
    private String channelCode;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String wayCode;

    /** 支付方式费率 */
    @Excel(name = "支付方式费率")
    private BigDecimal rate;

    /** 风控数据 */
    @Excel(name = "风控数据")
    private String riskConfig;

    /** 状态: 0-停用, 1-启用 */
    @Excel(name = "状态: 0-停用, 1-启用")
    private Long state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setRate(BigDecimal rate) 
    {
        this.rate = rate;
    }

    public BigDecimal getRate() 
    {
        return rate;
    }
    public void setRiskConfig(String riskConfig) 
    {
        this.riskConfig = riskConfig;
    }

    public String getRiskConfig() 
    {
        return riskConfig;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mchNo", getMchNo())
            .append("appId", getAppId())
            .append("channelCode", getChannelCode())
            .append("wayCode", getWayCode())
            .append("rate", getRate())
            .append("riskConfig", getRiskConfig())
            .append("state", getState())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}