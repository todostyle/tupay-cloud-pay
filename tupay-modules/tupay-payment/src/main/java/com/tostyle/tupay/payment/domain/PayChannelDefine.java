package com.tostyle.tupay.payment.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 支付渠道定义对象 pay_channel_define
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class PayChannelDefine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 渠道代码 全小写  wxpay alipay  */
    @Excel(name = "渠道代码 全小写  wxpay alipay ")
    private String channelCode;

    /** 渠道名称 */
    @Excel(name = "渠道名称")
    private String channelName;

    /** 是否支持普通商户模式: 0-不支持, 1-支持 */
    @Excel(name = "是否支持普通商户模式: 0-不支持, 1-支持")
    private Long isMchMode;

    /** 是否支持服务商子商户模式: 0-不支持, 1-支持 */
    @Excel(name = "是否支持服务商子商户模式: 0-不支持, 1-支持")
    private Long isIsvMode;

    /** 参数配置页面类型:1-JSON渲染,2-自定义 */
    @Excel(name = "参数配置页面类型:1-JSON渲染,2-自定义")
    private Long paramPageType;

    /** ISV接口配置定义描述,json字符串 */
    @Excel(name = "ISV接口配置定义描述,json字符串")
    private String isvParams;

    /** 特约商户接口配置定义描述,json字符串 */
    @Excel(name = "特约商户接口配置定义描述,json字符串")
    private String isvsubMchParams;

    /** 普通商户接口配置定义描述,json字符串 */
    @Excel(name = "普通商户接口配置定义描述,json字符串")
    private String normalMchParams;

    /** 支持的支付方式 ["wxpay_jsapi", "wxpay_bar"] */
    @Excel(name = "支持的支付方式")
    private String wayCodes;

    /** 页面展示：卡片-图标 */
    @Excel(name = "页面展示：卡片-图标")
    private String icon;

    /** 页面展示：卡片-背景色 */
    @Excel(name = "页面展示：卡片-背景色")
    private String bgColor;

    /** 状态: 0-停用, 1-启用 */
    @Excel(name = "状态: 0-停用, 1-启用")
    private Long state;

    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setChannelName(String channelName) 
    {
        this.channelName = channelName;
    }

    public String getChannelName() 
    {
        return channelName;
    }
    public void setIsMchMode(Long isMchMode) 
    {
        this.isMchMode = isMchMode;
    }

    public Long getIsMchMode() 
    {
        return isMchMode;
    }
    public void setIsIsvMode(Long isIsvMode) 
    {
        this.isIsvMode = isIsvMode;
    }

    public Long getIsIsvMode() 
    {
        return isIsvMode;
    }
    public void setParamPageType(Long paramPageType) 
    {
        this.paramPageType = paramPageType;
    }

    public Long getParamPageType() 
    {
        return paramPageType;
    }
    public void setIsvParams(String isvParams) 
    {
        this.isvParams = isvParams;
    }

    public String getIsvParams() 
    {
        return isvParams;
    }
    public void setIsvsubMchParams(String isvsubMchParams) 
    {
        this.isvsubMchParams = isvsubMchParams;
    }

    public String getIsvsubMchParams() 
    {
        return isvsubMchParams;
    }
    public void setNormalMchParams(String normalMchParams) 
    {
        this.normalMchParams = normalMchParams;
    }

    public String getNormalMchParams() 
    {
        return normalMchParams;
    }
    public void setWayCodes(String wayCodes) 
    {
        this.wayCodes = wayCodes;
    }

    public String getWayCodes() 
    {
        return wayCodes;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setBgColor(String bgColor) 
    {
        this.bgColor = bgColor;
    }

    public String getBgColor() 
    {
        return bgColor;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("channelCode", getChannelCode())
            .append("channelName", getChannelName())
            .append("isMchMode", getIsMchMode())
            .append("isIsvMode", getIsIsvMode())
            .append("paramPageType", getParamPageType())
            .append("isvParams", getIsvParams())
            .append("isvsubMchParams", getIsvsubMchParams())
            .append("normalMchParams", getNormalMchParams())
            .append("wayCodes", getWayCodes())
            .append("icon", getIcon())
            .append("bgColor", getBgColor())
            .append("state", getState())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
