package com.tostyle.tupay.payment.domain;

import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 支付接口配置参数对象 pay_channel_param
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
public class PayChannelParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 账号类型:1-服务商 2-商户 3-商户应用 */
    @Excel(name = "账号类型:1-服务商 2-商户 3-商户应用")
    private Long infoType;

    /** 服务商号/商户号/应用ID */
    @Excel(name = "服务商号/商户号/应用ID")
    private String infoId;

    /** 支付接口代码 */
    @Excel(name = "支付接口代码")
    private String channelCode;

    /** 接口配置参数,json字符串 */
    @Excel(name = "接口配置参数,json字符串")
    private String channelParams;

    /** 支付接口费率 */
    @Excel(name = "支付接口费率")
    private BigDecimal channelRate;

    /** 状态: 0-停用, 1-启用 */
    @Excel(name = "状态: 0-停用, 1-启用")
    private Long state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInfoType(Long infoType) 
    {
        this.infoType = infoType;
    }

    public Long getInfoType() 
    {
        return infoType;
    }
    public void setInfoId(String infoId) 
    {
        this.infoId = infoId;
    }

    public String getInfoId() 
    {
        return infoId;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setChannelParams(String channelParams) 
    {
        this.channelParams = channelParams;
    }

    public String getChannelParams() 
    {
        return channelParams;
    }
    public void setChannelRate(BigDecimal channelRate) 
    {
        this.channelRate = channelRate;
    }

    public BigDecimal getChannelRate() 
    {
        return channelRate;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("infoType", getInfoType())
            .append("infoId", getInfoId())
            .append("channelCode", getChannelCode())
            .append("channelParams", getChannelParams())
            .append("channelRate", getChannelRate())
            .append("state", getState())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}