package com.tostyle.tupay.payment.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 支付订单对象 pay_order
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class PayOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 支付订单号 */
    private String tradeNo;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Long mchType;

    /** 商户订单号 */
    @Excel(name = "商户订单号")
    private String mchOrderNo;

    /** 支付接口代码 */
    @Excel(name = "支付接口代码")
    private String channelCode;

    /** 支付方式代码 */
    @Excel(name = "支付方式代码")
    private String wayCode;

    /** 支付金额,单位分 */
    @Excel(name = "支付金额,单位分")
    private Long amount;

    /** 商户手续费费率快照 */
    @Excel(name = "商户手续费费率快照")
    private BigDecimal mchFeeRate;

    /** 商户手续费,单位分 */
    @Excel(name = "商户手续费,单位分")
    private Long mchFeeAmount;

    /** 三位货币代码,人民币:cny */
    @Excel(name = "三位货币代码,人民币:cny")
    private String currency;

    /** 支付状态: 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭 */
    @Excel(name = "支付状态: 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭")
    private Long state;

    /** 向下游回调状态, 0-未发送,  1-已发送 */
    @Excel(name = "向下游回调状态, 0-未发送,  1-已发送")
    private Long notifyState;

    /** 客户端IP */
    @Excel(name = "客户端IP")
    private String clientIp;

    /** 商品标题 */
    @Excel(name = "商品标题")
    private String subject;

    /** 商品描述信息 */
    @Excel(name = "商品描述信息")
    private String body;

    /** 特定渠道发起额外参数 */
    @Excel(name = "特定渠道发起额外参数")
    private String channelExtra;

    /** 渠道用户标识,如微信openId,支付宝账号 */
    @Excel(name = "渠道用户标识,如微信openId,支付宝账号")
    private String channelUser;

    /** 渠道订单号 */
    @Excel(name = "渠道订单号")
    private String channelOrderNo;

    /** 退款状态: 0-未发生实际退款, 1-部分退款, 2-全额退款 */
    private Long refundState;

    /** 退款次数 */
    private Long refundTimes;

    /** 退款总金额,单位分 */
    private Long refundAmount;

    /** 订单分账模式：0-该笔订单不允许分账, 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额) */
    private Long divisionMode;

    /** 订单分账状态：0-未发生分账, 1-等待分账任务处理, 2-分账处理中, 3-分账任务已结束(不体现状态) */
    private Long divisionState;

    /** 最新分账时间 */
    private Date divisionLastTime;

    /** 渠道支付错误码 */
    private String errCode;

    /** 渠道支付错误描述 */
    private String errMsg;

    /** 商户扩展参数 */
    private String extParam;

    /** 异步通知地址 */
    private String notifyUrl;

    /** 页面跳转地址 */
    private String returnUrl;

    /** 订单失效时间 */
    private Date expiredTime;

    /** 订单支付成功时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单支付成功时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date successTime;

    public void setTradeNo(String tradeNo) 
    {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() 
    {
        return tradeNo;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setMchType(Long mchType) 
    {
        this.mchType = mchType;
    }

    public Long getMchType() 
    {
        return mchType;
    }
    public void setMchOrderNo(String mchOrderNo) 
    {
        this.mchOrderNo = mchOrderNo;
    }

    public String getMchOrderNo() 
    {
        return mchOrderNo;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setMchFeeRate(BigDecimal mchFeeRate) 
    {
        this.mchFeeRate = mchFeeRate;
    }

    public BigDecimal getMchFeeRate() 
    {
        return mchFeeRate;
    }
    public void setMchFeeAmount(Long mchFeeAmount) 
    {
        this.mchFeeAmount = mchFeeAmount;
    }

    public Long getMchFeeAmount() 
    {
        return mchFeeAmount;
    }
    public void setCurrency(String currency) 
    {
        this.currency = currency;
    }

    public String getCurrency() 
    {
        return currency;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setNotifyState(Long notifyState) 
    {
        this.notifyState = notifyState;
    }

    public Long getNotifyState() 
    {
        return notifyState;
    }
    public void setClientIp(String clientIp) 
    {
        this.clientIp = clientIp;
    }

    public String getClientIp() 
    {
        return clientIp;
    }
    public void setSubject(String subject) 
    {
        this.subject = subject;
    }

    public String getSubject() 
    {
        return subject;
    }
    public void setBody(String body) 
    {
        this.body = body;
    }

    public String getBody() 
    {
        return body;
    }
    public void setChannelExtra(String channelExtra) 
    {
        this.channelExtra = channelExtra;
    }

    public String getChannelExtra() 
    {
        return channelExtra;
    }
    public void setChannelUser(String channelUser) 
    {
        this.channelUser = channelUser;
    }

    public String getChannelUser() 
    {
        return channelUser;
    }
    public void setChannelOrderNo(String channelOrderNo) 
    {
        this.channelOrderNo = channelOrderNo;
    }

    public String getChannelOrderNo() 
    {
        return channelOrderNo;
    }
    public void setRefundState(Long refundState) 
    {
        this.refundState = refundState;
    }

    public Long getRefundState() 
    {
        return refundState;
    }
    public void setRefundTimes(Long refundTimes) 
    {
        this.refundTimes = refundTimes;
    }

    public Long getRefundTimes() 
    {
        return refundTimes;
    }
    public void setRefundAmount(Long refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public Long getRefundAmount() 
    {
        return refundAmount;
    }
    public void setDivisionMode(Long divisionMode) 
    {
        this.divisionMode = divisionMode;
    }

    public Long getDivisionMode() 
    {
        return divisionMode;
    }
    public void setDivisionState(Long divisionState) 
    {
        this.divisionState = divisionState;
    }

    public Long getDivisionState() 
    {
        return divisionState;
    }
    public void setDivisionLastTime(Date divisionLastTime) 
    {
        this.divisionLastTime = divisionLastTime;
    }

    public Date getDivisionLastTime() 
    {
        return divisionLastTime;
    }
    public void setErrCode(String errCode) 
    {
        this.errCode = errCode;
    }

    public String getErrCode() 
    {
        return errCode;
    }
    public void setErrMsg(String errMsg) 
    {
        this.errMsg = errMsg;
    }

    public String getErrMsg() 
    {
        return errMsg;
    }
    public void setExtParam(String extParam) 
    {
        this.extParam = extParam;
    }

    public String getExtParam() 
    {
        return extParam;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setReturnUrl(String returnUrl) 
    {
        this.returnUrl = returnUrl;
    }

    public String getReturnUrl() 
    {
        return returnUrl;
    }
    public void setExpiredTime(Date expiredTime) 
    {
        this.expiredTime = expiredTime;
    }

    public Date getExpiredTime() 
    {
        return expiredTime;
    }
    public void setSuccessTime(Date successTime) 
    {
        this.successTime = successTime;
    }

    public Date getSuccessTime() 
    {
        return successTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tradeNo", getTradeNo())
            .append("mchNo", getMchNo())
            .append("isvNo", getIsvNo())
            .append("appId", getAppId())
            .append("mchName", getMchName())
            .append("mchType", getMchType())
            .append("mchOrderNo", getMchOrderNo())
            .append("channelCode", getChannelCode())
            .append("wayCode", getWayCode())
            .append("amount", getAmount())
            .append("mchFeeRate", getMchFeeRate())
            .append("mchFeeAmount", getMchFeeAmount())
            .append("currency", getCurrency())
            .append("state", getState())
            .append("notifyState", getNotifyState())
            .append("clientIp", getClientIp())
            .append("subject", getSubject())
            .append("body", getBody())
            .append("channelExtra", getChannelExtra())
            .append("channelUser", getChannelUser())
            .append("channelOrderNo", getChannelOrderNo())
            .append("refundState", getRefundState())
            .append("refundTimes", getRefundTimes())
            .append("refundAmount", getRefundAmount())
            .append("divisionMode", getDivisionMode())
            .append("divisionState", getDivisionState())
            .append("divisionLastTime", getDivisionLastTime())
            .append("errCode", getErrCode())
            .append("errMsg", getErrMsg())
            .append("extParam", getExtParam())
            .append("notifyUrl", getNotifyUrl())
            .append("returnUrl", getReturnUrl())
            .append("expiredTime", getExpiredTime())
            .append("successTime", getSuccessTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
