package com.tostyle.tupay.payment.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 支付方式对象 pay_way
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class PayWay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 支付方式代码  例如： wxpay_jsapi */
    @Excel(name = "支付方式代码  例如： wxpay_jsapi")
    private String wayCode;

    /** 支付方式名称 */
    @Excel(name = "支付方式名称")
    private String wayName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setWayName(String wayName) 
    {
        this.wayName = wayName;
    }

    public String getWayName() 
    {
        return wayName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wayCode", getWayCode())
            .append("wayName", getWayName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
