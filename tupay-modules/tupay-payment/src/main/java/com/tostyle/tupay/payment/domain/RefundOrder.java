package com.tostyle.tupay.payment.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 退款订单对象 refund_order
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class RefundOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 退款订单号（支付系统生成订单号） */
    @Excel(name = "退款订单号", readConverterExp = "支=付系统生成订单号")
    private String refundOrderId;

    /** 支付订单号（与t_pay_order对应） */
    @Excel(name = "支付订单号", readConverterExp = "与=t_pay_order对应")
    private String tradeNo;

    /** 渠道支付单号（与t_pay_order channel_order_no对应） */
    @Excel(name = "渠道支付单号", readConverterExp = "与=t_pay_order,c=hannel_order_no对应")
    private String channelPayOrderNo;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Long mchType;

    /** 商户退款单号（商户系统的订单号） */
    @Excel(name = "商户退款单号", readConverterExp = "商=户系统的订单号")
    private String mchRefundNo;

    /** 支付方式代码 */
    @Excel(name = "支付方式代码")
    private String wayCode;

    /** 支付接口代码 */
    @Excel(name = "支付接口代码")
    private String channelCode;

    /** 支付金额,单位分 */
    @Excel(name = "支付金额,单位分")
    private Long payAmount;

    /** 退款金额,单位分 */
    @Excel(name = "退款金额,单位分")
    private Long refundAmount;

    /** 三位货币代码,人民币:cny */
    @Excel(name = "三位货币代码,人民币:cny")
    private String currency;

    /** 退款状态:0-订单生成,1-退款中,2-退款成功,3-退款失败,4-退款任务关闭 */
    @Excel(name = "退款状态:0-订单生成,1-退款中,2-退款成功,3-退款失败,4-退款任务关闭")
    private Long state;

    /** 客户端IP */
    @Excel(name = "客户端IP")
    private String clientIp;

    /** 退款原因 */
    private String refundReason;

    /** 渠道订单号 */
    private String channelOrderNo;

    /** 渠道错误码 */
    private String errCode;

    /** 渠道错误描述 */
    private String errMsg;

    /** 特定渠道发起时额外参数 */
    private String channelExtra;

    /** 通知地址 */
    private String notifyUrl;

    /** 扩展参数 */
    private String extParam;

    /** 订单退款成功时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单退款成功时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date successTime;

    /** 退款失效时间（失效后系统更改为退款任务关闭状态） */
    private Date expiredTime;

    public void setRefundOrderId(String refundOrderId) 
    {
        this.refundOrderId = refundOrderId;
    }

    public String getRefundOrderId() 
    {
        return refundOrderId;
    }
    public void setTradeNo(String tradeNo) 
    {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() 
    {
        return tradeNo;
    }
    public void setChannelPayOrderNo(String channelPayOrderNo) 
    {
        this.channelPayOrderNo = channelPayOrderNo;
    }

    public String getChannelPayOrderNo() 
    {
        return channelPayOrderNo;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setMchType(Long mchType) 
    {
        this.mchType = mchType;
    }

    public Long getMchType() 
    {
        return mchType;
    }
    public void setMchRefundNo(String mchRefundNo) 
    {
        this.mchRefundNo = mchRefundNo;
    }

    public String getMchRefundNo() 
    {
        return mchRefundNo;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setPayAmount(Long payAmount) 
    {
        this.payAmount = payAmount;
    }

    public Long getPayAmount() 
    {
        return payAmount;
    }
    public void setRefundAmount(Long refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public Long getRefundAmount() 
    {
        return refundAmount;
    }
    public void setCurrency(String currency) 
    {
        this.currency = currency;
    }

    public String getCurrency() 
    {
        return currency;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setClientIp(String clientIp) 
    {
        this.clientIp = clientIp;
    }

    public String getClientIp() 
    {
        return clientIp;
    }
    public void setRefundReason(String refundReason) 
    {
        this.refundReason = refundReason;
    }

    public String getRefundReason() 
    {
        return refundReason;
    }
    public void setChannelOrderNo(String channelOrderNo) 
    {
        this.channelOrderNo = channelOrderNo;
    }

    public String getChannelOrderNo() 
    {
        return channelOrderNo;
    }
    public void setErrCode(String errCode) 
    {
        this.errCode = errCode;
    }

    public String getErrCode() 
    {
        return errCode;
    }
    public void setErrMsg(String errMsg) 
    {
        this.errMsg = errMsg;
    }

    public String getErrMsg() 
    {
        return errMsg;
    }
    public void setChannelExtra(String channelExtra) 
    {
        this.channelExtra = channelExtra;
    }

    public String getChannelExtra() 
    {
        return channelExtra;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setExtParam(String extParam) 
    {
        this.extParam = extParam;
    }

    public String getExtParam() 
    {
        return extParam;
    }
    public void setSuccessTime(Date successTime) 
    {
        this.successTime = successTime;
    }

    public Date getSuccessTime() 
    {
        return successTime;
    }
    public void setExpiredTime(Date expiredTime) 
    {
        this.expiredTime = expiredTime;
    }

    public Date getExpiredTime() 
    {
        return expiredTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("refundOrderId", getRefundOrderId())
            .append("tradeNo", getTradeNo())
            .append("channelPayOrderNo", getChannelPayOrderNo())
            .append("mchNo", getMchNo())
            .append("isvNo", getIsvNo())
            .append("appId", getAppId())
            .append("mchName", getMchName())
            .append("mchType", getMchType())
            .append("mchRefundNo", getMchRefundNo())
            .append("wayCode", getWayCode())
            .append("channelCode", getChannelCode())
            .append("payAmount", getPayAmount())
            .append("refundAmount", getRefundAmount())
            .append("currency", getCurrency())
            .append("state", getState())
            .append("clientIp", getClientIp())
            .append("refundReason", getRefundReason())
            .append("channelOrderNo", getChannelOrderNo())
            .append("errCode", getErrCode())
            .append("errMsg", getErrMsg())
            .append("channelExtra", getChannelExtra())
            .append("notifyUrl", getNotifyUrl())
            .append("extParam", getExtParam())
            .append("successTime", getSuccessTime())
            .append("expiredTime", getExpiredTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
