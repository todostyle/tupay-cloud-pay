package com.tostyle.tupay.payment.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tostyle.tupay.common.core.annotation.Excel;
import com.tostyle.tupay.common.core.web.domain.BaseEntity;

/**
 * 转账订单对象 transfer_order
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public class TransferOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 转账订单号 */
    private String transferId;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 服务商号 */
    @Excel(name = "服务商号")
    private String isvNo;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Long mchType;

    /** 商户订单号 */
    @Excel(name = "商户订单号")
    private String mchOrderNo;

    /** 支付接口代码 */
    @Excel(name = "支付接口代码")
    private String channelCode;

    /** 入账方式： WX_CASH-微信零钱; ALIPAY_CASH-支付宝转账; BANK_CARD-银行卡 */
    @Excel(name = "入账方式： WX_CASH-微信零钱; ALIPAY_CASH-支付宝转账; BANK_CARD-银行卡")
    private String entryType;

    /** 转账金额,单位分 */
    @Excel(name = "转账金额,单位分")
    private Long amount;

    /** 三位货币代码,人民币:cny */
    @Excel(name = "三位货币代码,人民币:cny")
    private String currency;

    /** 收款账号 */
    @Excel(name = "收款账号")
    private String accountNo;

    /** 收款人姓名 */
    @Excel(name = "收款人姓名")
    private String accountName;

    /** 收款人开户行名称 */
    @Excel(name = "收款人开户行名称")
    private String bankName;

    /** 转账备注信息 */
    @Excel(name = "转账备注信息")
    private String transferDesc;

    /** 客户端IP */
    @Excel(name = "客户端IP")
    private String clientIp;

    /** 支付状态: 0-订单生成, 1-转账中, 2-转账成功, 3-转账失败, 4-订单关闭 */
    @Excel(name = "支付状态: 0-订单生成, 1-转账中, 2-转账成功, 3-转账失败, 4-订单关闭")
    private Long state;

    /** 特定渠道发起额外参数 */
    private String channelExtra;

    /** 渠道订单号 */
    @Excel(name = "渠道订单号")
    private String channelOrderNo;

    /** 渠道支付错误码 */
    private String errCode;

    /** 渠道支付错误描述 */
    private String errMsg;

    /** 商户扩展参数 */
    private String extParam;

    /** 异步通知地址 */
    private String notifyUrl;

    /** 转账成功时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "转账成功时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date successTime;

    public void setTransferId(String transferId) 
    {
        this.transferId = transferId;
    }

    public String getTransferId() 
    {
        return transferId;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setMchType(Long mchType) 
    {
        this.mchType = mchType;
    }

    public Long getMchType() 
    {
        return mchType;
    }
    public void setMchOrderNo(String mchOrderNo) 
    {
        this.mchOrderNo = mchOrderNo;
    }

    public String getMchOrderNo() 
    {
        return mchOrderNo;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setEntryType(String entryType) 
    {
        this.entryType = entryType;
    }

    public String getEntryType() 
    {
        return entryType;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setCurrency(String currency) 
    {
        this.currency = currency;
    }

    public String getCurrency() 
    {
        return currency;
    }
    public void setAccountNo(String accountNo) 
    {
        this.accountNo = accountNo;
    }

    public String getAccountNo() 
    {
        return accountNo;
    }
    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setBankName(String bankName) 
    {
        this.bankName = bankName;
    }

    public String getBankName() 
    {
        return bankName;
    }
    public void setTransferDesc(String transferDesc) 
    {
        this.transferDesc = transferDesc;
    }

    public String getTransferDesc() 
    {
        return transferDesc;
    }
    public void setClientIp(String clientIp) 
    {
        this.clientIp = clientIp;
    }

    public String getClientIp() 
    {
        return clientIp;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setChannelExtra(String channelExtra) 
    {
        this.channelExtra = channelExtra;
    }

    public String getChannelExtra() 
    {
        return channelExtra;
    }
    public void setChannelOrderNo(String channelOrderNo) 
    {
        this.channelOrderNo = channelOrderNo;
    }

    public String getChannelOrderNo() 
    {
        return channelOrderNo;
    }
    public void setErrCode(String errCode) 
    {
        this.errCode = errCode;
    }

    public String getErrCode() 
    {
        return errCode;
    }
    public void setErrMsg(String errMsg) 
    {
        this.errMsg = errMsg;
    }

    public String getErrMsg() 
    {
        return errMsg;
    }
    public void setExtParam(String extParam) 
    {
        this.extParam = extParam;
    }

    public String getExtParam() 
    {
        return extParam;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setSuccessTime(Date successTime) 
    {
        this.successTime = successTime;
    }

    public Date getSuccessTime() 
    {
        return successTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("transferId", getTransferId())
            .append("mchNo", getMchNo())
            .append("isvNo", getIsvNo())
            .append("appId", getAppId())
            .append("mchName", getMchName())
            .append("mchType", getMchType())
            .append("mchOrderNo", getMchOrderNo())
            .append("channelCode", getChannelCode())
            .append("entryType", getEntryType())
            .append("amount", getAmount())
            .append("currency", getCurrency())
            .append("accountNo", getAccountNo())
            .append("accountName", getAccountName())
            .append("bankName", getBankName())
            .append("transferDesc", getTransferDesc())
            .append("clientIp", getClientIp())
            .append("state", getState())
            .append("channelExtra", getChannelExtra())
            .append("channelOrderNo", getChannelOrderNo())
            .append("errCode", getErrCode())
            .append("errMsg", getErrMsg())
            .append("extParam", getExtParam())
            .append("notifyUrl", getNotifyUrl())
            .append("successTime", getSuccessTime())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}