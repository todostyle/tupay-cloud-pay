package com.tostyle.tupay.payment.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 通用Req, 包含mchNo和appId 必填项
 * @author tostyle
 * 2022/2/22 17:24
 */
@Data
public class AbstractMchAppReq extends AbstractReq{


    /** 商户号 **/
    @NotBlank(message="商户号不能为空")
    private String mchNo;

    /** 商户应用ID **/
    @NotBlank(message="商户应用ID不能为空")
    private String appId;
}
