package com.tostyle.tupay.payment.domain.model;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.io.Serializable;

/**
 * 接口抽象Resp对象, 本身无需实例化
 * @author tostyle
 * 2022/2/22 17:21
 */
@Data
public abstract class AbstractResp  implements Serializable {

    /**
     * 返回json字符串
     * @return String
     */
    public String toJSONString(){
        return JSON.toJSONString(this);
    }
}
