package com.tostyle.tupay.payment.domain.model.payorder;

import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import lombok.Data;

/**
 * 关闭订单 请求参数对象
 * @author tostyle
 * 2022/3/2 16:52
 */
@Data
public class ClosePayOrderReq extends AbstractMchAppReq {

    /** 商户订单号 **/
    private String mchOrderNo;

    /** 支付系统订单号 **/
    private String tradeNo;
}
