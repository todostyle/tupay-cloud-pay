package com.tostyle.tupay.payment.domain.model.payorder;

import com.alibaba.fastjson.annotation.JSONField;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import lombok.Data;

/**
 * @author tostyle
 * 2022/3/2 16:53
 */
@Data
public class ClosePayOrderResp extends AbstractResp {

    /** 上游渠道返回数据包 (无需JSON序列化) **/
    @JSONField(serialize = false)
    private ChannelRetMsg channelRetMsg;
}
