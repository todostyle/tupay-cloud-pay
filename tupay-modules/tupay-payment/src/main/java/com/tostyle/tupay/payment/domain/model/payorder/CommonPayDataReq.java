package com.tostyle.tupay.payment.domain.model.payorder;

import lombok.Data;

/**
 * @author tostyle
 * 2022/2/22 17:34
 */
@Data
public class CommonPayDataReq extends UnifiedOrderReq {

    /** 请求参数： 支付数据包类型 **/
    private String payDataType;
}
