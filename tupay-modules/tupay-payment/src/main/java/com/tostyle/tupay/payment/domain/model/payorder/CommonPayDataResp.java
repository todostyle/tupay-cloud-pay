package com.tostyle.tupay.payment.domain.model.payorder;


import com.tostyle.tupay.common.core.model.PayConstant;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author tostyle
 * 2022/2/22 17:34
 */
@Data
public class CommonPayDataResp extends UnifiedOrderResp{

    /** 跳转地址 **/
    private String payUrl;

    /** 二维码地址 **/
    private String codeUrl;

    /** 二维码图片地址 **/
    private String codeImgUrl;

    /** 表单内容 **/
    private String formContent;

    @Override
    public String buildPayDataType(){

        if(StringUtils.isNotEmpty(payUrl)){
            return PayConstant.PAY_DATA_TYPE.PAY_URL;
        }

        if(StringUtils.isNotEmpty(codeUrl)){
            return PayConstant.PAY_DATA_TYPE.CODE_URL;
        }

        if(StringUtils.isNotEmpty(codeImgUrl)){
            return PayConstant.PAY_DATA_TYPE.CODE_IMG_URL;
        }

        if(StringUtils.isNotEmpty(formContent)){
            return PayConstant.PAY_DATA_TYPE.FORM;
        }

        return PayConstant.PAY_DATA_TYPE.PAY_URL;
    }

    @Override
    public String buildPayData(){

        if(StringUtils.isNotEmpty(payUrl)){
            return payUrl;
        }

        if(StringUtils.isNotEmpty(codeUrl)){
            return codeUrl;
        }

        if(StringUtils.isNotEmpty(codeImgUrl)){
            return codeImgUrl;
        }

        if(StringUtils.isNotEmpty(formContent)){
            return formContent;
        }

        return "";
    }
}
