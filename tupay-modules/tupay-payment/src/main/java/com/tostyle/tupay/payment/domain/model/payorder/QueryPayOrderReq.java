package com.tostyle.tupay.payment.domain.model.payorder;


import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import lombok.Data;

/**
 * @author tostyle
 * 2022/2/22 17:35
 */
@Data
public class QueryPayOrderReq extends AbstractMchAppReq {

    /** 商户订单号 **/
    private String mchOrderNo;

    /** 支付系统订单号 **/
    private String tradeNo;

}
