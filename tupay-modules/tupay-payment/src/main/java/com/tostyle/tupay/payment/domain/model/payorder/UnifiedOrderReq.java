package com.tostyle.tupay.payment.domain.model.payorder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import com.tostyle.tupay.payment.domain.model.payorder.alipay.*;
import com.tostyle.tupay.payment.domain.model.payorder.juhepay.AutoBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.juhepay.QrCashierOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxH5OrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxJsapiOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.wxpay.WxNativeOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.ysfpay.YsfBarOrderReq;
import com.tostyle.tupay.payment.domain.model.payorder.ysfpay.YsfJsapiOrderReq;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 创建订单请求参数对象
 * 聚合支付接口（统一下单）
 * @author tostyle
 * 2022/2/22 17:26
 */
@Data
public class UnifiedOrderReq extends AbstractMchAppReq {

    /** 商户订单号 **/
    @NotBlank(message="商户订单号不能为空")
    private String mchOrderNo;

    /** 支付方式  如： wxpay_jsapi,alipay_wap等   **/
    @NotBlank(message="支付方式不能为空")
    private String wayCode;

    /** 支付金额， 单位：分 **/
    @NotNull(message="支付金额不能为空")
    @Min(value = 1, message = "支付金额不能为空")
    private Long amount;

    /** 货币代码 **/
    @NotBlank(message="货币代码不能为空")
    private String currency;

    /** 客户端IP地址 **/
    private String clientIp;

    /** 商品标题 **/
    @NotBlank(message="商品标题不能为空")
    private String subject;

    /** 商品描述信息 **/
    @NotBlank(message="商品描述信息不能为空")
    private String body;

    /** 异步通知地址 **/
    private String notifyUrl;

    /** 跳转通知地址 **/
    private String returnUrl;

    /** 订单失效时间, 单位：秒 **/
    private Integer expiredTime;

    /** 特定渠道发起额外参数 **/
    private String channelExtra;

    /** 商户扩展参数 **/
    private String extParam;

    /** 分账模式： 0-该笔订单不允许分账, 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额) **/
    @Range(min = 0, max = 2, message = "分账模式设置值有误")
    private Long divisionMode;

    /** 返回真实的bizRQ **/
    public UnifiedOrderReq buildBizReq(){

        if(PayConstant.PAY_WAY_CODE.ALI_BAR.equals(wayCode)){
            AliBarOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliBarOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.ALI_JSAPI.equals(wayCode)){
            AliJsapiOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliJsapiOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.QR_CASHIER.equals(wayCode)){
            QrCashierOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), QrCashierOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.WX_JSAPI.equals(wayCode) || PayConstant.PAY_WAY_CODE.WX_LITE.equals(wayCode)){
            WxJsapiOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), WxJsapiOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.WX_BAR.equals(wayCode)){
            WxBarOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), WxBarOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.WX_NATIVE.equals(wayCode)){
            WxNativeOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), WxNativeOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.WX_H5.equals(wayCode)){
            WxH5OrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), WxH5OrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.YSF_BAR.equals(wayCode)){
            YsfBarOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), YsfBarOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.YSF_JSAPI.equals(wayCode)){
            YsfJsapiOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), YsfJsapiOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.AUTO_BAR.equals(wayCode)){
            AutoBarOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AutoBarOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.ALI_APP.equals(wayCode)){
            AliAppOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliAppOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.ALI_WAP.equals(wayCode)){
            AliWapOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliWapOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.ALI_PC.equals(wayCode)){
            AliPcOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliPcOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }else if(PayConstant.PAY_WAY_CODE.ALI_QR.equals(wayCode)){
            AliQrOrderReq bizReq = JSON.parseObject(StringUtils.defaultIfEmpty(this.channelExtra, "{}"), AliQrOrderReq.class);
            BeanUtils.copyProperties(this, bizReq);
            return bizReq;
        }
        return this;
    }




    /** 获取渠道用户ID **/
    @JSONField(serialize = false)
    public String getChannelUserId(){
        return null;
    }
}
