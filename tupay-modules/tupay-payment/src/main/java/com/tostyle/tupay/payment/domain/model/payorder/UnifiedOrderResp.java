package com.tostyle.tupay.payment.domain.model.payorder;

import com.alibaba.fastjson.annotation.JSONField;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.msg.ChannelRetMsg;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import lombok.Data;

/**
 * @author tostyle
 * 2022/2/22 17:29
 */
@Data
public class UnifiedOrderResp extends AbstractResp {

    /** 支付订单号 **/
    private String tradeNo;

    /** 商户订单号 **/
    private String mchOrderNo;

    /** 订单状态 **/
    private Long orderState;

    /** 支付参数类型  ( 无参数，  调起支付插件参数， 重定向到指定地址，  用户扫码   )   **/
    private String payDataType;

    /** 支付参数 **/
    private String payData;

    /** 渠道返回错误代码 **/
    private String errCode;

    /** 渠道返回错误信息 **/
    private String errMsg;

    /** 上游渠道返回数据包 (无需JSON序列化) **/
    @JSONField(serialize = false)
    private ChannelRetMsg channelRetMsg;

    /** 生成聚合支付参数 (仅统一下单接口使用) **/
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.NONE;
    }

    /** 生成支付参数 **/
    public String buildPayData(){
        return "";
    }
}
