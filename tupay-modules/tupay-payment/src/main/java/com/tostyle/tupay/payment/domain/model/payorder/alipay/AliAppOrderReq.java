package com.tostyle.tupay.payment.domain.model.payorder.alipay;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

/**
 * 支付方式： ALI_APP
 * @author tostyle
 * 2022/2/16 10:40
 */
@Data
public class AliAppOrderReq extends UnifiedOrderReq {

    /** 构造函数 **/
    public AliAppOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_APP); //默认 wayCode, 避免validate出现问题
    }
}
