package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import lombok.Data;

/**
 * 支付方式： ALI_APP
 * @author tostyle
 * 2022/2/16 10:40
 */
@Data
public class AliAppOrderResp extends UnifiedOrderResp {

    private String payData;

    @Override
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.ALI_APP;
    }

    @Override
    public String buildPayData(){
        return payData;
    }
}
