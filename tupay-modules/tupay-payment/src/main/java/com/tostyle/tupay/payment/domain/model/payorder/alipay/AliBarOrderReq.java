package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 支付方式： ALI_BAR
 * @author tostyle
 * 2022/2/16 10:42
 */
@Data
public class AliBarOrderReq extends UnifiedOrderReq {

    /** 用户 支付条码 **/
    @NotBlank(message = "支付条码不能为空")
    private String authCode;

    /** 构造函数 **/
    public AliBarOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_BAR); //默认 ali_bar, 避免validate出现问题
    }
}
