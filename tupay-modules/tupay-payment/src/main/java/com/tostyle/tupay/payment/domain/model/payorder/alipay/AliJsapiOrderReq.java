package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 支付方式： ALI_JSAPI
 * @author tostyle
 * 2022/2/16 10:45
 */
@Data
public class AliJsapiOrderReq extends UnifiedOrderReq {

    /** 支付宝用户ID **/
    @NotBlank(message = "用户ID不能为空")
    private String buyerUserId;

    /** 构造函数 **/
    public AliJsapiOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_JSAPI);
    }

    @Override
    public String getChannelUserId(){
        return this.buyerUserId;
    }
}
