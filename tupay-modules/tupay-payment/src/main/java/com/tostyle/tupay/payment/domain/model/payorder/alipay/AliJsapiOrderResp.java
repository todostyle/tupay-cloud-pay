package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 支付方式： ALI_JSAPI
 * @author tostyle
 * 2022/2/16 10:47
 */
@Data
public class AliJsapiOrderResp extends UnifiedOrderResp {
    /** 调起支付插件的支付宝订单号 **/
    private String alipayTradeNo;

    @Override
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.ALI_APP;
    }

    @Override
    public String buildPayData(){
        Map<String, String> param=new HashMap<>();
        param.put("alipayTradeNo",alipayTradeNo);
        return JSON.toJSONString(param);
    }
}
