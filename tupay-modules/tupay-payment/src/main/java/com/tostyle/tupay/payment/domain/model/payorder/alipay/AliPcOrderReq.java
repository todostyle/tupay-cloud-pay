package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * 支付方式： ALI_PC
 * @author tostyle
 * 2022/2/16 10:52
 */
@Data
public class AliPcOrderReq extends CommonPayDataReq {

    /** 构造函数 **/
    public AliPcOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_PC);
    }
}
