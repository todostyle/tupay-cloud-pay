package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * 支付方式： ALI_PC
 * @author tostyle
 * 2022/2/16 10:54
 */
@Data
public class AliPcOrderResp extends CommonPayDataResp {
}
