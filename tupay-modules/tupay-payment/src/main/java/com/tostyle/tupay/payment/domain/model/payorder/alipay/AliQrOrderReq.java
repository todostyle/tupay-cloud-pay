package com.tostyle.tupay.payment.domain.model.payorder.alipay;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * @author tostyle
 * 2022/2/16 10:54
 */
@Data
public class AliQrOrderReq extends CommonPayDataReq {

    /** 构造函数 **/
    public AliQrOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_QR);
    }

}
