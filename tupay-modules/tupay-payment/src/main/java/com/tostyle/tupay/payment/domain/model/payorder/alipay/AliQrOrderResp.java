package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * @author tostyle
 * 2022/2/16 10:55
 */
@Data
public class AliQrOrderResp extends CommonPayDataResp {
}
