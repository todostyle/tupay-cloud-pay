package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * 支付方式： ALI_WAP
 * @author tostyle
 * 2022/2/16 10:56
 */
@Data
public class AliWapOrderReq extends CommonPayDataReq {

    /** 构造函数 **/
    public AliWapOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.ALI_WAP); //默认 ALI_WAP, 避免validate出现问题
    }
}
