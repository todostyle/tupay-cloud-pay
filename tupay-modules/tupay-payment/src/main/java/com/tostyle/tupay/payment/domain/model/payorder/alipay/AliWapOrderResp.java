package com.tostyle.tupay.payment.domain.model.payorder.alipay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * 支付方式： ALI_WAP
 * @author tostyle
 * 2022/2/16 10:57
 */
@Data
public class AliWapOrderResp extends CommonPayDataResp {
}
