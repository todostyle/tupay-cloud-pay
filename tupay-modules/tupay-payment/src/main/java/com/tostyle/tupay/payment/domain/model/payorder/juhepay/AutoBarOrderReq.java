package com.tostyle.tupay.payment.domain.model.payorder.juhepay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

/**
 * 支付方式： AUTO_BAR
 * @author tostyle
 * 2022/2/16 11:01
 */
@Data
public class AutoBarOrderReq extends UnifiedOrderReq {

    /** 条码值 **/
    private String authCode;

    /** 构造函数 **/
    public AutoBarOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.AUTO_BAR);
    }

}
