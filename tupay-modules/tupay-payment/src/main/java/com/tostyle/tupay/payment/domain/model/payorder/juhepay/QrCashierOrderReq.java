package com.tostyle.tupay.payment.domain.model.payorder.juhepay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * 支付方式： QR_CASHIER
 * @author tostyle
 * 2022/2/16 11:03
 */
@Data
public class QrCashierOrderReq extends CommonPayDataReq {

    /** 构造函数 **/
    public QrCashierOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.QR_CASHIER);
    }
}
