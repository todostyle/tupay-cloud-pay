package com.tostyle.tupay.payment.domain.model.payorder.juhepay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * 支付方式： QR_CASHIER
 * @author tostyle
 * 2022/2/16 11:05
 */
@Data
public class QrCashierOrderResp extends CommonPayDataResp {
}
