package com.tostyle.tupay.payment.domain.model.payorder.wxpay;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import lombok.Data;

/**
 * @author tostyle
 * 2022/4/20 15:39
 */
@Data
public class WxAppOrderResp extends UnifiedOrderResp {
    /** 预支付数据包 **/
    private String payInfo;

    @Override
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.WX_APP;
    }

    @Override
    public String buildPayData(){
        return payInfo;
    }
}
