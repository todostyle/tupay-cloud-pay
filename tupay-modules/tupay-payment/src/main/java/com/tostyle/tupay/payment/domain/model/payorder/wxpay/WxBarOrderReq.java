package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 支付方式： WX_BAR
 * @author tostyle
 * 2022/2/16 11:21
 */
@Data
public class WxBarOrderReq extends UnifiedOrderReq {

    /** 用户 支付条码 **/
    @NotBlank(message = "支付条码不能为空")
    private String authCode;

    /** 构造函数 **/
    public WxBarOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.WX_BAR); //默认 wx_bar, 避免validate出现问题
    }

}
