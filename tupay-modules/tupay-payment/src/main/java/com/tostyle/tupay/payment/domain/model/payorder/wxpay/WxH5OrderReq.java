package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * 支付方式： WX_H5
 * @author tostyle
 * 2022/2/16 11:23
 */
@Data
public class WxH5OrderReq extends CommonPayDataReq {
    /** 构造函数 **/
    public WxH5OrderReq() {
        this.setWayCode(PayConstant.PAY_WAY_CODE.WX_H5);
    }
}
