package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * 支付方式： WX_H5
 * @author tostyle
 * 2022/2/16 11:24
 */
@Data
public class WxH5OrderResp extends CommonPayDataResp {
}
