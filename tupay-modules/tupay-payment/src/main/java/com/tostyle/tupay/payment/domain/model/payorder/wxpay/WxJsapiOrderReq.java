package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 支付方式： WX_JSAPI
 * @author tostyle
 * 2022/2/16 11:25
 */
@Data
public class WxJsapiOrderReq extends UnifiedOrderReq {

    /** 微信openid **/
    @NotBlank(message = "openid不能为空")
    private String openid;

    /** 构造函数 **/
    public WxJsapiOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.WX_JSAPI);
    }

    @Override
    public String getChannelUserId() {
        return this.openid;
    }
}
