package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataReq;
import lombok.Data;

/**
 * 支付方式： WX_NATIVE
 * @author tostyle
 * 2022/2/16 11:27
 */
@Data
public class WxNativeOrderReq extends CommonPayDataReq {

    /** 构造函数 **/
    public WxNativeOrderReq() {
        this.setWayCode(PayConstant.PAY_WAY_CODE.WX_NATIVE);
    }
}
