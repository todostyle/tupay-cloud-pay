package com.tostyle.tupay.payment.domain.model.payorder.wxpay;


import com.tostyle.tupay.payment.domain.model.payorder.CommonPayDataResp;
import lombok.Data;

/**
 * 支付方式： WX_NATIVE
 * @author tostyle
 * 2022/2/16 11:28
 */
@Data
public class WxNativeOrderResp extends CommonPayDataResp {
}
