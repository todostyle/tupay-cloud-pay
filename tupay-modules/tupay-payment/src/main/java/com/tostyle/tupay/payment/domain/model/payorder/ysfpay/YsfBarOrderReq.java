package com.tostyle.tupay.payment.domain.model.payorder.ysfpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 支付方式： YSF_BAR
 * @author tostyle
 * 2022/2/16 11:05
 */
@Data
public class YsfBarOrderReq extends UnifiedOrderReq {

    /** 用户 支付条码 **/
    @NotBlank(message = "支付条码不能为空")
    private String authCode;

    /** 构造函数 **/
    public YsfBarOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.YSF_BAR);
    }


}
