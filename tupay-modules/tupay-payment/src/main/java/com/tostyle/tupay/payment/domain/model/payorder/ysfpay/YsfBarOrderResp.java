package com.tostyle.tupay.payment.domain.model.payorder.ysfpay;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import lombok.Data;

/**
 * 支付方式： YSF_BAR
 * @author tostyle
 * 2022/2/16 11:07
 */
@Data
public class YsfBarOrderResp extends UnifiedOrderResp {

    @Override
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.NONE;
    }

    @Override
    public String buildPayData(){
        return "";
    }
}
