package com.tostyle.tupay.payment.domain.model.payorder.ysfpay;


import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderReq;
import lombok.Data;

/**
 * 支付方式： YSF_JSAPI
 * @author tostyle
 * 2022/2/16 11:08
 */
@Data
public class YsfJsapiOrderReq extends UnifiedOrderReq {

    /** 构造函数 **/
    public YsfJsapiOrderReq(){
        this.setWayCode(PayConstant.PAY_WAY_CODE.YSF_JSAPI);
    }
}
