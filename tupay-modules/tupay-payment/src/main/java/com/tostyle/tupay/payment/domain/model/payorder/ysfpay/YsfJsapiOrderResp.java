package com.tostyle.tupay.payment.domain.model.payorder.ysfpay;

import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.model.payorder.UnifiedOrderResp;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tostyle
 * 2022/2/16 11:09
 */
@Data
public class YsfJsapiOrderResp extends UnifiedOrderResp {

    /** 调起支付插件的云闪付订单号 **/
    private String redirectUrl;

    @Override
    public String buildPayDataType(){
        return PayConstant.PAY_DATA_TYPE.YSF_APP;
    }

    @Override
    public String buildPayData(){
        Map<String, String> param=new HashMap<>();
        param.put("redirectUrl",redirectUrl);
        return JSON.toJSONString(param);
    }
}
