package com.tostyle.tupay.payment.domain.model.refund;


import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import lombok.Data;

/**
 * 查询退款单请求参数对象
 * @author tostyle
 * 2022/3/3 9:13
 */
@Data
public class QueryRefundOrderReq extends AbstractMchAppReq {

    /** 商户退款单号 **/
    private String mchRefundNo;

    /** 支付系统退款订单号 **/
    private String refundOrderId;
}
