package com.tostyle.tupay.payment.domain.model.refund;

import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * @author tostyle
 * 2022/3/3 9:11
 */
@Data
public class RefundOrderResp extends AbstractResp {

    /** 支付系统退款订单号 **/
    private String refundOrderId;

    /** 商户发起的退款订单号 **/
    private String mchRefundNo;

    /** 订单支付金额 **/
    private Long payAmount;

    /** 申请退款金额 **/
    private Long refundAmount;

    /** 退款状态 **/
    private Long state;

    /** 渠道退款单号   **/
    private String channelOrderNo;

    /** 渠道返回错误代码 **/
    private String errCode;

    /** 渠道返回错误信息 **/
    private String errMsg;
    
    public static RefundOrderResp buildByRefundOrder(RefundOrder refundOrder){
        if(refundOrder == null){
            return null;
        }
        RefundOrderResp result = new RefundOrderResp();
        BeanUtils.copyProperties(refundOrder, result);
        return result;
    }

}
