package com.tostyle.tupay.payment.domain.model.transfer;


import com.tostyle.tupay.payment.domain.model.AbstractMchAppReq;
import lombok.Data;

/**
 * @author tostyle
 * 2022/3/3 14:12
 */
@Data
public class QueryTransferOrderReq extends AbstractMchAppReq {

    /** 商户转账单号 **/
    private String mchOrderNo;

    /** 支付系统转账单号 **/
    private String transferId;
}
