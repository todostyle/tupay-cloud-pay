package com.tostyle.tupay.payment.domain.model.transfer;


import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.AbstractResp;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * 创建订单(统一订单) 响应参数
 * @author tostyle
 * 2022/3/3 14:09
 */
@Data
public class TransferOrderResp extends AbstractResp {

    /** 转账单号 **/
    private String transferId;

    /** 商户单号 **/
    private String mchOrderNo;

    /** 转账金额 **/
    private Long amount;

    /**
     * 收款账号
     */
    private String accountNo;

    /**
     * 收款人姓名
     */
    private String accountName;

    /**
     * 收款人开户行名称
     */
    private String bankName;

    /** 状态 **/
    private Byte state;

    /** 渠道退款单号   **/
    private String channelOrderNo;

    /** 渠道返回错误代码 **/
    private String errCode;

    /** 渠道返回错误信息 **/
    private String errMsg;

    public static TransferOrderResp buildByRecord(TransferOrder record){
        if(record == null){
            return null;
        }
        TransferOrderResp result = new TransferOrderResp();
        BeanUtils.copyProperties(record, result);
        return result;
    }

}
