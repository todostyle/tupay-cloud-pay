package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.MchApp;
import org.apache.ibatis.annotations.Param;

/**
 * 应用列表Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface MchAppMapper 
{
    /**
     * 查询应用列表
     * 
     * @param appId 应用列表主键
     * @return 应用列表
     */
    public MchApp selectMchAppByAppId(String appId);

    /**
     * 查询应用列表列表
     * 
     * @param mchApp 应用列表
     * @return 应用列表集合
     */
    public List<MchApp> selectMchAppList(MchApp mchApp);

    /**
     * 新增应用列表
     * 
     * @param mchApp 应用列表
     * @return 结果
     */
    public int insertMchApp(MchApp mchApp);

    /**
     * 修改应用列表
     * 
     * @param mchApp 应用列表
     * @return 结果
     */
    public int updateMchApp(MchApp mchApp);

    /**
     * 删除应用列表
     * 
     * @param appId 应用列表主键
     * @return 结果
     */
    public int deleteMchAppByAppId(String appId);

    /**
     * 批量删除应用列表
     * 
     * @param appIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMchAppByAppIds(String[] appIds);

    /**
     * 根据商户号和商户应用查询商户信息
     * @param appId 应用Id
     * @param mchNo 商户号
     * @return MchApp
     */
    MchApp selectMchAppByAppIdAndMchNo(@Param("appId") String appId,@Param("mchNo") String mchNo);

    /**
     * 根据商户号查询商户应用
     * @param mchNoList 商户号集合
     * @return List<MchApp>
     */
    List<MchApp> selectMchAppByMchNoList(List<String> mchNoList);
}
