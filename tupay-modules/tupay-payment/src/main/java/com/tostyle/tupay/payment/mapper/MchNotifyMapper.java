package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.MchNotify;
import org.apache.ibatis.annotations.Param;

/**
 * 商户通知Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface MchNotifyMapper 
{
    /**
     * 查询商户通知
     * 
     * @param notifyId 商户通知主键
     * @return 商户通知
     */
    public MchNotify selectMchNotifyByNotifyId(Long notifyId);

    /**
     * 查询商户通知列表
     * 
     * @param mchNotify 商户通知
     * @return 商户通知集合
     */
    public List<MchNotify> selectMchNotifyList(MchNotify mchNotify);

    /**
     * 新增商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    public int insertMchNotify(MchNotify mchNotify);

    /**
     * 修改商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    public int updateMchNotify(MchNotify mchNotify);

    /**
     * 删除商户通知
     * 
     * @param notifyId 商户通知主键
     * @return 结果
     */
    public int deleteMchNotifyByNotifyId(Long notifyId);

    /**
     * 批量删除商户通知
     * 
     * @param notifyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMchNotifyByNotifyIds(Long[] notifyIds);

    /**
     * 更新通知状态
     * @param notifyId 通知主键
     * @param stateSuccess 通知状态
     * @param resResult 结果
     * @return
     */
    int updateNotifyResult(@Param("notifyId") Long notifyId,@Param("state") Long stateSuccess,@Param("resResult") String resResult);
    /**
     * 查询通知信息
     * @param orderId  订单号
     * @param orderType  订单类型
     * @return MchNotify
     */
    MchNotify selectMchNotifyByOrderAndType(@Param("orderId") String orderId,@Param("orderType") Long orderType);
}
