package com.tostyle.tupay.payment.mapper;

import com.tostyle.tupay.payment.domain.MchPayChannel;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 商户支付通道Mapper接口
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
public interface MchPayChannelMapper 
{
    /**
     * 查询商户支付通道
     * 
     * @param id 商户支付通道主键
     * @return 商户支付通道
     */
    public MchPayChannel selectMchPayChannelById(Long id);

    /**
     * 查询商户支付通道列表
     * 
     * @param mchPayChannel 商户支付通道
     * @return 商户支付通道集合
     */
    public List<MchPayChannel> selectMchPayChannelList(MchPayChannel mchPayChannel);

    /**
     * 新增商户支付通道
     * 
     * @param mchPayChannel 商户支付通道
     * @return 结果
     */
    public int insertMchPayChannel(MchPayChannel mchPayChannel);

    /**
     * 修改商户支付通道
     * 
     * @param mchPayChannel 商户支付通道
     * @return 结果
     */
    public int updateMchPayChannel(MchPayChannel mchPayChannel);

    /**
     * 删除商户支付通道
     * 
     * @param id 商户支付通道主键
     * @return 结果
     */
    public int deleteMchPayChannelById(Long id);

    /**
     * 批量删除商户支付通道
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMchPayChannelByIds(Long[] ids);

    /**
     * 根据商户号、应用Id和支付方式查询商户支付通道
     * @param mchNo 商户号
     * @param appId 应用Id
     * @param wayCode 支付方式
     * @return 商户支付通道
     */
    MchPayChannel getMchPayChannel(@Param("mchNo")String mchNo, @Param("appId")String appId, @Param("wayCode")String wayCode);
}