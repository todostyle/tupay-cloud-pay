package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.PayChannelDefine;

/**
 * 支付渠道定义Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface PayChannelDefineMapper 
{
    /**
     * 查询支付渠道定义
     * 
     * @param channelCode 支付渠道定义主键
     * @return 支付渠道定义
     */
    public PayChannelDefine selectPayChannelDefineByChannelCode(String channelCode);

    /**
     * 查询支付渠道定义列表
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 支付渠道定义集合
     */
    public List<PayChannelDefine> selectPayChannelDefineList(PayChannelDefine payChannelDefine);

    /**
     * 新增支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    public int insertPayChannelDefine(PayChannelDefine payChannelDefine);

    /**
     * 修改支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    public int updatePayChannelDefine(PayChannelDefine payChannelDefine);

    /**
     * 删除支付渠道定义
     * 
     * @param channelCode 支付渠道定义主键
     * @return 结果
     */
    public int deletePayChannelDefineByChannelCode(String channelCode);

    /**
     * 批量删除支付渠道定义
     * 
     * @param channelCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayChannelDefineByChannelCodes(String[] channelCodes);
}
