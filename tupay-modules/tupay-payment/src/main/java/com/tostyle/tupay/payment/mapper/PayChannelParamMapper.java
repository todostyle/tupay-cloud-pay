package com.tostyle.tupay.payment.mapper;

import com.tostyle.tupay.payment.domain.PayChannelParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 支付接口配置参数Mapper接口
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
public interface PayChannelParamMapper 
{
    /**
     * 查询支付接口配置参数
     * 
     * @param id 支付接口配置参数主键
     * @return 支付接口配置参数
     */
    public PayChannelParam selectPayChannelParamById(Long id);

    /**
     * 查询支付接口配置参数列表
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 支付接口配置参数集合
     */
    public List<PayChannelParam> selectPayChannelParamList(PayChannelParam payChannelParam);

    /**
     * 新增支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    public int insertPayChannelParam(PayChannelParam payChannelParam);

    /**
     * 修改支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    public int updatePayChannelParam(PayChannelParam payChannelParam);

    /**
     * 删除支付接口配置参数
     * 
     * @param id 支付接口配置参数主键
     * @return 结果
     */
    public int deletePayChannelParamById(Long id);

    /**
     * 批量删除支付接口配置参数
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayChannelParamByIds(Long[] ids);

    PayChannelParam selectPayChannelParam(@Param("infoId") String infoId,@Param("channelCode") String channelCode,@Param("infoType") Long infoType,@Param("state") Long state);
}