package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.PayOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 支付订单Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface PayOrderMapper 
{
    /**
     * 查询支付订单
     * 
     * @param tradeNo 支付订单主键
     * @return 支付订单
     */
    public PayOrder selectPayOrderByTradeNo(String tradeNo);

    /**
     * 查询支付订单列表
     * 
     * @param payOrder 支付订单
     * @return 支付订单集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 删除支付订单
     * 
     * @param tradeNo 支付订单主键
     * @return 结果
     */
    public int deletePayOrderByTradeNo(String tradeNo);

    /**
     * 批量删除支付订单
     * 
     * @param tradeNos 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayOrderByTradeNos(String[] tradeNos);

    /**
     * 根据支付订单号和商户号查询订单信息
     * @param tradeNo 支付订单号
     * @param mchNo  商户号
     * @return
     */
    PayOrder selectPayOrderByTradeNoAndMchNo(@Param("tradeNo") String tradeNo, @Param("mchNo") String mchNo);

    /**
     * 根据商户订单号和商户号
     * @param mchOrderNo 商户订单号
     * @param mchNo 商户号
     * @return
     */
    PayOrder selectPayOrderByMchOrderNoAndMchNo(@Param("mchOrderNo") String mchOrderNo,@Param("mchNo") String mchNo);

    /**
     * 根据支付订单号和订单状态查询订单信息
     * @param tradeNo 支付订单号
     * @param state 订单状态
     * @return
     */
    PayOrder selectPayOrderByTradeNoAndState(@Param("tradeNo") String tradeNo, @Param("state") Long state);

    /**
     * 更新订单退款金额和次数
     * @param tradeNo 支付订单号
     * @param refundAmount 退款金额
     * @return int
     */
    int updateRefundAmountAndCount(@Param("tradeNo")String tradeNo, @Param("refundAmount") Long refundAmount);
}
