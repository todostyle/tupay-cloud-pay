package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.PayWay;

/**
 * 支付方式Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface PayWayMapper 
{
    /**
     * 查询支付方式
     * 
     * @param id 支付方式主键
     * @return 支付方式
     */
    public PayWay selectPayWayById(Long id);

    /**
     * 查询支付方式列表
     * 
     * @param payWay 支付方式
     * @return 支付方式集合
     */
    public List<PayWay> selectPayWayList(PayWay payWay);

    /**
     * 新增支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    public int insertPayWay(PayWay payWay);

    /**
     * 修改支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    public int updatePayWay(PayWay payWay);

    /**
     * 删除支付方式
     * 
     * @param id 支付方式主键
     * @return 结果
     */
    public int deletePayWayById(Long id);

    /**
     * 批量删除支付方式
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayWayByIds(Long[] ids);

    /**
     * 根据支付方式编码查询支付方式
     * @param wayCode 支付方式
     * @return
     */
    PayWay checkWayCode(String wayCode);
}
