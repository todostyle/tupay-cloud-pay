package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.RefundOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 退款订单Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface RefundOrderMapper 
{
    /**
     * 查询退款订单
     * 
     * @param refundOrderId 退款订单主键
     * @return 退款订单
     */
    public RefundOrder selectRefundOrderByRefundOrderId(String refundOrderId);

    /**
     * 查询退款订单列表
     * 
     * @param refundOrder 退款订单
     * @return 退款订单集合
     */
    public List<RefundOrder> selectRefundOrderList(RefundOrder refundOrder);

    /**
     * 新增退款订单
     * 
     * @param refundOrder 退款订单
     * @return 结果
     */
    public int insertRefundOrder(RefundOrder refundOrder);

    /**
     * 修改退款订单
     * 
     * @param refundOrder 退款订单
     * @return 结果
     */
    public int updateRefundOrder(RefundOrder refundOrder);

    /**
     * 删除退款订单
     * 
     * @param refundOrderId 退款订单主键
     * @return 结果
     */
    public int deleteRefundOrderByRefundOrderId(String refundOrderId);

    /**
     * 批量删除退款订单
     * 
     * @param refundOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRefundOrderByRefundOrderIds(String[] refundOrderIds);

    /**
     * 查询退款信息
     * @param tradeNo 支付订单号
     * @param state 状态
     * @return RefundOrder
     */
    RefundOrder selectRefundOrderByTradeNoAndState(@Param("tradeNo") String tradeNo,@Param("state") Long state);

    /**
     * 通过商户号和商户退款订单号查询退款信息
     * @param mchNo 商户号
     * @param mchRefundNo 商户退款订单号
     * @return RefundOrder
     */
    RefundOrder selectRefundOrderByMchNoAndMchRefundNo(@Param("mchNo")String mchNo, @Param("mchRefundNo")String mchRefundNo);

    /**
     * 通过商户号和退款订单号查询退款信息
     * @param mchNo 商户号
     * @param refundOrderId 退款订单号
     * @return RefundOrder
     */
    RefundOrder selectRefundOrderByMchNoAndRefundOrderId(@Param("mchNo") String mchNo, @Param("refundOrderId") String refundOrderId);

    /**
     * 通过退款订单号和状态查询退款信息
     * @param refundOrderId 退款订单号
     * @param state 状态
     * @return RefundOrder
     */
    RefundOrder selectRefundOrderByRefundOrderIdAndState(@Param("refundOrderId") String refundOrderId,@Param("state") Long state);
    /**
     * 查询全部退成功金额
     * @param tradeNo 支付订单号
     * @return Long
     */
    Long sumSuccessRefundAmount(String tradeNo);
}
