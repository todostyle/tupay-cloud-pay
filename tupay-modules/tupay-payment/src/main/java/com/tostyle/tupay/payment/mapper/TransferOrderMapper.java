package com.tostyle.tupay.payment.mapper;

import java.util.List;
import com.tostyle.tupay.payment.domain.TransferOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 转账订单Mapper接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface TransferOrderMapper 
{
    /**
     * 查询转账订单
     * 
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    public TransferOrder selectTransferOrderByTransferId(String transferId);

    /**
     * 查询转账订单列表
     * 
     * @param transferOrder 转账订单
     * @return 转账订单集合
     */
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder);

    /**
     * 新增转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int insertTransferOrder(TransferOrder transferOrder);

    /**
     * 修改转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int updateTransferOrder(TransferOrder transferOrder);

    /**
     * 删除转账订单
     * 
     * @param transferId 转账订单主键
     * @return 结果
     */
    public int deleteTransferOrderByTransferId(String transferId);

    /**
     * 批量删除转账订单
     * 
     * @param transferIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTransferOrderByTransferIds(String[] transferIds);

    /**
     * 根据商户号和商户订单号查询转账订单
     * @param mchNo 商户号
     * @param mchOrderNo 商户订单号
     * @param transferId 转账订单号
     * @return 转账订单信息
     */
    TransferOrder selectTransferOrderByMchNoAndMchOrderNo(@Param("mchNo") String mchNo, @Param("mchOrderNo")String mchOrderNo,@Param("transferId")String transferId);

    /**
     * 根据订单号和和状态查询订单信息
     * @param transferId 转账订单
     * @param state  转账状态
     * @return TransferOrder
     */
    TransferOrder selectTransferOrderByTransferIdAndState(String transferId, Long state);

}