package com.tostyle.tupay.payment.mq;

/**
 * MQ 消息接收器 接口定义
 * @author tostyle
 * 2022/3/4 10:12
 */
public interface MqMsgReceiver {

    /** 接收消息 **/
    void receiveMsg(String msg);
}
