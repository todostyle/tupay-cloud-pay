package com.tostyle.tupay.payment.mq;


import com.tostyle.tupay.payment.mq.model.AbstractMq;

/**
 * MQ 消息发送器 接口定义
 * @author tostyle
 * 2022/3/4 10:11
 */
public interface MqSender {

    /** 推送MQ消息， 实时 **/
    void send(AbstractMq mqModel);

    /** 推送MQ消息， 延迟接收，单位：s **/
    void send(AbstractMq mqModel, int delay);
}
