package com.tostyle.tupay.payment.mq.constant;

/**
 * 定义Mq消息类型
 * @author tostyle
 * 2022/3/4 10:08
 */
public enum MqSendTypeEnum {

    /** QUEUE - 点对点 （只有1个消费者可消费。 ActiveMQ的queue模式 ） **/
    QUEUE,
    /** BROADCAST - 订阅模式 (所有接收者都可接收到。 ActiveMQ的topic模式, RabbitMQ的fanout类型的交换机, RocketMQ的广播模式  ) **/
    BROADCAST
}
