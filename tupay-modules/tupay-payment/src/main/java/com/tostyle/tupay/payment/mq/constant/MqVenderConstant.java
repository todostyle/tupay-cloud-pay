package com.tostyle.tupay.payment.mq.constant;

/**
 * MQ 厂商定义类
 * @author tostyle
 * 2022/3/4 10:17
 */
public class MqVenderConstant {

    public static final String YML_VENDER_KEY = "isys.mq.vender";

    public static final String ACTIVE_MQ = "activeMq";
    public static final String RABBIT_MQ = "rabbitMq";
    public static final String ROCKET_MQ = "rocketMq";
}
