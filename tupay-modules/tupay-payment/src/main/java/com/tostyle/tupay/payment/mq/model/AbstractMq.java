package com.tostyle.tupay.payment.mq.model;

import com.tostyle.tupay.payment.mq.constant.MqSendTypeEnum;
/**
 * 定义MQ消息格式
 * @author tostyle
 * 2022/3/4 10:07
 */
public abstract class AbstractMq {

    /** MQ名称 **/
    public abstract String getMqName();

    /** MQ 类型 **/
    public abstract MqSendTypeEnum getMqType();

    /** 构造MQ消息体 String类型 **/
    public abstract String toMsg();
}
