package com.tostyle.tupay.payment.mq.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 定义Msg消息载体
 * @author tostyle
 * 2022/3/7 14:31
 */
@Data
@AllArgsConstructor
public class MsgPayload {

    /** 通知单号 **/
    private Long notifyId;
}
