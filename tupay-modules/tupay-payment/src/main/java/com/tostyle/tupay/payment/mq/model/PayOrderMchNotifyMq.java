package com.tostyle.tupay.payment.mq.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.payment.mq.constant.MqSendTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 定义MQ消息格式
 * 业务场景： [ 支付订单的商户通知消息 ]
 * @author tostyle
 * 2022/3/4 10:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayOrderMchNotifyMq extends AbstractMq{

    /** 【！重要配置项！】 定义MQ名称 **/
    public static final String MQ_NAME = "QUEUE_PAY_ORDER_MCH_NOTIFY";

    /** 内置msg 消息体定义 **/
    private MsgPayload payload;


    @Override
    public String getMqName() {
        return MQ_NAME;
    }

    /**  【！重要配置项！】 **/
    @Override
    public MqSendTypeEnum getMqType(){
        return MqSendTypeEnum.QUEUE;  // QUEUE - 点对点 、 BROADCAST - 广播模式
    }

    @Override
    public String toMsg() {
        return JSONObject.toJSONString(payload);
    }

    /**  【！重要配置项！】 构造MQModel , 一般用于发送MQ时 **/
    public static PayOrderMchNotifyMq buildMq(Long notifyId){
        return new PayOrderMchNotifyMq(new MsgPayload(notifyId));
    }

    /** 解析MQ消息， 一般用于接收MQ消息时 **/
    public static MsgPayload parse(String msg){
        return JSON.parseObject(msg, MsgPayload.class);
    }

    /** 定义 IMQReceiver 接口： 项目实现该接口则可接收到对应的业务消息  **/
    public interface MqReceiver{
        /**
         * 接收消息体
         * @param payload 消息体
         */
        void receive(MsgPayload payload);
    }
}
