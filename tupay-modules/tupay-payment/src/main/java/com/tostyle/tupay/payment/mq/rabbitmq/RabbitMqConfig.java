package com.tostyle.tupay.payment.mq.rabbitmq;


import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ReflectUtil;
import com.tostyle.tupay.payment.mq.constant.MqSendTypeEnum;
import com.tostyle.tupay.payment.mq.constant.MqVenderConstant;
import com.tostyle.tupay.payment.mq.model.AbstractMq;
import com.tostyle.tupay.payment.util.SpringBeansUtil;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Set;

/**
 * RabbitMQ的配置项
 * 1. 注册全部定义好的Queue Bean
 * 2. 动态注册fanout交换机
 * 3. 将Queue模式绑定到延时消息的交换机
 * @author tostyle
 * 2022/3/4 10:15
 */
@Component
@ConditionalOnProperty(name = MqVenderConstant.YML_VENDER_KEY, havingValue = MqVenderConstant.RABBIT_MQ)
public class RabbitMqConfig {

    /** 全局定义延迟交换机名称 **/
    public static final String DELAYED_EXCHANGE_NAME = "delayedExchange";

    /** 扇形交换机前缀（activeMQ中的topic模式）， 需根据queue动态拼接 **/
    public static final String FANOUT_EXCHANGE_NAME_PREFIX = "fanout_exchange_";

    /** 注入延迟交换机Bean **/
    @Resource
    @Qualifier(DELAYED_EXCHANGE_NAME)
    private CustomExchange delayedExchange;

    /** 注入rabbitMQBeanProcessor **/
    @Resource
    private RabbitMqBeanProcessor rabbitMqBeanProcessor;


    /** 在全部bean注册完成后再执行 **/
    @PostConstruct
    public void init(){
        // 获取到所有的MQ定义
        Set<Class<?>> set = ClassUtil.scanPackageBySuper(ClassUtil.getPackage(AbstractMq.class), AbstractMq.class);
        for (Class<?> aClass : set) {
            // 实例化
            AbstractMq amq = (AbstractMq) ReflectUtil.newInstance(aClass);
            // 注册Queue === new Queue(name)，  queue名称/bean名称 = mqName
            rabbitMqBeanProcessor.beanDefinitionRegistry.registerBeanDefinition(amq.getMqName(),
                BeanDefinitionBuilder.rootBeanDefinition(Queue.class).addConstructorArgValue(amq.getMqName()).getBeanDefinition());
            // 广播模式
            if(amq.getMqType() == MqSendTypeEnum.BROADCAST){
                // 动态注册交换机， 交换机名称/bean名称 =  FANOUT_EXCHANGE_NAME_PREFIX + amq.getMQName()
                rabbitMqBeanProcessor.beanDefinitionRegistry.registerBeanDefinition(FANOUT_EXCHANGE_NAME_PREFIX +amq.getMqName(),
                    BeanDefinitionBuilder.genericBeanDefinition(FanoutExchange.class, () ->{
                            // 普通FanoutExchange 交换机
                            return new FanoutExchange(FANOUT_EXCHANGE_NAME_PREFIX +amq.getMqName(),true,false);
                        }
                    ).getBeanDefinition()
                );
            }else{
                // 延迟交换机与Queue进行绑定， 绑定Bean名称 = mqName_DelayedBind
                rabbitMqBeanProcessor.beanDefinitionRegistry.registerBeanDefinition(amq.getMqName() + "_DelayedBind",
                    BeanDefinitionBuilder.genericBeanDefinition(Binding.class, () ->
                        BindingBuilder.bind(SpringBeansUtil.getBean(amq.getMqName(), Queue.class)).to(delayedExchange).with(amq.getMqName()).noargs()
                    ).getBeanDefinition()
                );
            }
        }
    }
}
