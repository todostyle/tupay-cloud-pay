package com.tostyle.tupay.payment.mq.rabbitmq;


import com.tostyle.tupay.payment.mq.MqSender;
import com.tostyle.tupay.payment.mq.constant.MqSendTypeEnum;
import com.tostyle.tupay.payment.mq.constant.MqVenderConstant;
import com.tostyle.tupay.payment.mq.model.AbstractMq;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * RabbitMQ的配置项
 * 1. 注册全部定义好的Queue Bean
 * 2. 动态注册fanout交换机
 * 3. 将Queue模式绑定到延时消息的交换机
 * @author tostyle
 * 2022/3/4 10:27
 */
@Component
@ConditionalOnProperty(name = MqVenderConstant.YML_VENDER_KEY, havingValue = MqVenderConstant.RABBIT_MQ)
public class RabbitMqSender implements MqSender {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public void send(AbstractMq mqModel) {
        if(mqModel.getMqType() == MqSendTypeEnum.QUEUE){
            rabbitTemplate.convertAndSend(mqModel.getMqName(), mqModel.toMsg());
        }else{
            // fanout模式 的 routeKEY 没意义。
            this.rabbitTemplate.convertAndSend(RabbitMqConfig.FANOUT_EXCHANGE_NAME_PREFIX + mqModel.getMqName(), null, mqModel.toMsg());
        }
    }

    @Override
    public void send(AbstractMq mqModel, int delay) {
        if(mqModel.getMqType() == MqSendTypeEnum.QUEUE){
            rabbitTemplate.convertAndSend(RabbitMqConfig.DELAYED_EXCHANGE_NAME, mqModel.getMqName(), mqModel.toMsg(), messagePostProcessor ->{
                messagePostProcessor.getMessageProperties().setDelay(Math.toIntExact(delay * 1000));
                return messagePostProcessor;
            });
        }else{
            // fanout模式 的 routeKEY 没意义。  没有延迟属性
            this.rabbitTemplate.convertAndSend(RabbitMqConfig.FANOUT_EXCHANGE_NAME_PREFIX + mqModel.getMqName(), null, mqModel.toMsg());
        }
    }
}
