package com.tostyle.tupay.payment.mq.rabbitmq.receive;


import com.tostyle.tupay.payment.mq.MqMsgReceiver;
import com.tostyle.tupay.payment.mq.executor.MqThreadExecutor;
import com.tostyle.tupay.payment.mq.model.PayOrderMchNotifyMq;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tostyle
 * 2022/3/4 10:58
 */
@Component
public class PayOrderMchNotifyRabbitMqReceiver implements MqMsgReceiver {


    @Resource
    private PayOrderMchNotifyMq.MqReceiver mqReceiver;

    /** 接收 【 queue 】 类型的消息 **/
    @Override
    @Async(MqThreadExecutor.EXECUTOR_PAYORDER_MCH_NOTIFY)
    @RabbitListener(queues = PayOrderMchNotifyMq.MQ_NAME)
    public void receiveMsg(String msg){
        mqReceiver.receive(PayOrderMchNotifyMq.parse(msg));
    }
}
