package com.tostyle.tupay.payment.mq.receiver;


import cn.hutool.core.net.url.UrlBuilder;
import cn.hutool.http.HttpUtil;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.MchNotify;
import com.tostyle.tupay.payment.mq.MqSender;
import com.tostyle.tupay.payment.mq.model.MsgPayload;
import com.tostyle.tupay.payment.mq.model.PayOrderMchNotifyMq;
import com.tostyle.tupay.payment.service.IMchNotifyService;
import com.tostyle.tupay.payment.service.IPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tostyle
 * 2022/3/7 14:43
 */
@Slf4j
@Component
public class PayOrderMchNotifyMqReceiver implements PayOrderMchNotifyMq.MqReceiver {


    @Resource
    private IMchNotifyService mchNotifyService;

    @Resource
    private IPayOrderService payOrderService;

    @Resource
    private MqSender mqSender;



    @Override
    public void receive(MsgPayload payload) {
        try {
            log.info("接收商户通知Mq, msg={}", payload.toString());
            Long notifyId = payload.getNotifyId();
            MchNotify mchNotify = mchNotifyService.selectMchNotifyByNotifyId(notifyId);
            if(mchNotify == null || mchNotify.getState() != PayConstant.STATE_ING){
                log.info("查询通知记录不存在或状态不是通知中");
                return;
            }
            if( mchNotify.getNotifyCount() >= mchNotify.getNotifyCountLimit() ){
                log.info("已达到最大发送次数");
                return;
            }
            //1. (发送结果最多6次)
            Integer currentCount = mchNotify.getNotifyCount().intValue() + 1;

            String notifyUrl = mchNotify.getNotifyUrl();
            String res = "";
            try {
                res = HttpUtil.createPost(notifyUrl).timeout(20000).execute().body();
            } catch (Exception e) {
                log.error("http error", e);
                res = "连接["+ UrlBuilder.of(notifyUrl).getHost() +"]异常:【" + e.getMessage() + "】";
            }
            //支付订单 & 第一次通知: 更新为已通知
            if(currentCount == 1 && PayConstant.TYPE_PAY_ORDER == mchNotify.getOrderType()){
                payOrderService.updateNotifySent(mchNotify.getOrderId());
            }

            //通知成功
            if("SUCCESS".equalsIgnoreCase(res)){
                mchNotifyService.updateNotifyResult(notifyId, PayConstant.STATE_SUCCESS, res);
                return;
            }

            //通知次数 >= 最大通知次数时， 更新响应结果为异常， 不在继续延迟发送消息
            if( currentCount >= mchNotify.getNotifyCountLimit() ){
                mchNotifyService.updateNotifyResult(notifyId, PayConstant.STATE_FAIL, res);
                return;
            }

            // 继续发送MQ 延迟发送
            mchNotifyService.updateNotifyResult(notifyId, PayConstant.STATE_ING, res);
            // 通知延时次数
            //        1   2  3  4   5   6
            //        0  30 60 90 120 150

            mqSender.send(PayOrderMchNotifyMq.buildMq(notifyId), currentCount * 30);

        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
    }
}
