package com.tostyle.tupay.payment.service;

import com.tostyle.tupay.common.core.model.params.IsvParams;
import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import com.tostyle.tupay.common.core.model.params.NormalMchParams;
import com.tostyle.tupay.payment.beans.AlipayClientWrapper;
import com.tostyle.tupay.payment.beans.MchAppContext;
import com.tostyle.tupay.payment.beans.WxServiceWrapper;
import com.tostyle.tupay.payment.domain.MchApp;

/**
 * @author tostyle
 * 2022/4/18 10:20
 */
public interface ContextService {


    /**
     * 根据商户号和应用Id查询商户信息
     * @param mchNo 商户号
     * @param appId 应用Id
     * @return 商户应用信息
     */
    public MchApp queryMchApp(String mchNo, String appId);

    /**
     * 根据商户号和应用Id查询商户缓存信息
     * @param mchNo 商户号
     * @param appId 应用Id
     * @return MchAppContext
     */
    public MchAppContext queryMchInfoAndAppInfo(String mchNo, String appId);


    /**
     *  根据商户号、应用Id和渠道编号查询普通商户信息
     * @param mchNo 商户号
     * @param appId 应用Id
     * @param channelCode 渠道编号
     * @return 普通商户信息
     */
    public NormalMchParams queryNormalMchParams(String mchNo, String appId, String channelCode);

    /**
     * 根据商户号、应用Id和渠道编号查询特约商户信息
     * @param mchNo 商户号
     * @param appId 应用Id
     * @param channelCode 渠道编号
     * @return 特约商户信息
     */
    public IsvSubMchParams queryIsvSubMchParams(String mchNo, String appId, String channelCode);

    /**
     *  根据服务商号和渠道编号查询信息
     * @param isvNo 服务商号
     * @param channelCode 渠道编号
     * @return IsvParams
     */
    public IsvParams queryIsvParams(String isvNo, String channelCode);

    /**
     * 缓存支付宝客户端
     * @param mchAppContext 商户应用数据
     * @return AlipayClientWrapper
     */
    public AlipayClientWrapper getAlipayClientWrapper(MchAppContext mchAppContext);

    /**
     * 缓存微信客户端
     * @param mchAppContext
     * @return
     */
    public WxServiceWrapper getWxServiceWrapper(MchAppContext mchAppContext);




}
