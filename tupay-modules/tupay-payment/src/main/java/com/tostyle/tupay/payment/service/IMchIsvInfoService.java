package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.MchIsvInfo;

/**
 * 服务商列表Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface IMchIsvInfoService 
{
    /**
     * 查询服务商列表
     * 
     * @param isvNo 服务商列表主键
     * @return 服务商列表
     */
    public MchIsvInfo selectMchIsvInfoByIsvNo(String isvNo);

    /**
     * 查询服务商列表列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 服务商列表集合
     */
    public List<MchIsvInfo> selectMchIsvInfoList(MchIsvInfo mchIsvInfo);

    /**
     * 新增服务商列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 结果
     */
    public int insertMchIsvInfo(MchIsvInfo mchIsvInfo);

    /**
     * 修改服务商列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 结果
     */
    public int updateMchIsvInfo(MchIsvInfo mchIsvInfo);

    /**
     * 批量删除服务商列表
     * 
     * @param isvNos 需要删除的服务商列表主键集合
     * @return 结果
     */
    public int deleteMchIsvInfoByIsvNos(String[] isvNos);

    /**
     * 删除服务商列表信息
     * 
     * @param isvNo 服务商列表主键
     * @return 结果
     */
    public int deleteMchIsvInfoByIsvNo(String isvNo);
}
