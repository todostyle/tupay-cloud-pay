package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.MchNotify;

/**
 * 商户通知Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface IMchNotifyService 
{
    /**
     * 查询商户通知
     * 
     * @param notifyId 商户通知主键
     * @return 商户通知
     */
    public MchNotify selectMchNotifyByNotifyId(Long notifyId);

    /**
     * 查询商户通知列表
     * 
     * @param mchNotify 商户通知
     * @return 商户通知集合
     */
    public List<MchNotify> selectMchNotifyList(MchNotify mchNotify);

    /**
     * 新增商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    public int insertMchNotify(MchNotify mchNotify);

    /**
     * 修改商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    public int updateMchNotify(MchNotify mchNotify);

    /**
     * 批量删除商户通知
     * 
     * @param notifyIds 需要删除的商户通知主键集合
     * @return 结果
     */
    public int deleteMchNotifyByNotifyIds(Long[] notifyIds);

    /**
     * 删除商户通知信息
     * 
     * @param notifyId 商户通知主键
     * @return 结果
     */
    public int deleteMchNotifyByNotifyId(Long notifyId);

    /**
     * 更新通知状态
     * @param notifyId 通知Id
     * @param stateSuccess  状态
     * @param res 结果
     * @return 结果
     */
    int updateNotifyResult(Long notifyId, Long stateSuccess, String res);

    /**
     * 查询支付通知信息
     * @param orderId 订单号
     * @return MchNotify
     */
    MchNotify selectMchNotifyByPayOrder(String orderId);

    /**
     * 查询退款通知信息
     * @param orderId 订单号
     * @return MchNotify
     */
    MchNotify selectMchNotifyByRefundOrder(String orderId);

    /**
     *  查询转账通知订单
     * @param orderId 订单号
     * @return MchNotify
     */
    MchNotify selectMchNotifyByTransferOrder(String orderId);
}
