package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.PayChannelDefine;

/**
 * 支付渠道定义Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface IPayChannelDefineService 
{
    /**
     * 查询支付渠道定义
     * 
     * @param channelCode 支付渠道定义主键
     * @return 支付渠道定义
     */
    public PayChannelDefine selectPayChannelDefineByChannelCode(String channelCode);

    /**
     * 查询支付渠道定义列表
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 支付渠道定义集合
     */
    public List<PayChannelDefine> selectPayChannelDefineList(PayChannelDefine payChannelDefine);

    /**
     * 新增支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    public int insertPayChannelDefine(PayChannelDefine payChannelDefine);

    /**
     * 修改支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    public int updatePayChannelDefine(PayChannelDefine payChannelDefine);

    /**
     * 批量删除支付渠道定义
     * 
     * @param channelCodes 需要删除的支付渠道定义主键集合
     * @return 结果
     */
    public int deletePayChannelDefineByChannelCodes(String[] channelCodes);

    /**
     * 删除支付渠道定义信息
     * 
     * @param channelCode 支付渠道定义主键
     * @return 结果
     */
    public int deletePayChannelDefineByChannelCode(String channelCode);
}
