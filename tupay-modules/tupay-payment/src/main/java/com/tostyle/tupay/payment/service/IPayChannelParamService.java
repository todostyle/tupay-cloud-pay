package com.tostyle.tupay.payment.service;

import com.tostyle.tupay.payment.domain.PayChannelParam;

import java.util.List;

/**
 * 支付接口配置参数Service接口
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
public interface IPayChannelParamService 
{
    /**
     * 查询支付接口配置参数
     * 
     * @param id 支付接口配置参数主键
     * @return 支付接口配置参数
     */
    public PayChannelParam selectPayChannelParamById(Long id);

    /**
     * 查询支付接口配置参数列表
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 支付接口配置参数集合
     */
    public List<PayChannelParam> selectPayChannelParamList(PayChannelParam payChannelParam);

    /**
     * 新增支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    public int insertPayChannelParam(PayChannelParam payChannelParam);

    /**
     * 修改支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    public int updatePayChannelParam(PayChannelParam payChannelParam);

    /**
     * 批量删除支付接口配置参数
     * 
     * @param ids 需要删除的支付接口配置参数主键集合
     * @return 结果
     */
    public int deletePayChannelParamByIds(Long[] ids);

    /**
     * 删除支付接口配置参数信息
     * 
     * @param id 支付接口配置参数主键
     * @return 结果
     */
    public int deletePayChannelParamById(Long id);

    /**
     *  查询支付渠道参数
     * @param infoId  id
     * @param infoType 类型
     * @param state 状态
     * @return 结果
     */
    List<PayChannelParam> selectPayChannelParam(String infoId,Long infoType, Long state);

    /**
     *  查询支付渠道参数
     * @param infoId  id
     * @param channelCode 渠道编号
     * @param infoType 类型
     * @param state 状态
     * @return 结果
     */
    PayChannelParam selectPayChannelParam(String infoId,String channelCode, Long infoType, Long state);

    /**
     * 查询商户App使用已正确配置了通道信息
     * @param appId 应用Id
     * @param channelCode 渠道编号
     * @return 结果
     */
    boolean mchAppHasAvailableChannelCode(String appId, String channelCode);

}