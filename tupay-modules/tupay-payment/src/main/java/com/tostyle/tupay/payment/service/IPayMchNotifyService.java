package com.tostyle.tupay.payment.service;

import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.TransferOrder;

/**
 * 商户通知 service
 * @author tostyle
 * 2022/4/20 11:22
 */
public interface IPayMchNotifyService {

    /**
     * 商户通知信息， 只有订单是终态，才会发送通知， 如明确成功和明确失败
     * @param dbPayOrder 订单信息
     */
    public void payOrderNotify(PayOrder dbPayOrder);

    /**
     * 商户通知信息，退款成功的发送通知
     * @param dbRefundOrder 退款订单
     */
    public void refundOrderNotify(RefundOrder dbRefundOrder);


    /**
     * 商户通知信息，转账订单的通知接口
     * @param dbTransferOrder 转账订单
     */
    public void transferOrderNotify(TransferOrder dbTransferOrder);

    /**
     * 创建支付订单响应URL
     * @param payOrder 订单信息
     * @param appSecret 密钥
     * @return String
     */
    public String createNotifyUrl(PayOrder payOrder, String appSecret);

    /**
     * 创建退款订单响应URL
     * @param refundOrder 退款
     * @param appSecret 密钥
     * @return String
     */
    public String createNotifyUrl(RefundOrder refundOrder, String appSecret);


    /**
     * 创建转账响应URL
     * @param transferOrder 转账信息
     * @param appSecret 密钥
     * @return String
     */
    public String createNotifyUrl(TransferOrder transferOrder, String appSecret);


    /**
     * 创建响应URL
     * @param payOrder 订单信息
     * @param appSecret 密钥
     * @return String
     */
    public String createReturnUrl(PayOrder payOrder, String appSecret);
}
