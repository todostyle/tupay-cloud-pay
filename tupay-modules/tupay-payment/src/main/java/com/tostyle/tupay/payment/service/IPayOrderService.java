package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.PayOrder;

/**
 * 支付订单Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface IPayOrderService 
{
    /**
     * 查询支付订单
     * 
     * @param tradeNo 支付订单主键
     * @return 支付订单
     */
    public PayOrder selectPayOrderByTradeNo(String tradeNo);

    /**
     * 查询支付订单列表
     * 
     * @param payOrder 支付订单
     * @return 支付订单集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 批量删除支付订单
     * 
     * @param tradeNos 需要删除的支付订单主键集合
     * @return 结果
     */
    public int deletePayOrderByTradeNos(String[] tradeNos);

    /**
     * 删除支付订单信息
     * 
     * @param tradeNo 支付订单主键
     * @return 结果
     */
    public int deletePayOrderByTradeNo(String tradeNo);

    /**
     * 更新通知状态
     * @param tradeNo 支付订单主键
     */
    void updateNotifySent(String tradeNo);
    /**
     * 更新订单状态  【订单生成】 --》 【支付中】
     * @param tradeNo  平台订单号
     * @param payOrder  订单参数
     * @return
     */
    boolean updateInit2Ing(String tradeNo, PayOrder payOrder);

    /**
     * 更新订单状态  【支付中】 --》 【支付成功】
     * @param tradeNo 平台订单号
     * @param channelOrderNo 渠道订单号
     * @param channelUserId 渠道用户标识
     * @return
     */
    boolean updateIng2Success(String tradeNo, String channelOrderNo, String channelUserId);
    /**
     * 更新订单状态  【支付中】 --》 【支付失败】
     * @param tradeNo  平台订单号
     * @param channelOrderNo 渠道订单号
     * @param channelUserId 渠道用户标识
     * @param channelErrCode 渠道错误码
     * @param channelErrMsg 渠道错误信息
     * @return
     */
    boolean updateIng2Fail(String tradeNo, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg);

    /**
     *
     * @param tradeNo 平台订单号
     * @param updateState 支付状态
     * @param channelOrderNo 渠道订单号
     * @param channelUserId 渠道用户标识
     * @param channelErrCode 渠道错误码
     * @param channelErrMsg 渠道错误信息
     * @return
     */
    boolean updateIng2SuccessOrFail(String tradeNo, Long updateState, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg);

    /**
     * 订单关闭
     * @param tradeNo 平台订单号
     * @return
     */
    boolean updateIng2Close(String tradeNo);

    /**
     * 查询订单
     * @param mchNo  商户号
     * @param tradeNo 平台订单号
     * @param mchOrderNo 商户订单号
     * @return
     */
    PayOrder queryMchOrder(String mchNo, String tradeNo, String mchOrderNo);


    /**
     * 根据商户号和商户订单号校验订单信息
     * @param mchNo 商户号
     * @param mchOrderNo  商户订单号
     * @return
     */
    boolean checkMchOrder(String mchNo, String mchOrderNo);
}
