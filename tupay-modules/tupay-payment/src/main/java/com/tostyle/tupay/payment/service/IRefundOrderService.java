package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.RefundOrder;

/**
 * 退款订单Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface IRefundOrderService 
{
    /**
     * 查询退款订单
     * 
     * @param refundOrderId 退款订单主键
     * @return 退款订单
     */
    public RefundOrder selectRefundOrderByRefundOrderId(String refundOrderId);

    /**
     * 查询退款订单列表
     * 
     * @param refundOrder 退款订单
     * @return 退款订单集合
     */
    public List<RefundOrder> selectRefundOrderList(RefundOrder refundOrder);

    /**
     * 新增退款订单
     * 
     * @param refundOrder 退款订单
     * @return 结果
     */
    public int insertRefundOrder(RefundOrder refundOrder);

    /**
     * 修改退款订单
     * 
     * @param refundOrder 退款订单
     * @return 结果
     */
    public int updateRefundOrder(RefundOrder refundOrder);

    /**
     * 批量删除退款订单
     * 
     * @param refundOrderIds 需要删除的退款订单主键集合
     * @return 结果
     */
    public int deleteRefundOrderByRefundOrderIds(String[] refundOrderIds);

    /**
     * 删除退款订单信息
     * 
     * @param refundOrderId 退款订单主键
     * @return 结果
     */
    public int deleteRefundOrderByRefundOrderId(String refundOrderId);

    /**
     * 根据商户号、商户退款订单和退款订单号查询订单信息
     * @param mchNo 商户号
     * @param mchRefundNo 商户退款订单号
     * @param refundOrderId 退款订单号
     * @return RefundOrder
     */
    RefundOrder queryMchOrder(String mchNo, String mchRefundNo, String refundOrderId);

    /**
     * 更新退款单状态  【退款单生成】 --》 【退款中】
     * @param refundOrderId 退款订单号
     * @param channelOrderNo 渠道订单号
     * @return boolean
     */
    boolean updateInit2Ing(String refundOrderId, String channelOrderNo);


    /**
     * 更新退款单状态  【退款中】 --》 【退款成功】
     * @param refundOrderId  退款订单号
     * @param channelOrderNo 渠道订单号
     * @return boolean
     */
    boolean updateIng2Success(String refundOrderId, String channelOrderNo);

    /**
     * 更新退款单状态  【退款中】 --》 【退款失败】
     * @param refundOrderId 退款订单号
     * @param channelOrderNo 渠道订单号
     * @param channelErrCode 渠道错误编码
     * @param channelErrMsg 渠道错误信息
     * @return boolean
     */
    boolean updateIng2Fail(String refundOrderId, String channelOrderNo, String channelErrCode, String channelErrMsg);


    /**
     * 更新退款单状态  【退款中】 --》 【退款成功/退款失败】
     * @param refundOrderId 退款订单号
     * @param updateState 状态
     * @param channelOrderNo 渠道订单号
     * @param channelErrCode 渠道错误编码
     * @param channelErrMsg 渠道错误信息
     * @return boolean
     */
    boolean updateIng2SuccessOrFail(String refundOrderId, Long updateState, String channelOrderNo, String channelErrCode, String channelErrMsg);


    /**
     * 更新退款单为 关闭状态
     * @return Integer
     */
    Integer updateOrderExpired();
    /**
     * 校验支付订单号
     * @param tradeNo  支付订单号
     * @param state 订单状态
     * @return boolean
     */
    boolean checkTradeNoAndState(String tradeNo, Long state);

    /**
     * 校验商户退款订单号
     * @param mchNo 商户号
     * @param mchRefundNo 商户退款订单号
     * @return boolean
     */
    boolean checkMchRefundNoAndMchNo(String mchNo, String mchRefundNo);
    /**
     * 查询全部退成功金额
     * @param tradeNo 支付订单号
     * @return Long
     */
    Long sumSuccessRefundAmount(String tradeNo);


}
