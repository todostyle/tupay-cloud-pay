package com.tostyle.tupay.payment.service;

import java.util.List;
import com.tostyle.tupay.payment.domain.TransferOrder;

/**
 * 转账订单Service接口
 * 
 * @author tostyle
 * @date 2022-04-15
 */
public interface ITransferOrderService 
{
    /**
     * 查询转账订单
     * 
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    public TransferOrder selectTransferOrderByTransferId(String transferId);

    /**
     * 查询转账订单列表
     * 
     * @param transferOrder 转账订单
     * @return 转账订单集合
     */
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder);

    /**
     * 新增转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int insertTransferOrder(TransferOrder transferOrder);

    /**
     * 修改转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int updateTransferOrder(TransferOrder transferOrder);

    /**
     * 批量删除转账订单
     * 
     * @param transferIds 需要删除的转账订单主键集合
     * @return 结果
     */
    public int deleteTransferOrderByTransferIds(String[] transferIds);

    /**
     * 删除转账订单信息
     * 
     * @param transferId 转账订单主键
     * @return 结果
     */
    public int deleteTransferOrderByTransferId(String transferId);

    /**
     * 查询转账订单
     * @param mchNo 商户号
     * @param mchOrderNo 商户订单号
     * @param transferId 转账订单号
     * @return TransferOrder
     */
    TransferOrder queryMchOrder(String mchNo, String mchOrderNo, String transferId);


    /**
     * 校验商户订单号是否重复
     * @param mchNo 商户号
     * @param mchOrderNo 商户订单号
     * @return boolean
     */
    boolean checkMchOrderNo(String mchNo, String mchOrderNo);

    /**
     * 更新转账状态
     * @param transferId 转账订单号
     * @return  boolean
     */
    boolean updateInit2Ing(String transferId);

    /**
     * 更新转账订单状态  【转账中】 --》 【转账成功】
     * @param transferId 转账订单号
     * @param channelOrderNo 渠道订单号
     * @return boolean
     */
    boolean updateIng2Success(String transferId, String channelOrderNo);


    /**
     * 更新转账订单状态  【转账中】 --》 【转账失败】
     * @param transferId 转账订单号
     * @param channelOrderNo 渠道订单号
     * @param channelErrCode 渠道错误码
     * @param channelErrMsg 渠道错误信息
     * @return boolean
     */
    boolean updateIng2Fail(String transferId, String channelOrderNo, String channelErrCode, String channelErrMsg);
    /**
     * 更新转账状态
     * @param transferId 转账订单号
     * @param state 状态
     * @param channelOrderId 渠道订单号
     * @param channelErrCode 渠道错误码
     * @param channelErrMsg 渠道错误信息
     * @return boolean
     */
    boolean updateIng2SuccessOrFail(String transferId, Long state, String channelOrderId, String channelErrCode, String channelErrMsg);
}