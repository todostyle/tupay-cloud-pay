package com.tostyle.tupay.payment.service;

import com.tostyle.tupay.payment.domain.PayOrder;

/**
 * 订单处理通用逻辑
 * @author tostyle
 * 2022/4/18 14:27
 */
public interface PayOrderProcessService {

    /**
     * 明确成功的处理逻辑（除更新订单其他业务）
     * @param payOrder
     */
    public void confirmSuccess(PayOrder payOrder);
}
