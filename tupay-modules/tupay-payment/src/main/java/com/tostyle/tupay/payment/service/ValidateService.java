package com.tostyle.tupay.payment.service;

/**
 * @author tostyle
 * 2022/4/18 9:18
 */
public interface ValidateService {

    /**
     * 参数校验
     * @param obj 校验数据
     */
    public void validate(Object obj);
}
