package com.tostyle.tupay.payment.service.impl;

import com.alibaba.fastjson.JSON;
import com.tostyle.tupay.common.core.constant.CacheConstants;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.model.params.IsvParams;
import com.tostyle.tupay.common.core.model.params.IsvSubMchParams;
import com.tostyle.tupay.common.core.model.params.NormalMchParams;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayIsvParams;
import com.tostyle.tupay.common.core.model.params.alipay.AlipayNormalMchParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayIsvParams;
import com.tostyle.tupay.common.core.model.params.wxpay.WxpayNormalMchParams;
import com.tostyle.tupay.common.core.text.Convert;
import com.tostyle.tupay.common.redis.service.RedisService;
import com.tostyle.tupay.payment.beans.*;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.domain.MchInfo;
import com.tostyle.tupay.payment.domain.MchIsvInfo;
import com.tostyle.tupay.payment.domain.PayChannelParam;
import com.tostyle.tupay.payment.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tostyle
 * 2022/4/18 10:20
 */
@Service
public class ContextServiceImpl implements ContextService {


    /** <商户ID, 商户配置项>  **/
    private static final Map<String, MchInfoContext> mchInfoContextMap = new ConcurrentHashMap<>();

    /** <应用ID, 商户配置上下文>  **/
    private static final Map<String, MchAppContext> mchAppContextMap = new ConcurrentHashMap<>();

    /** <服务商号, 服务商配置上下文>  **/
    private static final Map<String, IsvConfigContext> isvConfigContextMap = new ConcurrentHashMap<>();

    @Autowired
    private IMchAppService mchAppService;
    @Autowired
    private IMchInfoService mchInfoService;
    @Autowired
    private IMchIsvInfoService mchIsvInfoService;
    @Autowired
    private IPayChannelParamService payChannelParamService;

    @Override
    public MchApp queryMchApp(String mchNo, String appId) {
        //1.查询缓存
        MchAppContext mchAppContext = mchAppContextMap.get(appId);
        if(mchAppContext!=null){
            return mchAppContext.getMchApp();
        }
        return mchAppService.selectMchAppByAppIdAndMchNo(appId,mchNo);
    }

    @Override
    public MchAppContext queryMchInfoAndAppInfo(String mchNo, String appId) {
        MchInfo mchInfo = mchInfoService.selectMchInfoByMchNo(mchNo);
        MchApp mchApp = queryMchApp(mchNo, appId);
        if (mchInfo == null || mchApp == null) {
            return null;
        }
        MchAppContext result = new MchAppContext();
        result.setMchInfo(mchInfo);
        result.setMchNo(mchNo);
        result.setMchType(mchInfo.getType());
        result.setMchApp(mchApp);
        result.setAppId(appId);
        return result;
    }

    @Override
    public NormalMchParams queryNormalMchParams(String mchNo, String appId, String channelCode) {
        // 查询商户的所有支持的参数配置
        PayChannelParam payChannelParam = payChannelParamService.selectPayChannelParam(appId, channelCode,PayConstant.INFO_TYPE_MCH_APP, PayConstant.YES);
        if(payChannelParam == null){
            return null;
        }
        return NormalMchParams.factory(payChannelParam.getChannelCode(),payChannelParam.getChannelParams());
    }

    @Override
    public IsvSubMchParams queryIsvSubMchParams(String mchNo, String appId, String channelCode) {
        // 查询商户的所有支持的参数配置
        PayChannelParam payChannelParam = payChannelParamService.selectPayChannelParam(appId,channelCode,PayConstant.INFO_TYPE_MCH_APP, PayConstant.YES);
        if(payChannelParam == null){
            return null;
        }
        return IsvSubMchParams.factory(payChannelParam.getChannelCode(),payChannelParam.getChannelParams());
    }

    @Override
    public IsvParams queryIsvParams(String isvNo, String channelCode) {
        // 查询商户的所有支持的参数配置
        PayChannelParam payChannelParam = payChannelParamService.selectPayChannelParam(isvNo,channelCode,PayConstant.INFO_TYPE_ISV, PayConstant.YES);
        if(payChannelParam == null){
            return null;
        }
        return IsvParams.factory(payChannelParam.getChannelCode(),payChannelParam.getChannelParams());
    }

    @Override
    public AlipayClientWrapper getAlipayClientWrapper(MchAppContext mchAppContext) {
        if (mchAppContext.isIsvsubMch()) {
            AlipayIsvParams alipayParams = (AlipayIsvParams) queryIsvParams(mchAppContext.getMchInfo().getIsvNo(), PayConstant.CHANNEL_CODE.ALIPAY);
            return AlipayClientWrapper.buildAlipayClientWrapper(alipayParams);
        } else {
            AlipayNormalMchParams alipayParams = (AlipayNormalMchParams) queryNormalMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), PayConstant.CHANNEL_CODE.ALIPAY);
            return AlipayClientWrapper.buildAlipayClientWrapper(alipayParams);
        }
    }

    @Override
    public WxServiceWrapper getWxServiceWrapper(MchAppContext mchAppContext) {
        if(mchAppContext.isIsvsubMch()){
            WxpayIsvParams wxParams = (WxpayIsvParams)queryIsvParams(mchAppContext.getMchInfo().getIsvNo(), PayConstant.CHANNEL_CODE.WXPAY);
            return WxServiceWrapper.buildWxServiceWrapper(wxParams);
        }else{
            WxpayNormalMchParams wxParams = (WxpayNormalMchParams)queryNormalMchParams(mchAppContext.getMchNo(), mchAppContext.getAppId(), PayConstant.CHANNEL_CODE.WXPAY);
            return WxServiceWrapper.buildWxServiceWrapper(wxParams);
        }
    }



    /** 获取 [商户应用支付参数配置信息] **/
    public MchAppContext getMchAppContext(String mchNo, String appId){
        MchAppContext mchAppContext = mchAppContextMap.get(appId);
        if(mchAppContext==null){
            initMchAppContext(appId,mchNo);
        }
        return mchAppContext;
    }



    /** 初始化 [商户应用支付参数配置信息] **/
    private synchronized void initMchAppContext(String appId, String mchNo) {
// 获取商户的配置信息
        MchInfoContext mchInfoContext = getMchInfoContext(mchNo);
        if(mchInfoContext == null){ // 商户信息不存在
            return;
        }
        MchApp dbMchApp = mchAppService.selectMchAppByAppId(appId);
        //DB已经删除
        if(dbMchApp == null){
            mchAppContextMap.remove(appId);  //清除缓存信息
            mchInfoContext.getAppMap().remove(appId); //清除主体信息中的appId
            return ;
        }
        // 商户应用mchNo 与参数不匹配
        if(!dbMchApp.getMchNo().equals(mchNo)){
            return;
        }
        //更新商户信息主体中的商户应用
        mchInfoContext.putMchApp(dbMchApp);
       //商户主体信息
        MchInfo mchInfo = mchInfoContext.getMchInfo();
        MchAppContext mchAppContext = new MchAppContext();
        // 设置商户信息
        mchAppContext.setAppId(appId);
        mchAppContext.setMchNo(mchInfo.getMchNo());
        mchAppContext.setMchType(mchInfo.getType());
        mchAppContext.setMchInfo(mchInfo);
        mchAppContext.setMchApp(dbMchApp);
        List<PayChannelParam> payChannelParams = payChannelParamService.selectPayChannelParam(appId, PayConstant.INFO_TYPE_MCH_APP, PayConstant.YES);
        // 普通商户
        if(mchInfo.getType() == PayConstant.MCH_TYPE_NORMAL){
            for (PayChannelParam payChannelParam : payChannelParams) {
                mchAppContext.getNormalMchParamsMap().put(
                        payChannelParam.getChannelCode(),
                        NormalMchParams.factory(payChannelParam.getChannelCode(), payChannelParam.getChannelParams())
                );
            }
            //放置alipay client
            AlipayNormalMchParams alipayParams = mchAppContext.getNormalMchParamsByChannelCode(PayConstant.CHANNEL_CODE.ALIPAY, AlipayNormalMchParams.class);
            if(alipayParams != null){
                mchAppContext.setAlipayClientWrapper(AlipayClientWrapper.buildAlipayClientWrapper(alipayParams));
            }
            //放置 wxJavaService
            WxpayNormalMchParams wxpayParams = mchAppContext.getNormalMchParamsByChannelCode(PayConstant.CHANNEL_CODE.WXPAY, WxpayNormalMchParams.class);
            if(wxpayParams != null){
                mchAppContext.setWxServiceWrapper(WxServiceWrapper.buildWxServiceWrapper(wxpayParams));
            }
        }else{ //服务商模式商户
            for (PayChannelParam payChannelParam : payChannelParams) {
                mchAppContext.getIsvSubMchParamsMap().put(
                        payChannelParam.getChannelCode(),
                        IsvSubMchParams.factory(payChannelParam.getChannelCode(), payChannelParam.getChannelParams())
                );
            }
            //放置 当前商户的 服务商信息
            mchAppContext.setIsvConfigContext(getIsvConfigContext(mchInfo.getIsvNo()));
        }
        mchAppContextMap.put(appId, mchAppContext);


    }
    /** 获取 [商户配置信息] **/
    public MchInfoContext getMchInfoContext(String mchNo){
        MchInfoContext mchInfoContext = mchInfoContextMap.get(mchNo);
        if(mchInfoContext==null){
            initMchInfoContext(mchNo);
        }
        return mchInfoContext;
    }
    /** 初始化 [商户配置信息] **/
    private synchronized void initMchInfoContext(String mchNo) {
        MchInfo mchInfo = mchInfoService.selectMchInfoByMchNo(mchNo);
        if(mchInfo==null){
            MchInfoContext mchInfoContext = mchInfoContextMap.get(mchNo);
            // 删除所有的商户应用
            if(mchInfoContext != null){
                mchInfoContext.getAppMap().forEach((k, v) -> mchAppContextMap.remove(k));
            }
            mchAppContextMap.remove(mchNo);
            return ;
        }
        MchInfoContext mchInfoContext = new MchInfoContext();
        // 设置商户信息
        mchInfoContext.setMchNo(mchInfo.getMchNo());
        mchInfoContext.setMchType(mchInfo.getType());
        mchInfoContext.setMchInfo(mchInfo);
        MchApp dbMchApp=new MchApp();
        dbMchApp.setMchNo(mchNo);
        List<MchApp> mchApps = mchAppService.selectMchAppList(dbMchApp);
        mchApps.forEach(mchApp -> {
            //1. 更新商户内appId集合
            mchInfoContext.putMchApp(mchApp);
            MchAppContext mchAppContext = mchAppContextMap.get(mchApp.getAppId());
            if(mchAppContext != null){
                mchAppContext.setMchApp(mchApp);
                mchAppContext.setMchNo(mchInfo.getMchNo());
                mchAppContext.setMchType(mchInfo.getType());
                mchAppContext.setMchInfo(mchInfo);
            }
        });
        mchInfoContextMap.put(mchNo,mchInfoContext);
    }
    /** 获取 [ISV支付参数配置信息] **/
    public IsvConfigContext getIsvConfigContext(String isvNo){
        IsvConfigContext isvConfigContext = isvConfigContextMap.get(isvNo);
        //无此数据， 需要初始化
        if(isvConfigContext == null){
            initIsvConfigContext(isvNo);
        }
        return isvConfigContextMap.get(isvNo);
    }
    /** 初始化 [商户配置信息] **/
    private void initIsvConfigContext(String isvNo) {
        //查询出所有商户的配置信息并更新
        List<String> mchNoList = new ArrayList<>();
        MchInfo mchInfoParam=new MchInfo();
        mchInfoParam.setIsvNo(isvNo);
        List<MchInfo> mchInfos = mchInfoService.selectMchInfoList(mchInfoParam);
        mchInfos.forEach(mchInfo -> {
            mchNoList.add(mchInfo.getMchNo());
        });
        // 查询出所有 所属当前服务商的所有应用集合
        List<String> mchAppIdList = new ArrayList<>();
        if(!mchNoList.isEmpty()){
         List<MchApp> mchApps= mchAppService.selectMchAppByMchNoList(mchNoList);
         mchApps.forEach(mchApp -> {
             mchAppIdList.add(mchApp.getAppId());
         });
        }
        IsvConfigContext isvConfigContext = new IsvConfigContext();
        MchIsvInfo mchIsvInfo = mchIsvInfoService.selectMchIsvInfoByIsvNo(isvNo);
        if(mchIsvInfo == null){
            for (String appId : mchAppIdList) {
                //将更新已存在缓存的商户配置信息 （每个商户下存储的为同一个 服务商配置的对象指针）
                MchAppContext mchAppConfigContext = mchAppContextMap.get(appId);
                if(mchAppConfigContext != null){
                    mchAppConfigContext.setIsvConfigContext(null);
                }
            }
            isvConfigContextMap.remove(isvNo); // 服务商有商户不可删除， 此处不再更新商户下的配置信息
            return ;
        }
        // 设置商户信息
        isvConfigContext.setIsvNo(mchIsvInfo.getIsvNo());
        isvConfigContext.setMchIsvInfo(mchIsvInfo);
        // 查询商户的所有支持的参数配置
        List<PayChannelParam> payChannelParams = payChannelParamService.selectPayChannelParam(isvNo, PayConstant.INFO_TYPE_ISV, PayConstant.YES);

        for (PayChannelParam payChannelParam : payChannelParams) {
            isvConfigContext.getIsvParamsMap().put(
                    payChannelParam.getChannelCode(),
                    IsvParams.factory(payChannelParam.getChannelCode(), payChannelParam.getChannelParams())
            );
        }

        //放置alipay client
        AlipayIsvParams alipayParams = isvConfigContext.getIsvParamsByIfCode(PayConstant.CHANNEL_CODE.ALIPAY, AlipayIsvParams.class);
        if(alipayParams != null){
            isvConfigContext.setAlipayClientWrapper(AlipayClientWrapper.buildAlipayClientWrapper(alipayParams));
        }

        //放置 wxJavaService
        WxpayIsvParams wxpayParams = isvConfigContext.getIsvParamsByIfCode(PayConstant.CHANNEL_CODE.WXPAY, WxpayIsvParams.class);
        if(wxpayParams != null){
            isvConfigContext.setWxServiceWrapper(WxServiceWrapper.buildWxServiceWrapper(wxpayParams));
        }

        isvConfigContextMap.put(isvNo, isvConfigContext);

        //查询出所有商户的配置信息并更新
        for (String appId : mchAppIdList) {
            //将更新已存在缓存的商户配置信息 （每个商户下存储的为同一个 服务商配置的对象指针）
            MchAppContext mchAppConfigContext = mchAppContextMap.get(appId);
            if(mchAppConfigContext != null){
                mchAppConfigContext.setIsvConfigContext(isvConfigContext);
            }
        }

    }

}
