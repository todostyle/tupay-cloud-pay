package com.tostyle.tupay.payment.service.impl;

import java.util.List;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.MchAppMapper;
import com.tostyle.tupay.payment.domain.MchApp;
import com.tostyle.tupay.payment.service.IMchAppService;

/**
 * 应用列表Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class MchAppServiceImpl implements IMchAppService 
{
    @Autowired
    private MchAppMapper mchAppMapper;

    /**
     * 查询应用列表
     * 
     * @param appId 应用列表主键
     * @return 应用列表
     */
    @Override
    public MchApp selectMchAppByAppId(String appId)
    {
        return mchAppMapper.selectMchAppByAppId(appId);
    }

    /**
     * 查询应用列表列表
     * 
     * @param mchApp 应用列表
     * @return 应用列表
     */
    @Override
    public List<MchApp> selectMchAppList(MchApp mchApp)
    {
        return mchAppMapper.selectMchAppList(mchApp);
    }

    /**
     * 新增应用列表
     * 
     * @param mchApp 应用列表
     * @return 结果
     */
    @Override
    public int insertMchApp(MchApp mchApp)
    {
        mchApp.setCreateTime(DateUtils.getNowDate());
        return mchAppMapper.insertMchApp(mchApp);
    }

    /**
     * 修改应用列表
     * 
     * @param mchApp 应用列表
     * @return 结果
     */
    @Override
    public int updateMchApp(MchApp mchApp)
    {
        mchApp.setUpdateTime(DateUtils.getNowDate());
        return mchAppMapper.updateMchApp(mchApp);
    }

    /**
     * 批量删除应用列表
     * 
     * @param appIds 需要删除的应用列表主键
     * @return 结果
     */
    @Override
    public int deleteMchAppByAppIds(String[] appIds)
    {
        return mchAppMapper.deleteMchAppByAppIds(appIds);
    }

    /**
     * 删除应用列表信息
     * 
     * @param appId 应用列表主键
     * @return 结果
     */
    @Override
    public int deleteMchAppByAppId(String appId)
    {
        return mchAppMapper.deleteMchAppByAppId(appId);
    }

    @Override
    public MchApp selectMchAppByAppIdAndMchNo(String appId, String mchNo) {
        return mchAppMapper.selectMchAppByAppIdAndMchNo(appId,mchNo);
    }

    @Override
    public List<MchApp> selectMchAppByMchNoList(List<String> mchNoList) {
        return mchAppMapper.selectMchAppByMchNoList(mchNoList);
    }
}
