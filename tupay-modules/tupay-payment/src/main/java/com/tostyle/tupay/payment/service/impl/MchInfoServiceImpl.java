package com.tostyle.tupay.payment.service.impl;

import java.util.List;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.MchInfoMapper;
import com.tostyle.tupay.payment.domain.MchInfo;
import com.tostyle.tupay.payment.service.IMchInfoService;

/**
 * 商户列表Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class MchInfoServiceImpl implements IMchInfoService 
{
    @Autowired
    private MchInfoMapper mchInfoMapper;

    /**
     * 查询商户列表
     * 
     * @param mchNo 商户列表主键
     * @return 商户列表
     */
    @Override
    public MchInfo selectMchInfoByMchNo(String mchNo)
    {
        return mchInfoMapper.selectMchInfoByMchNo(mchNo);
    }

    /**
     * 查询商户列表列表
     * 
     * @param mchInfo 商户列表
     * @return 商户列表
     */
    @Override
    public List<MchInfo> selectMchInfoList(MchInfo mchInfo)
    {
        return mchInfoMapper.selectMchInfoList(mchInfo);
    }

    /**
     * 新增商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    @Override
    public int insertMchInfo(MchInfo mchInfo)
    {
        mchInfo.setCreateTime(DateUtils.getNowDate());
        return mchInfoMapper.insertMchInfo(mchInfo);
    }

    /**
     * 修改商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    @Override
    public int updateMchInfo(MchInfo mchInfo)
    {
        mchInfo.setUpdateTime(DateUtils.getNowDate());
        return mchInfoMapper.updateMchInfo(mchInfo);
    }

    /**
     * 批量删除商户列表
     * 
     * @param mchNos 需要删除的商户列表主键
     * @return 结果
     */
    @Override
    public int deleteMchInfoByMchNos(String[] mchNos)
    {
        return mchInfoMapper.deleteMchInfoByMchNos(mchNos);
    }

    /**
     * 删除商户列表信息
     * 
     * @param mchNo 商户列表主键
     * @return 结果
     */
    @Override
    public int deleteMchInfoByMchNo(String mchNo)
    {
        return mchInfoMapper.deleteMchInfoByMchNo(mchNo);
    }
}
