package com.tostyle.tupay.payment.service.impl;

import java.util.List;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.MchIsvInfoMapper;
import com.tostyle.tupay.payment.domain.MchIsvInfo;
import com.tostyle.tupay.payment.service.IMchIsvInfoService;

/**
 * 服务商列表Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class MchIsvInfoServiceImpl implements IMchIsvInfoService 
{
    @Autowired
    private MchIsvInfoMapper mchIsvInfoMapper;

    /**
     * 查询服务商列表
     * 
     * @param isvNo 服务商列表主键
     * @return 服务商列表
     */
    @Override
    public MchIsvInfo selectMchIsvInfoByIsvNo(String isvNo)
    {
        return mchIsvInfoMapper.selectMchIsvInfoByIsvNo(isvNo);
    }

    /**
     * 查询服务商列表列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 服务商列表
     */
    @Override
    public List<MchIsvInfo> selectMchIsvInfoList(MchIsvInfo mchIsvInfo)
    {
        return mchIsvInfoMapper.selectMchIsvInfoList(mchIsvInfo);
    }

    /**
     * 新增服务商列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 结果
     */
    @Override
    public int insertMchIsvInfo(MchIsvInfo mchIsvInfo)
    {
        mchIsvInfo.setCreateTime(DateUtils.getNowDate());
        return mchIsvInfoMapper.insertMchIsvInfo(mchIsvInfo);
    }

    /**
     * 修改服务商列表
     * 
     * @param mchIsvInfo 服务商列表
     * @return 结果
     */
    @Override
    public int updateMchIsvInfo(MchIsvInfo mchIsvInfo)
    {
        mchIsvInfo.setUpdateTime(DateUtils.getNowDate());
        return mchIsvInfoMapper.updateMchIsvInfo(mchIsvInfo);
    }

    /**
     * 批量删除服务商列表
     * 
     * @param isvNos 需要删除的服务商列表主键
     * @return 结果
     */
    @Override
    public int deleteMchIsvInfoByIsvNos(String[] isvNos)
    {
        return mchIsvInfoMapper.deleteMchIsvInfoByIsvNos(isvNos);
    }

    /**
     * 删除服务商列表信息
     * 
     * @param isvNo 服务商列表主键
     * @return 结果
     */
    @Override
    public int deleteMchIsvInfoByIsvNo(String isvNo)
    {
        return mchIsvInfoMapper.deleteMchIsvInfoByIsvNo(isvNo);
    }
}
