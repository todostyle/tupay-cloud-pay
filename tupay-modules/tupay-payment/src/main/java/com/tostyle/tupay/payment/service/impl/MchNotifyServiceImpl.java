package com.tostyle.tupay.payment.service.impl;

import java.util.List;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.MchNotifyMapper;
import com.tostyle.tupay.payment.domain.MchNotify;
import com.tostyle.tupay.payment.service.IMchNotifyService;

/**
 * 商户通知Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class MchNotifyServiceImpl implements IMchNotifyService 
{
    @Autowired
    private MchNotifyMapper mchNotifyMapper;

    /**
     * 查询商户通知
     * 
     * @param notifyId 商户通知主键
     * @return 商户通知
     */
    @Override
    public MchNotify selectMchNotifyByNotifyId(Long notifyId)
    {
        return mchNotifyMapper.selectMchNotifyByNotifyId(notifyId);
    }

    /**
     * 查询商户通知列表
     * 
     * @param mchNotify 商户通知
     * @return 商户通知
     */
    @Override
    public List<MchNotify> selectMchNotifyList(MchNotify mchNotify)
    {
        return mchNotifyMapper.selectMchNotifyList(mchNotify);
    }

    /**
     * 新增商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    @Override
    public int insertMchNotify(MchNotify mchNotify)
    {
        mchNotify.setCreateTime(DateUtils.getNowDate());
        return mchNotifyMapper.insertMchNotify(mchNotify);
    }

    /**
     * 修改商户通知
     * 
     * @param mchNotify 商户通知
     * @return 结果
     */
    @Override
    public int updateMchNotify(MchNotify mchNotify)
    {
        mchNotify.setUpdateTime(DateUtils.getNowDate());
        return mchNotifyMapper.updateMchNotify(mchNotify);
    }

    /**
     * 批量删除商户通知
     * 
     * @param notifyIds 需要删除的商户通知主键
     * @return 结果
     */
    @Override
    public int deleteMchNotifyByNotifyIds(Long[] notifyIds)
    {
        return mchNotifyMapper.deleteMchNotifyByNotifyIds(notifyIds);
    }

    /**
     * 删除商户通知信息
     * 
     * @param notifyId 商户通知主键
     * @return 结果
     */
    @Override
    public int deleteMchNotifyByNotifyId(Long notifyId)
    {
        return mchNotifyMapper.deleteMchNotifyByNotifyId(notifyId);
    }

    @Override
    public int updateNotifyResult(Long notifyId, Long stateSuccess, String res) {
        return mchNotifyMapper.updateNotifyResult(notifyId,stateSuccess,res);
    }

    @Override
    public MchNotify selectMchNotifyByPayOrder(String orderId) {
        return mchNotifyMapper.selectMchNotifyByOrderAndType(orderId, PayConstant.TYPE_PAY_ORDER);
    }

    @Override
    public MchNotify selectMchNotifyByRefundOrder(String orderId) {
        return mchNotifyMapper.selectMchNotifyByOrderAndType(orderId,PayConstant.TYPE_REFUND_ORDER);
    }

    @Override
    public MchNotify selectMchNotifyByTransferOrder(String orderId) {
        return mchNotifyMapper.selectMchNotifyByOrderAndType(orderId,PayConstant.TYPE_TRANSFER_ORDER);
    }
}
