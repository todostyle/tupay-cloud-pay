package com.tostyle.tupay.payment.service.impl;

import com.tostyle.tupay.common.core.utils.DateUtils;
import com.tostyle.tupay.payment.domain.MchPayChannel;
import com.tostyle.tupay.payment.mapper.MchPayChannelMapper;
import com.tostyle.tupay.payment.service.IMchPayChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商户支付通道Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
@Service
public class MchPayChannelServiceImpl implements IMchPayChannelService
{
    @Autowired
    private MchPayChannelMapper mchPayChannelMapper;

    /**
     * 查询商户支付通道
     * 
     * @param id 商户支付通道主键
     * @return 商户支付通道
     */
    @Override
    public MchPayChannel selectMchPayChannelById(Long id)
    {
        return mchPayChannelMapper.selectMchPayChannelById(id);
    }

    /**
     * 查询商户支付通道列表
     * 
     * @param mchPayChannel 商户支付通道
     * @return 商户支付通道
     */
    @Override
    public List<MchPayChannel> selectMchPayChannelList(MchPayChannel mchPayChannel)
    {
        return mchPayChannelMapper.selectMchPayChannelList(mchPayChannel);
    }

    /**
     * 新增商户支付通道
     * 
     * @param mchPayChannel 商户支付通道
     * @return 结果
     */
    @Override
    public int insertMchPayChannel(MchPayChannel mchPayChannel)
    {
        mchPayChannel.setCreateTime(DateUtils.getNowDate());
        return mchPayChannelMapper.insertMchPayChannel(mchPayChannel);
    }

    /**
     * 修改商户支付通道
     * 
     * @param mchPayChannel 商户支付通道
     * @return 结果
     */
    @Override
    public int updateMchPayChannel(MchPayChannel mchPayChannel)
    {
        mchPayChannel.setUpdateTime(DateUtils.getNowDate());
        return mchPayChannelMapper.updateMchPayChannel(mchPayChannel);
    }

    /**
     * 批量删除商户支付通道
     * 
     * @param ids 需要删除的商户支付通道主键
     * @return 结果
     */
    @Override
    public int deleteMchPayChannelByIds(Long[] ids)
    {
        return mchPayChannelMapper.deleteMchPayChannelByIds(ids);
    }

    /**
     * 删除商户支付通道信息
     * 
     * @param id 商户支付通道主键
     * @return 结果
     */
    @Override
    public int deleteMchPayChannelById(Long id)
    {
        return mchPayChannelMapper.deleteMchPayChannelById(id);
    }

    @Override
    public MchPayChannel getMchPayChannel(String mchNo, String appId, String wayCode) {
        return mchPayChannelMapper.getMchPayChannel(mchNo,appId,wayCode);
    }
}