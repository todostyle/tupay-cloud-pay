package com.tostyle.tupay.payment.service.impl;

import java.util.List;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.PayChannelDefineMapper;
import com.tostyle.tupay.payment.domain.PayChannelDefine;
import com.tostyle.tupay.payment.service.IPayChannelDefineService;

/**
 * 支付渠道定义Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class PayChannelDefineServiceImpl implements IPayChannelDefineService 
{
    @Autowired
    private PayChannelDefineMapper payChannelDefineMapper;

    /**
     * 查询支付渠道定义
     * 
     * @param channelCode 支付渠道定义主键
     * @return 支付渠道定义
     */
    @Override
    public PayChannelDefine selectPayChannelDefineByChannelCode(String channelCode)
    {
        return payChannelDefineMapper.selectPayChannelDefineByChannelCode(channelCode);
    }

    /**
     * 查询支付渠道定义列表
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 支付渠道定义
     */
    @Override
    public List<PayChannelDefine> selectPayChannelDefineList(PayChannelDefine payChannelDefine)
    {
        return payChannelDefineMapper.selectPayChannelDefineList(payChannelDefine);
    }

    /**
     * 新增支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    @Override
    public int insertPayChannelDefine(PayChannelDefine payChannelDefine)
    {
        payChannelDefine.setCreateTime(DateUtils.getNowDate());
        return payChannelDefineMapper.insertPayChannelDefine(payChannelDefine);
    }

    /**
     * 修改支付渠道定义
     * 
     * @param payChannelDefine 支付渠道定义
     * @return 结果
     */
    @Override
    public int updatePayChannelDefine(PayChannelDefine payChannelDefine)
    {
        payChannelDefine.setUpdateTime(DateUtils.getNowDate());
        return payChannelDefineMapper.updatePayChannelDefine(payChannelDefine);
    }

    /**
     * 批量删除支付渠道定义
     * 
     * @param channelCodes 需要删除的支付渠道定义主键
     * @return 结果
     */
    @Override
    public int deletePayChannelDefineByChannelCodes(String[] channelCodes)
    {
        return payChannelDefineMapper.deletePayChannelDefineByChannelCodes(channelCodes);
    }

    /**
     * 删除支付渠道定义信息
     * 
     * @param channelCode 支付渠道定义主键
     * @return 结果
     */
    @Override
    public int deletePayChannelDefineByChannelCode(String channelCode)
    {
        return payChannelDefineMapper.deletePayChannelDefineByChannelCode(channelCode);
    }
}
