package com.tostyle.tupay.payment.service.impl;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.DateUtils;
import com.tostyle.tupay.payment.domain.PayChannelParam;
import com.tostyle.tupay.payment.mapper.PayChannelParamMapper;
import com.tostyle.tupay.payment.service.IPayChannelParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付接口配置参数Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-04-18
 */
@Service
public class PayChannelParamServiceImpl implements IPayChannelParamService
{
    @Autowired
    private PayChannelParamMapper payChannelParamMapper;

    /**
     * 查询支付接口配置参数
     * 
     * @param id 支付接口配置参数主键
     * @return 支付接口配置参数
     */
    @Override
    public PayChannelParam selectPayChannelParamById(Long id)
    {
        return payChannelParamMapper.selectPayChannelParamById(id);
    }

    /**
     * 查询支付接口配置参数列表
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 支付接口配置参数
     */
    @Override
    public List<PayChannelParam> selectPayChannelParamList(PayChannelParam payChannelParam)
    {
        return payChannelParamMapper.selectPayChannelParamList(payChannelParam);
    }

    /**
     * 新增支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    @Override
    public int insertPayChannelParam(PayChannelParam payChannelParam)
    {
        payChannelParam.setCreateTime(DateUtils.getNowDate());
        return payChannelParamMapper.insertPayChannelParam(payChannelParam);
    }

    /**
     * 修改支付接口配置参数
     * 
     * @param payChannelParam 支付接口配置参数
     * @return 结果
     */
    @Override
    public int updatePayChannelParam(PayChannelParam payChannelParam)
    {
        payChannelParam.setUpdateTime(DateUtils.getNowDate());
        return payChannelParamMapper.updatePayChannelParam(payChannelParam);
    }

    /**
     * 批量删除支付接口配置参数
     * 
     * @param ids 需要删除的支付接口配置参数主键
     * @return 结果
     */
    @Override
    public int deletePayChannelParamByIds(Long[] ids)
    {
        return payChannelParamMapper.deletePayChannelParamByIds(ids);
    }

    /**
     * 删除支付接口配置参数信息
     * 
     * @param id 支付接口配置参数主键
     * @return 结果
     */
    @Override
    public int deletePayChannelParamById(Long id)
    {
        return payChannelParamMapper.deletePayChannelParamById(id);
    }

    @Override
    public List<PayChannelParam> selectPayChannelParam(String infoId, Long infoType, Long state) {
        PayChannelParam payChannelParam=new PayChannelParam();
        payChannelParam.setInfoId(infoId);
        payChannelParam.setInfoType(infoType);
        payChannelParam.setState(state);
        return payChannelParamMapper.selectPayChannelParamList(payChannelParam);
    }

    @Override
    public PayChannelParam selectPayChannelParam(String infoId,String channelCode, Long infoType, Long state) {
        return payChannelParamMapper.selectPayChannelParam(infoId,channelCode,infoType,state);
    }

    @Override
    public boolean mchAppHasAvailableChannelCode(String appId, String channelCode) {
        PayChannelParam payChannelParam = selectPayChannelParam(appId, channelCode, PayConstant.INFO_TYPE_MCH_APP, PayConstant.PUB_USABLE);
        return payChannelParam!=null;
    }
}