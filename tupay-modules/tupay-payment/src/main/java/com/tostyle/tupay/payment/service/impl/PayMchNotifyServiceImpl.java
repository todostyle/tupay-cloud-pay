package com.tostyle.tupay.payment.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.common.core.utils.sign.Md5Utils;
import com.tostyle.tupay.payment.domain.MchNotify;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.domain.model.payorder.QueryPayOrderResp;
import com.tostyle.tupay.payment.domain.model.refund.QueryRefundOrderResp;
import com.tostyle.tupay.payment.domain.model.transfer.QueryTransferOrderResp;
import com.tostyle.tupay.payment.mq.MqSender;
import com.tostyle.tupay.payment.mq.model.PayOrderMchNotifyMq;
import com.tostyle.tupay.payment.service.ContextService;
import com.tostyle.tupay.payment.service.IMchNotifyService;
import com.tostyle.tupay.payment.service.IPayMchNotifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tostyle
 * 2022/4/20 11:28
 */
@Service
@Slf4j
public class PayMchNotifyServiceImpl implements IPayMchNotifyService {

    @Autowired
    private IMchNotifyService mchNotifyService;
    @Autowired
    private ContextService contextService;
    @Autowired
    private MqSender mqSender;

    @Override
    public void payOrderNotify(PayOrder dbPayOrder) {
        try {
            // 通知地址为空
            if(StringUtils.isEmpty(dbPayOrder.getNotifyUrl())){
                return ;
            }
            //获取到通知对象
            MchNotify mchNotify = mchNotifyService.selectMchNotifyByPayOrder(dbPayOrder.getTradeNo());
            if(mchNotify != null){

                log.info("当前已存在通知消息， 不再发送。");
                return ;
            }
            //商户app私钥
            String appSecret = contextService.queryMchApp(dbPayOrder.getMchNo(), dbPayOrder.getAppId()).getAppSecret();
            // 封装通知url
            String notifyUrl = createNotifyUrl(dbPayOrder, appSecret);
            mchNotify = new MchNotify();
            mchNotify.setOrderId(dbPayOrder.getTradeNo());
            mchNotify.setOrderType(PayConstant.TYPE_PAY_ORDER);
            mchNotify.setMchNo(dbPayOrder.getMchNo());
            mchNotify.setMchOrderNo(dbPayOrder.getMchOrderNo()); //商户订单号
            mchNotify.setIsvNo(dbPayOrder.getIsvNo());
            mchNotify.setAppId(dbPayOrder.getAppId());
            mchNotify.setNotifyUrl(notifyUrl);
            mchNotify.setResResult("");
            mchNotify.setNotifyCount(0L);
            mchNotify.setState(PayConstant.NOTIFY_STATE_ING); // 通知中
            try {
                mchNotifyService.insertMchNotify(mchNotify);
            } catch (Exception e) {
                log.info("数据库已存在[{}]消息，本次不再推送。", mchNotify.getOrderId());
                return ;
            }
            //推送到MQ
            Long notifyId = mchNotify.getNotifyId();
            mqSender.send(PayOrderMchNotifyMq.buildMq(notifyId));
        } catch (Exception e) {
            log.error("推送失败！", e);
        }
    }

    @Override
    public void refundOrderNotify(RefundOrder dbRefundOrder) {
        try {
            // 通知地址为空
            if(StringUtils.isEmpty(dbRefundOrder.getNotifyUrl())){
                return ;
            }
            //获取到通知对象
            MchNotify mchNotify = mchNotifyService.selectMchNotifyByRefundOrder(dbRefundOrder.getRefundOrderId());
            if(mchNotify != null){
                log.info("当前已存在通知消息， 不再发送。");
                return ;
            }
            //商户app私钥
            String appSecret = contextService.queryMchApp(dbRefundOrder.getMchNo(), dbRefundOrder.getAppId()).getAppSecret();
            // 封装通知url
            String notifyUrl = createNotifyUrl(dbRefundOrder, appSecret);
            mchNotify = new MchNotify();
            mchNotify.setOrderId(dbRefundOrder.getRefundOrderId());
            mchNotify.setOrderType(PayConstant.TYPE_REFUND_ORDER);
            mchNotify.setMchNo(dbRefundOrder.getMchNo());
            mchNotify.setMchOrderNo(dbRefundOrder.getMchRefundNo()); //商户订单号
            mchNotify.setIsvNo(dbRefundOrder.getIsvNo());
            mchNotify.setAppId(dbRefundOrder.getAppId());
            mchNotify.setNotifyUrl(notifyUrl);
            mchNotify.setResResult("");
            mchNotify.setNotifyCount(0L);
            mchNotify.setState(PayConstant.NOTIFY_STATE_ING); // 通知中
            try {
                mchNotifyService.insertMchNotify(mchNotify);
            } catch (Exception e) {
                log.info("数据库已存在[{}]消息，本次不再推送。", mchNotify.getOrderId());
                return ;
            }

            //推送到MQ
            Long notifyId = mchNotify.getNotifyId();
            mqSender.send(PayOrderMchNotifyMq.buildMq(notifyId));

        } catch (Exception e) {
            log.error("推送失败！", e);
        }
    }

    @Override
    public void transferOrderNotify(TransferOrder dbTransferOrder) {
        try {
            // 通知地址为空
            if(StringUtils.isEmpty(dbTransferOrder.getNotifyUrl())){
                return ;
            }
            //获取到通知对象
            MchNotify mchNotify = mchNotifyService.selectMchNotifyByTransferOrder(dbTransferOrder.getTransferId());

            if(mchNotify != null){
                log.info("当前已存在通知消息， 不再发送。");
                return ;
            }

            //商户app私钥
            String appSecret = contextService.queryMchApp(dbTransferOrder.getMchNo(), dbTransferOrder.getAppId()).getAppSecret();

            // 封装通知url
            String notifyUrl = createNotifyUrl(dbTransferOrder, appSecret);
            mchNotify = new MchNotify();
            mchNotify.setOrderId(dbTransferOrder.getTransferId());
            mchNotify.setOrderType(PayConstant.TYPE_TRANSFER_ORDER);
            mchNotify.setMchNo(dbTransferOrder.getMchNo());
            mchNotify.setMchOrderNo(dbTransferOrder.getMchOrderNo()); //商户订单号
            mchNotify.setIsvNo(dbTransferOrder.getIsvNo());
            mchNotify.setAppId(dbTransferOrder.getAppId());
            mchNotify.setNotifyUrl(notifyUrl);
            mchNotify.setResResult("");
            mchNotify.setNotifyCount(0L);
            mchNotify.setState(PayConstant.NOTIFY_STATE_ING); // 通知中

            try {
                mchNotifyService.insertMchNotify(mchNotify);
            } catch (Exception e) {
                log.info("数据库已存在[{}]消息，本次不再推送。", mchNotify.getOrderId());
                return ;
            }
            //推送到MQ
            Long notifyId = mchNotify.getNotifyId();
            mqSender.send(PayOrderMchNotifyMq.buildMq(notifyId));
        } catch (Exception e) {
            log.error("推送失败！", e);
        }
    }

    @Override
    public String createNotifyUrl(PayOrder payOrder, String appSecret) {
        QueryPayOrderResp queryPayOrderResp = QueryPayOrderResp.buildByPayOrder(payOrder);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(queryPayOrderResp);
        jsonObject.put("reqTime", System.currentTimeMillis()); //添加请求时间
        // 报文签名
        jsonObject.put("sign", Md5Utils.getSign(jsonObject, appSecret));
        // 生成通知
        return StringUtils.appendUrlQuery(payOrder.getNotifyUrl(), jsonObject);
    }

    @Override
    public String createNotifyUrl(RefundOrder refundOrder, String appSecret) {
        QueryRefundOrderResp queryRefundOrderResp = QueryRefundOrderResp.buildByRefundOrder(refundOrder);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(queryRefundOrderResp);
        jsonObject.put("reqTime", System.currentTimeMillis()); //添加请求时间
        // 报文签名
        jsonObject.put("sign", Md5Utils.getSign(jsonObject, appSecret));
        // 生成通知
        return StringUtils.appendUrlQuery(refundOrder.getNotifyUrl(), jsonObject);
    }

    @Override
    public String createNotifyUrl(TransferOrder transferOrder, String appSecret) {
        QueryTransferOrderResp queryTransferOrderResp = QueryTransferOrderResp.buildByRecord(transferOrder);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(queryTransferOrderResp);
        jsonObject.put("reqTime", System.currentTimeMillis()); //添加请求时间
        // 报文签名
        jsonObject.put("sign", Md5Utils.getSign(jsonObject, appSecret));
        // 生成通知
        return StringUtils.appendUrlQuery(transferOrder.getNotifyUrl(), jsonObject);
    }

    @Override
    public String createReturnUrl(PayOrder payOrder, String appSecret) {
        if(StringUtils.isEmpty(payOrder.getReturnUrl())){
            return "";
        }
        QueryPayOrderResp queryPayOrderResp = QueryPayOrderResp.buildByPayOrder(payOrder);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(queryPayOrderResp);
        jsonObject.put("reqTime", System.currentTimeMillis()); //添加请求时间
        // 报文签名
        jsonObject.put("sign", Md5Utils.getSign(jsonObject, appSecret));   // 签名
        // 生成跳转地址
        return StringUtils.appendUrlQuery(payOrder.getReturnUrl(), jsonObject);
    }
}
