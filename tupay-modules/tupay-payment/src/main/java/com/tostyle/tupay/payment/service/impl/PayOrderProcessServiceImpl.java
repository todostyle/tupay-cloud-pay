package com.tostyle.tupay.payment.service.impl;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.mq.MqSender;
import com.tostyle.tupay.payment.service.IPayMchNotifyService;
import com.tostyle.tupay.payment.service.IPayOrderService;
import com.tostyle.tupay.payment.service.PayOrderProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单处理通用逻辑
 * @author tostyle
 * 2022/4/18 14:32
 */
@Service
@Slf4j
public class PayOrderProcessServiceImpl implements PayOrderProcessService {

    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private IPayMchNotifyService payMchNotifyService;
    @Autowired
    private MqSender mqSender;


    /** 明确成功的处理逻辑（除更新订单其他业务） **/
    @Override
    public void confirmSuccess(PayOrder payOrder) {

        //设置订单状态
        payOrder.setState(PayConstant.STATE_SUCCESS);
        //自动分账 处理逻辑， 不影响主订单任务
        this.updatePayOrderAutoDivision(payOrder);

        //发送商户通知
        payMchNotifyService.payOrderNotify(payOrder);

    }


    /** 更新订单自动分账业务 **/
    private void updatePayOrderAutoDivision(PayOrder payOrder){
        try {
            //默认不分账  || 其他非【自动分账】逻辑时， 不处理
            if(payOrder == null || payOrder.getDivisionMode() == null || payOrder.getDivisionMode() != PayConstant.DIVISION_MODE_AUTO){
                return ;
            }

            //更新订单表分账状态为： 等待分账任务处理
            /*boolean updDivisionState = payOrderService.updateDivisionStateByTradeNo(payOrder.getTradeNo(),PayConstant.DIVISION_STATE_UNHAPPEN);

            if(updDivisionState){
                //推送到分账MQ
               // mqSender.send(PayOrderDivisionMQ.build(payOrder.getTradeNo(), PayConstant.YES,null), 80); //80s 后执行
            }*/
        } catch (Exception e) {
            log.error("订单[{}]自动分账逻辑异常：", payOrder.getTradeNo(), e);
        }
    }
}
