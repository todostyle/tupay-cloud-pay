package com.tostyle.tupay.payment.service.impl;

import java.util.Date;
import java.util.List;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.DateUtils;
import com.tostyle.tupay.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.PayOrderMapper;
import com.tostyle.tupay.payment.domain.PayOrder;
import com.tostyle.tupay.payment.service.IPayOrderService;

/**
 * 支付订单Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class PayOrderServiceImpl implements IPayOrderService 
{
    @Autowired
    private PayOrderMapper payOrderMapper;

    /**
     * 查询支付订单
     * 
     * @param tradeNo 支付订单主键
     * @return 支付订单
     */
    @Override
    public PayOrder selectPayOrderByTradeNo(String tradeNo)
    {
        return payOrderMapper.selectPayOrderByTradeNo(tradeNo);
    }

    /**
     * 查询支付订单列表
     * 
     * @param payOrder 支付订单
     * @return 支付订单
     */
    @Override
    public List<PayOrder> selectPayOrderList(PayOrder payOrder)
    {
        return payOrderMapper.selectPayOrderList(payOrder);
    }

    /**
     * 新增支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    @Override
    public int insertPayOrder(PayOrder payOrder)
    {
        payOrder.setCreateTime(DateUtils.getNowDate());
        return payOrderMapper.insertPayOrder(payOrder);
    }

    /**
     * 修改支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    @Override
    public int updatePayOrder(PayOrder payOrder)
    {
        payOrder.setUpdateTime(DateUtils.getNowDate());
        return payOrderMapper.updatePayOrder(payOrder);
    }

    /**
     * 批量删除支付订单
     * 
     * @param tradeNos 需要删除的支付订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByTradeNos(String[] tradeNos)
    {
        return payOrderMapper.deletePayOrderByTradeNos(tradeNos);
    }

    /**
     * 删除支付订单信息
     * 
     * @param tradeNo 支付订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByTradeNo(String tradeNo)
    {
        return payOrderMapper.deletePayOrderByTradeNo(tradeNo);
    }

    /**
     * 更新通知状态
     * @param tradeNo 支付订单主键
     */
    @Override
    public void updateNotifySent(String tradeNo) {
        PayOrder payOrder = new PayOrder();
        payOrder.setNotifyState(PayConstant.YES);
        payOrder.setTradeNo(tradeNo);
        payOrderMapper.updatePayOrder(payOrder);
    }

    @Override
    public boolean updateInit2Ing(String tradeNo, PayOrder payOrder) {
        PayOrder dbPayOrder = payOrderMapper.selectPayOrderByTradeNoAndState(tradeNo, PayConstant.STATE_INIT);
        dbPayOrder.setState(PayConstant.STATE_ING);
        //同时更新， 未确定 --》 已确定的其他信息。  如支付接口的确认、 费率的计算。
        dbPayOrder.setChannelCode(payOrder.getChannelCode());
        dbPayOrder.setWayCode(payOrder.getWayCode());
        dbPayOrder.setMchFeeRate(payOrder.getMchFeeRate());
        dbPayOrder.setMchFeeAmount(payOrder.getMchFeeAmount());
        dbPayOrder.setChannelUser(payOrder.getChannelUser());
        return payOrderMapper.updatePayOrder(dbPayOrder)>0;
    }

    @Override
    public boolean updateIng2Success(String tradeNo, String channelOrderNo, String channelUserId) {
        PayOrder dbPayOrder = payOrderMapper.selectPayOrderByTradeNoAndState(tradeNo, PayConstant.STATE_ING);
        dbPayOrder.setState(PayConstant.STATE_SUCCESS);
        dbPayOrder.setChannelOrderNo(channelOrderNo);
        dbPayOrder.setChannelUser(channelUserId);
        dbPayOrder.setSuccessTime(new Date());
        return payOrderMapper.updatePayOrder(dbPayOrder)>0;
    }

    @Override
    public boolean updateIng2Fail(String tradeNo, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg) {
        PayOrder dbPayOrder = payOrderMapper.selectPayOrderByTradeNoAndState(tradeNo, PayConstant.STATE_ING);
        dbPayOrder.setState(PayConstant.STATE_FAIL);
        dbPayOrder.setErrCode(channelErrCode);
        dbPayOrder.setErrMsg(channelErrMsg);
        dbPayOrder.setChannelOrderNo(channelOrderNo);
        dbPayOrder.setChannelUser(channelUserId);
        return payOrderMapper.updatePayOrder(dbPayOrder)>0;
    }

    @Override
    public boolean updateIng2SuccessOrFail(String tradeNo, Long updateState, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg) {
        if(updateState == PayConstant.STATE_ING){
            return true;
        }else if(updateState == PayConstant.STATE_SUCCESS){
            return updateIng2Success(tradeNo, channelOrderNo, channelUserId);
        }else if(updateState == PayConstant.STATE_FAIL){
            return updateIng2Fail(tradeNo, channelOrderNo, channelUserId, channelErrCode, channelErrMsg);
        }
        return false;
    }

    @Override
    public boolean updateIng2Close(String tradeNo) {
        PayOrder dbPayOrder = payOrderMapper.selectPayOrderByTradeNoAndState(tradeNo, PayConstant.STATE_ING);
        dbPayOrder.setState(PayConstant.STATE_CLOSED);
        dbPayOrder.setSuccessTime(new Date());
        return payOrderMapper.updatePayOrder(dbPayOrder)>0;
    }

    @Override
    public PayOrder queryMchOrder(String mchNo, String tradeNo, String mchOrderNo) {
        if(StringUtils.isNotEmpty(tradeNo)){
            return payOrderMapper.selectPayOrderByTradeNoAndMchNo(tradeNo,mchNo);
        }else if(StringUtils.isNotEmpty(mchOrderNo)){
            return payOrderMapper.selectPayOrderByMchOrderNoAndMchNo(mchOrderNo,mchNo);
        }
        return null;
    }

    @Override
    public boolean checkMchOrder(String mchNo, String mchOrderNo) {
        PayOrder payOrder = payOrderMapper.selectPayOrderByMchOrderNoAndMchNo(mchOrderNo, mchNo);
        return payOrder != null;
    }
}
