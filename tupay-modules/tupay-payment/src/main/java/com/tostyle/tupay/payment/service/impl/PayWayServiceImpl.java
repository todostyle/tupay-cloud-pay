package com.tostyle.tupay.payment.service.impl;

import java.util.List;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.PayWayMapper;
import com.tostyle.tupay.payment.domain.PayWay;
import com.tostyle.tupay.payment.service.IPayWayService;

/**
 * 支付方式Service业务层处理
 * 
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class PayWayServiceImpl implements IPayWayService 
{
    @Autowired
    private PayWayMapper payWayMapper;

    /**
     * 查询支付方式
     * 
     * @param id 支付方式主键
     * @return 支付方式
     */
    @Override
    public PayWay selectPayWayById(Long id)
    {
        return payWayMapper.selectPayWayById(id);
    }

    /**
     * 查询支付方式列表
     * 
     * @param payWay 支付方式
     * @return 支付方式
     */
    @Override
    public List<PayWay> selectPayWayList(PayWay payWay)
    {
        return payWayMapper.selectPayWayList(payWay);
    }

    /**
     * 新增支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    @Override
    public int insertPayWay(PayWay payWay)
    {
        payWay.setCreateTime(DateUtils.getNowDate());
        return payWayMapper.insertPayWay(payWay);
    }

    /**
     * 修改支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    @Override
    public int updatePayWay(PayWay payWay)
    {
        payWay.setUpdateTime(DateUtils.getNowDate());
        return payWayMapper.updatePayWay(payWay);
    }

    /**
     * 批量删除支付方式
     * 
     * @param ids 需要删除的支付方式主键
     * @return 结果
     */
    @Override
    public int deletePayWayByIds(Long[] ids)
    {
        return payWayMapper.deletePayWayByIds(ids);
    }

    /**
     * 删除支付方式信息
     * 
     * @param id 支付方式主键
     * @return 结果
     */
    @Override
    public int deletePayWayById(Long id)
    {
        return payWayMapper.deletePayWayById(id);
    }

    @Override
    public boolean checkWayCode(String wayCode) {
        PayWay payWay = payWayMapper.checkWayCode(wayCode);
        return payWay==null;
    }
}
