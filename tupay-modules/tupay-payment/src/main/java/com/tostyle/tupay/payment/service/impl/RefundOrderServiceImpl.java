package com.tostyle.tupay.payment.service.impl;

import java.util.Date;
import java.util.List;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.DateUtils;
import com.tostyle.tupay.common.core.utils.StringUtils;
import com.tostyle.tupay.payment.mapper.PayOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.RefundOrderMapper;
import com.tostyle.tupay.payment.domain.RefundOrder;
import com.tostyle.tupay.payment.service.IRefundOrderService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 退款订单Service业务层处理
 *
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class RefundOrderServiceImpl implements IRefundOrderService {
    @Autowired
    private RefundOrderMapper refundOrderMapper;
    @Autowired
    private PayOrderMapper payOrderMapper;

    /**
     * 查询退款订单
     *
     * @param refundOrderId 退款订单主键
     * @return 退款订单
     */
    @Override
    public RefundOrder selectRefundOrderByRefundOrderId(String refundOrderId) {
        return refundOrderMapper.selectRefundOrderByRefundOrderId(refundOrderId);
    }

    /**
     * 查询退款订单列表
     *
     * @param refundOrder 退款订单
     * @return 退款订单
     */
    @Override
    public List<RefundOrder> selectRefundOrderList(RefundOrder refundOrder) {
        return refundOrderMapper.selectRefundOrderList(refundOrder);
    }

    /**
     * 新增退款订单
     *
     * @param refundOrder 退款订单
     * @return 结果
     */
    @Override
    public int insertRefundOrder(RefundOrder refundOrder) {
        refundOrder.setCreateTime(DateUtils.getNowDate());
        return refundOrderMapper.insertRefundOrder(refundOrder);
    }

    /**
     * 修改退款订单
     *
     * @param refundOrder 退款订单
     * @return 结果
     */
    @Override
    public int updateRefundOrder(RefundOrder refundOrder) {
        refundOrder.setUpdateTime(DateUtils.getNowDate());
        return refundOrderMapper.updateRefundOrder(refundOrder);
    }

    /**
     * 批量删除退款订单
     *
     * @param refundOrderIds 需要删除的退款订单主键
     * @return 结果
     */
    @Override
    public int deleteRefundOrderByRefundOrderIds(String[] refundOrderIds) {
        return refundOrderMapper.deleteRefundOrderByRefundOrderIds(refundOrderIds);
    }

    /**
     * 删除退款订单信息
     *
     * @param refundOrderId 退款订单主键
     * @return 结果
     */
    @Override
    public int deleteRefundOrderByRefundOrderId(String refundOrderId) {
        return refundOrderMapper.deleteRefundOrderByRefundOrderId(refundOrderId);
    }

    @Override
    public RefundOrder queryMchOrder(String mchNo, String mchRefundNo, String refundOrderId) {
        if (StringUtils.isNotEmpty(refundOrderId)) {
            return refundOrderMapper.selectRefundOrderByMchNoAndRefundOrderId(mchNo, refundOrderId);
        } else if (StringUtils.isNotEmpty(mchRefundNo)) {
            return refundOrderMapper.selectRefundOrderByMchNoAndMchRefundNo(mchNo, mchRefundNo);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public boolean updateInit2Ing(String refundOrderId, String channelOrderNo) {
        RefundOrder updateRecord = refundOrderMapper.selectRefundOrderByRefundOrderIdAndState(refundOrderId, PayConstant.REFUND_STATE_INIT);
        updateRecord.setState(PayConstant.REFUND_STATE_ING);
        updateRecord.setChannelOrderNo(channelOrderNo);
        return refundOrderMapper.updateRefundOrder(updateRecord) > 0;
    }

    @Override
    @Transactional
    public boolean updateIng2Success(String refundOrderId, String channelOrderNo) {
        RefundOrder updateRecord = refundOrderMapper.selectRefundOrderByRefundOrderIdAndState(refundOrderId, PayConstant.REFUND_STATE_ING);
        updateRecord.setState(PayConstant.REFUND_STATE_SUCCESS);
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setSuccessTime(new Date());
        //更新退款订单信息
        int refundRet = refundOrderMapper.updateRefundOrder(updateRecord);
        if (refundRet < 0) {
            return false;
        }
        //更新订单表数据（更新退款次数,退款状态,如全额退款更新支付状态为已退款）
        RefundOrder refundOrder = refundOrderMapper.selectRefundOrderByRefundOrderId(refundOrderId);
        int updateCount =payOrderMapper.updateRefundAmountAndCount(refundOrder.getTradeNo(),refundOrder.getRefundAmount());
        if(updateCount <= 0){
            throw new BizException("更新订单数据异常");
        }
        return true;
    }

    @Override
    @Transactional
    public boolean updateIng2Fail(String refundOrderId, String channelOrderNo, String channelErrCode, String channelErrMsg) {
        RefundOrder updateRecord = refundOrderMapper.selectRefundOrderByRefundOrderIdAndState(refundOrderId, PayConstant.REFUND_STATE_ING);
        updateRecord.setState(PayConstant.REFUND_STATE_FAIL);
        updateRecord.setErrCode(channelErrCode);
        updateRecord.setErrMsg(channelErrMsg);
        updateRecord.setChannelOrderNo(channelOrderNo);
        return refundOrderMapper.updateRefundOrder(updateRecord)>0;
    }

    @Override
    @Transactional
    public boolean updateIng2SuccessOrFail(String refundOrderId, Long updateState, String channelOrderNo, String channelErrCode, String channelErrMsg) {
        if(updateState == PayConstant.REFUND_STATE_ING){
            return true;
        }else if(updateState == PayConstant.REFUND_STATE_SUCCESS){
            return updateIng2Success(refundOrderId, channelOrderNo);
        }else if(updateState == PayConstant.REFUND_STATE_FAIL){
            return updateIng2Fail(refundOrderId, channelOrderNo, channelErrCode, channelErrMsg);
        }
        return false;
    }

    @Override
    public Integer updateOrderExpired() {
        return null;
    }

    @Override
    public boolean checkTradeNoAndState(String tradeNo, Long state) {
        RefundOrder refundOrder = refundOrderMapper.selectRefundOrderByTradeNoAndState(tradeNo, state);
        return refundOrder != null;
    }

    @Override
    public boolean checkMchRefundNoAndMchNo(String mchNo, String mchRefundNo) {
        RefundOrder refundOrder = refundOrderMapper.selectRefundOrderByMchNoAndMchRefundNo(mchNo, mchRefundNo);
        return refundOrder != null;
    }

    @Override
    public Long sumSuccessRefundAmount(String tradeNo) {
        return refundOrderMapper.sumSuccessRefundAmount(tradeNo);
    }
}
