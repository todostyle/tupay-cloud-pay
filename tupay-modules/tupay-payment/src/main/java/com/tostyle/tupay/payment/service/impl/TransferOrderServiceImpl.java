package com.tostyle.tupay.payment.service.impl;

import java.util.Date;
import java.util.List;

import com.tostyle.tupay.common.core.model.PayConstant;
import com.tostyle.tupay.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tostyle.tupay.payment.mapper.TransferOrderMapper;
import com.tostyle.tupay.payment.domain.TransferOrder;
import com.tostyle.tupay.payment.service.ITransferOrderService;

/**
 * 转账订单Service业务层处理
 *
 * @author tostyle
 * @date 2022-04-15
 */
@Service
public class TransferOrderServiceImpl implements ITransferOrderService {
    @Autowired
    private TransferOrderMapper transferOrderMapper;

    /**
     * 查询转账订单
     *
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    @Override
    public TransferOrder selectTransferOrderByTransferId(String transferId) {
        return transferOrderMapper.selectTransferOrderByTransferId(transferId);
    }

    /**
     * 查询转账订单列表
     *
     * @param transferOrder 转账订单
     * @return 转账订单
     */
    @Override
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder) {
        return transferOrderMapper.selectTransferOrderList(transferOrder);
    }

    /**
     * 新增转账订单
     *
     * @param transferOrder 转账订单
     * @return 结果
     */
    @Override
    public int insertTransferOrder(TransferOrder transferOrder) {
        transferOrder.setCreateTime(DateUtils.getNowDate());
        return transferOrderMapper.insertTransferOrder(transferOrder);
    }

    /**
     * 修改转账订单
     *
     * @param transferOrder 转账订单
     * @return 结果
     */
    @Override
    public int updateTransferOrder(TransferOrder transferOrder) {
        transferOrder.setUpdateTime(DateUtils.getNowDate());
        return transferOrderMapper.updateTransferOrder(transferOrder);
    }

    /**
     * 批量删除转账订单
     *
     * @param transferIds 需要删除的转账订单主键
     * @return 结果
     */
    @Override
    public int deleteTransferOrderByTransferIds(String[] transferIds) {
        return transferOrderMapper.deleteTransferOrderByTransferIds(transferIds);
    }

    /**
     * 删除转账订单信息
     *
     * @param transferId 转账订单主键
     * @return 结果
     */
    @Override
    public int deleteTransferOrderByTransferId(String transferId) {
        return transferOrderMapper.deleteTransferOrderByTransferId(transferId);
    }

    @Override
    public TransferOrder queryMchOrder(String mchNo, String mchOrderNo, String transferId) {
        return transferOrderMapper.selectTransferOrderByMchNoAndMchOrderNo(mchNo,mchOrderNo,transferId);
    }

    @Override
    public boolean checkMchOrderNo(String mchNo, String mchOrderNo) {
        return transferOrderMapper.selectTransferOrderByMchNoAndMchOrderNo(mchNo, mchOrderNo,null) != null;
    }

    @Override
    public boolean updateInit2Ing(String transferId) {
        TransferOrder transferOrder = transferOrderMapper.selectTransferOrderByTransferIdAndState(transferId, PayConstant.TRANSFER_STATE_INIT);
        transferOrder.setState(PayConstant.TRANSFER_STATE_ING);
        return transferOrderMapper.updateTransferOrder(transferOrder) > 0;
    }

    @Override
    public boolean updateIng2Success(String transferId, String channelOrderNo) {
        TransferOrder updateRecord = transferOrderMapper.selectTransferOrderByTransferIdAndState(transferId, PayConstant.TRANSFER_STATE_ING);
        updateRecord.setState(PayConstant.TRANSFER_STATE_SUCCESS);
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setSuccessTime(new Date());
        return transferOrderMapper.updateTransferOrder(updateRecord) > 0;
    }

    @Override
    public boolean updateIng2Fail(String transferId, String channelOrderNo, String channelErrCode, String channelErrMsg) {
        TransferOrder updateRecord = transferOrderMapper.selectTransferOrderByTransferIdAndState(transferId, PayConstant.TRANSFER_STATE_ING);
        updateRecord.setState(PayConstant.TRANSFER_STATE_FAIL);
        updateRecord.setErrCode(channelErrCode);
        updateRecord.setErrMsg(channelErrMsg);
        updateRecord.setChannelOrderNo(channelOrderNo);
        return transferOrderMapper.updateTransferOrder(updateRecord) > 0;
    }

    @Override
    public boolean updateIng2SuccessOrFail(String transferId, Long state, String channelOrderNo, String channelErrCode, String channelErrMsg) {
        if(state == PayConstant.TRANSFER_STATE_ING){
            return true;
        }else if(state == PayConstant.TRANSFER_STATE_SUCCESS){
            return updateIng2Success(transferId, channelOrderNo);
        }else if(state == PayConstant.TRANSFER_STATE_FAIL){
            return updateIng2Fail(transferId, channelOrderNo, channelErrCode, channelErrMsg);
        }
        return false;
    }
}