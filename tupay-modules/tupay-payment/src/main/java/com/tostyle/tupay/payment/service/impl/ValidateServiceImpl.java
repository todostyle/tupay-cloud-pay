package com.tostyle.tupay.payment.service.impl;

import com.tostyle.tupay.common.core.exception.BizException;
import com.tostyle.tupay.payment.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * @author tostyle
 * 2022/4/18 9:19
 */
@Service
public class ValidateServiceImpl implements ValidateService {

    @Autowired
    private Validator validator;

    @Override
    public void validate(Object obj) {
        Set<ConstraintViolation<Object>> resultSet = validator.validate(obj);
        if(resultSet == null || resultSet.isEmpty()){
            return ;
        }
        resultSet.forEach(item -> {throw new BizException(item.getMessage());});
    }
}
