package com.tostyle.tupay.payment.util;

import com.tostyle.tupay.payment.domain.model.AbstractResp;

/**
 * api响应结果构造器
 * @author tostyle
 * 2022/4/18 18:25
 */
public class ApiRespBuilder {


    /** 构建自定义响应对象, 默认响应成功 **/
    public static <T extends AbstractResp> T buildSuccess(Class<? extends AbstractResp> T){
        try {
            T result = (T)T.newInstance();
            return result;
        } catch (Exception e) { return null; }
    }
}
