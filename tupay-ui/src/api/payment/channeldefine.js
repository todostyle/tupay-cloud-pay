import request from '@/utils/request'

// 查询支付渠道列表
export function listChanneldefine(query) {
  return request({
    url: '/payment/channeldefine/list',
    method: 'get',
    params: query
  })
}

// 查询支付渠道详细
export function getChanneldefine(channelCode) {
  return request({
    url: '/payment/channeldefine/' + channelCode,
    method: 'get'
  })
}

// 新增支付渠道
export function addChanneldefine(data) {
  return request({
    url: '/payment/channeldefine',
    method: 'post',
    data: data
  })
}

// 修改支付渠道
export function updateChanneldefine(data) {
  return request({
    url: '/payment/channeldefine',
    method: 'put',
    data: data
  })
}

// 删除支付渠道
export function delChanneldefine(channelCode) {
  return request({
    url: '/payment/channeldefine/' + channelCode,
    method: 'delete'
  })
}
