import request from '@/utils/request'

// 查询服务商列表列表
export function listIsvinfo(query) {
  return request({
    url: '/payment/isvinfo/list',
    method: 'get',
    params: query
  })
}

// 查询服务商列表详细
export function getIsvinfo(isvNo) {
  return request({
    url: '/payment/isvinfo/' + isvNo,
    method: 'get'
  })
}

// 新增服务商列表
export function addIsvinfo(data) {
  return request({
    url: '/payment/isvinfo',
    method: 'post',
    data: data
  })
}

// 修改服务商列表
export function updateIsvinfo(data) {
  return request({
    url: '/payment/isvinfo',
    method: 'put',
    data: data
  })
}

// 删除服务商列表
export function delIsvinfo(isvNo) {
  return request({
    url: '/payment/isvinfo/' + isvNo,
    method: 'delete'
  })
}
