import request from '@/utils/request'

// 查询应用列表列表
export function listMchapp(query) {
  return request({
    url: '/payment/mchapp/list',
    method: 'get',
    params: query
  })
}

// 查询应用列表详细
export function getMchapp(appId) {
  return request({
    url: '/payment/mchapp/' + appId,
    method: 'get'
  })
}

// 新增应用列表
export function addMchapp(data) {
  return request({
    url: '/payment/mchapp',
    method: 'post',
    data: data
  })
}

// 修改应用列表
export function updateMchapp(data) {
  return request({
    url: '/payment/mchapp',
    method: 'put',
    data: data
  })
}

// 删除应用列表
export function delMchapp(appId) {
  return request({
    url: '/payment/mchapp/' + appId,
    method: 'delete'
  })
}
