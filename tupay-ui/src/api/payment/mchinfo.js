import request from '@/utils/request'

// 查询商户列表列表
export function listMchinfo(query) {
  return request({
    url: '/payment/mchinfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户列表详细
export function getMchinfo(mchNo) {
  return request({
    url: '/payment/mchinfo/' + mchNo,
    method: 'get'
  })
}

// 新增商户列表
export function addMchinfo(data) {
  return request({
    url: '/payment/mchinfo',
    method: 'post',
    data: data
  })
}

// 修改商户列表
export function updateMchinfo(data) {
  return request({
    url: '/payment/mchinfo',
    method: 'put',
    data: data
  })
}

// 删除商户列表
export function delMchinfo(mchNo) {
  return request({
    url: '/payment/mchinfo/' + mchNo,
    method: 'delete'
  })
}
