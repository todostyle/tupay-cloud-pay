import request from '@/utils/request'

// 查询商户通知列表
export function listMchnotify(query) {
  return request({
    url: '/payment/mchnotify/list',
    method: 'get',
    params: query
  })
}

// 查询商户通知详细
export function getMchnotify(notifyId) {
  return request({
    url: '/payment/mchnotify/' + notifyId,
    method: 'get'
  })
}

// 新增商户通知
export function addMchnotify(data) {
  return request({
    url: '/payment/mchnotify',
    method: 'post',
    data: data
  })
}

// 修改商户通知
export function updateMchnotify(data) {
  return request({
    url: '/payment/mchnotify',
    method: 'put',
    data: data
  })
}

// 删除商户通知
export function delMchnotify(notifyId) {
  return request({
    url: '/payment/mchnotify/' + notifyId,
    method: 'delete'
  })
}
