import request from '@/utils/request'

// 查询支付订单列表
export function listPayorder(query) {
  return request({
    url: '/payment/payorder/list',
    method: 'get',
    params: query
  })
}

// 查询支付订单详细
export function getPayorder(tradeNo) {
  return request({
    url: '/payment/payorder/' + tradeNo,
    method: 'get'
  })
}

// 新增支付订单
export function addPayorder(data) {
  return request({
    url: '/payment/payorder',
    method: 'post',
    data: data
  })
}

// 修改支付订单
export function updatePayorder(data) {
  return request({
    url: '/payment/payorder',
    method: 'put',
    data: data
  })
}

// 删除支付订单
export function delPayorder(tradeNo) {
  return request({
    url: '/payment/payorder/' + tradeNo,
    method: 'delete'
  })
}
