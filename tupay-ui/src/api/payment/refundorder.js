import request from '@/utils/request'

// 查询退款订单列表
export function listRefundorder(query) {
  return request({
    url: '/payment/refundorder/list',
    method: 'get',
    params: query
  })
}

// 查询退款订单详细
export function getRefundorder(refundOrderId) {
  return request({
    url: '/payment/refundorder/' + refundOrderId,
    method: 'get'
  })
}

// 新增退款订单
export function addRefundorder(data) {
  return request({
    url: '/payment/refundorder',
    method: 'post',
    data: data
  })
}

// 修改退款订单
export function updateRefundorder(data) {
  return request({
    url: '/payment/refundorder',
    method: 'put',
    data: data
  })
}

// 删除退款订单
export function delRefundorder(refundOrderId) {
  return request({
    url: '/payment/refundorder/' + refundOrderId,
    method: 'delete'
  })
}
