import request from '@/utils/request'

// 查询转账订单列表
export function listTransferorder(query) {
  return request({
    url: '/payment/transferorder/list',
    method: 'get',
    params: query
  })
}

// 查询转账订单详细
export function getTransferorder(transferId) {
  return request({
    url: '/payment/transferorder/' + transferId,
    method: 'get'
  })
}

// 新增转账订单
export function addTransferorder(data) {
  return request({
    url: '/payment/transferorder',
    method: 'post',
    data: data
  })
}

// 修改转账订单
export function updateTransferorder(data) {
  return request({
    url: '/payment/transferorder',
    method: 'put',
    data: data
  })
}

// 删除转账订单
export function delTransferorder(transferId) {
  return request({
    url: '/payment/transferorder/' + transferId,
    method: 'delete'
  })
}
